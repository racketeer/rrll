#lang scribble/manual

@require[scribble-math/dollar
	pict
	pict/color
	rrll/canvas
	rrll/canvas/bitmap
	racket/class
	(for-syntax racket/base)
	"../rasterizer.rkt"]

@title[#:style (with-html5 manual-doc-style)]{RRLL: Rasterizer}
@author[
 @author+email["Dominik Pantůček" "dominik.pantucek@trustica.cz"]]

Racket Rogue-Like Library: Rasterizer for 3D renderer.

@section{Rasterizer}

@defmodule[rrll/rasterizer]

This module implements rasterization of triangles in screen coordinates.

@defproc[(make-rasterizer (can canvas?)
			  (num-futures positive-integer? (processor-count))
			  (#:defer-alpha defer-alpha? boolean? #t)) rasterizer?]{

Creates a rasterizer for single rendering.

If @racket[defer-alpha?] is @racket[#t], rasterization of triangles
needing alpha blending is postponed until the
@racket[finish-rasterizer] call.

}

@defproc[(rasterizer? (v any/c)) boolean?]{

Returns @racket[#t] if given value is a rasterizer struct.

}

@defproc[(finish-rasterizer (rst rasterizer?)) void?]{

Waits until all pending operations on given rasterizer finished.

}

@defproc[(raster-triangle (rst rasterizer?)
			  (x1 fixnum?) (y1 fixnum?) (w1 flonum?)
			  (x2 fixnum?) (y2 fixnum?) (w2 flonum?)
			  (x3 fixnum?) (y3 fixnum?) (w3 flonum?)
			  (#:color color fixnum? #xff00ff)
			  (#:c1 c1 (or/c fixnum? #f) #f)
			  (#:c2 c2 (or/c fixnum? #f) #f)
			  (#:c3 c3 (or/c fixnum? #f) #f)
			  (#:u1 u1 (or/c flonum? #f) #f)
			  (#:u2 u2 (or/c flonum? #f) #f)
			  (#:u3 u3 (or/c flonum? #f) #f)
			  (#:v1 v1 (or/c flonum? #f) #f)
			  (#:v2 v2 (or/c flonum? #f) #f)
			  (#:v3 v3 (or/c flonum? #f) #f)
			  (#:tex tex (or/c mipmap? #f) #f)
			  (#:blending blending boolean? #f)
			  (#:alpha alpha (or/c (integer-in 0 255) #f) #f)
			  (#:id id fixnum? 0)
			  (#:setw setw boolean #t)
			  ) void?]{

Sends triangle to given rasterizer for processing.

If @racket[setw] is @racket[#f], the w-buffer is only tested but not
updated.

}

@defproc[(raster-quad (rst rasterizer?)
		      (x1 fixnum?) (y1 fixnum?) (w1 flonum?)
		      (x2 fixnum?) (y2 fixnum?) (w2 flonum?)
		      (x3 fixnum?) (y3 fixnum?) (w3 flonum?)
		      (x4 fixnum?) (y4 fixnum?) (w4 flonum?)
		      (#:color color fixnum? #xff00ff)
		      (#:c1 c1 (or/c fixnum? #f) #f)
		      (#:c2 c2 (or/c fixnum? #f) #f)
		      (#:c3 c3 (or/c fixnum? #f) #f)
		      (#:c4 c4 (or/c fixnum? #f) #f)
		      (#:u1 u1 (or/c flonum? #f) #f)
		      (#:u2 u2 (or/c flonum? #f) #f)
		      (#:u3 u3 (or/c flonum? #f) #f)
		      (#:u4 u4 (or/c flonum? #f) #f)
		      (#:v1 v1 (or/c flonum? #f) #f)
		      (#:v2 v2 (or/c flonum? #f) #f)
		      (#:v3 v3 (or/c flonum? #f) #f)
		      (#:v4 v4 (or/c flonum? #f) #f)
		      (#:tex tex (or/c mipmap? #f) #f)
		      (#:blending blending boolean? #f)
		      (#:alpha alpha (or/c (integer-in 0 255) #f) #f)
		      (#:id id fixnum? 0)
		      (#:setw setw boolean #t)
		      ) void?]{

Sends two triangles comprising the quad to the rasterizer. The
triangles are composed of @racket[(1 2 3)] and @racket[(1 3 4)]
vertices.

}

@defproc[(raster-line (rst rasterizer?)
		      (x0 fixnum?) (y0 fixnum?) (w0 flonum?)
		      (x1 fixnum?) (y1 fixnum?) (w1 flonum?)
		      (#:color color fixnum? #xff000000)
		      (#:w-bias w-bias flonum? 1.024)
		      (#:blending blending boolean? #f)
		      (#:alpha alpha (or/c (integer-in 0 255) #f) #f)
		      (#:id id (or/c fixnum? #f) #f)
		      ) void?]{

Renders a 2D line with given W-coordinates gradient. The
@racket[w-bias] is used to ensure the line is always drawn even when
the triangle of the same depth is already drawn onto the
@racket[canvas?].

}

@section{Usage}

The @racket[rasterizer?] must be used with @racket[canvas?] in order
to rasterizer triangles. The common setup is:

@racketblock[
(define can (make-canvas 320 240))
(define rst (make-rasterizer can))
...
(finish-rasterizer rst)
]

@(define (rstbmp proc)
  (define can (make-canvas 320 240))
  (draw-clear can)
  (define rst (make-rasterizer can))
  (proc rst)
  (finish-rasterizer rst)
  (canvas->bitmap% can))

@(define-syntax (rstex stx)
  (syntax-case stx ()
    ((_ (rst) expr ...)
     #'(list (racketblock expr ...)
     	     (rstbmp (lambda (rst) expr ...))))))

@;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

@subsection{Triangle Filled with Single Color}

@(rstex (rst) (raster-triangle rst 20 20 1.0 40 220 1.0 300 50 1.0 #:color #xccff))

@subsection{Alpha Blending Triangles Filled with Single Color}

@(rstex (rst)
(raster-triangle rst 20 20 2.0 40 220 2.0 300 50 2.0 #:color #xff0000)
(raster-triangle rst 280 30 1.0 10 90 1.0 310 230 1.0 #:color #x00ff00 #:alpha 127))

@subsection{Smoothly-Colored Triangles}

@(rstex (rst)
(raster-triangle rst 20 220 1.0 300 180 1.0 140 20 1.0 #:c1 #xff0000 #:c2 #xff00 #:c3 #xff))

@subsection{Alpha Blending Smoothly-Colored Triangles}

@(rstex (rst)
(raster-triangle rst 20 220 2.0 300 180 2.0 140 20 2.0 #:c1 #xff0000 #:c2 #xff00 #:c3 #xff)
(raster-triangle rst 20 20 1.0 140 220 1.0 300 50 1.0 #:c1 #xffff00 #:c2 #xffff #:c3 #xff00ff #:alpha 127)
)
