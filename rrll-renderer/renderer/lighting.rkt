#lang racket/base

(require rrll/math/algebra3
         rrll/unsafe/flonum
         rrll/color/lighting)

(provide make-lightsrc
         lightsrc?
         move-lightsrc
         colorize-lightsrc
         apply-lightsrc
         apply-lightsrc*
         triangle-normal)

(struct lightsrc
  (pos
   rgb
   ))

(define (make-lightsrc pos lrgb)
  (lightsrc pos lrgb))

(define (move-lightsrc ls pos)
  (struct-copy lightsrc ls
               (pos pos)))

(define (colorize-lightsrc ls rgb)
  (struct-copy lightsrc ls
               (rgb rgb)))

(define (apply-lightsrc l ; lsource
                        v ; vertex - flvec3
                        n ; normal - flvec3
                        )
  (define lv
    (flvec3-sub v (lightsrc-pos l)))
  (define lv1
    (flvec3-normalize lv))
  (define dot
    (flvec3-dot n lv1))
  (if (fl< dot 0.0)
      (make-light 0.0 0.0 0.0)
      (flvec3-mul
       (flvec3-div
        (lightsrc-rgb l)
        (flvec3-length lv))
       dot)))

(define (apply-lightsrc* ls v n)
  (for/fold ((lr (make-light 0.0 0.0 0.0)))
            ((l (in-list ls)))
    (light-add lr (apply-lightsrc l v n))))

(define (triangle-normal A B C)
  (flvec3-normalize
   (flvec3-cross (flvec3-sub B A)
                 (flvec3-sub C A))))
