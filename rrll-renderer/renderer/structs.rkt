#lang racket/base

(require racket/match
         racket/performance-hint
         rrll/unsafe/fixnum)

(provide (struct-out rvertex)
         (struct-out rtriangle)
         make-rtriangle-flags
         rtriangle-flags-cw?
         rtriangle-flags-ccw?
         rtriangle-flags-setw?)

(define rtriangle-flags-cw 1)
(define rtriangle-flags-ccw 2)
(define rtriangle-flags-setw 4)

(struct rvertex
  (flpos ; flvec4 - x,y,z,w
   uv ; flvector - u,v
   light ; fllight b g r
   color
   )
  #:methods gen:custom-write
  ((define (write-proc v p m)
     (match-define (rvertex flpos uv light color) v)
     (display
      (format
       "#<vvertex ~v ~v ~v ~v>"
       flpos uv light color
       ) p))))

; Used only to support easier clipping against near plane
(struct rtriangle
  (
   A B C
   tex/color
   blending
   alpha
   id
   wire ; #f or color
   flags
   )
  #:methods gen:custom-write
  ((define (write-proc v p m)
     (match-define (rtriangle A B C tex/color alpha? blend-alpha id wire flags) v)
     (display
      (format
       "#<vtriangle tex=~v α=~v β=~a id=~v ~v ~v ~v ~v f:~v>"
       tex/color alpha?
       (if blend-alpha (real->decimal-string blend-alpha) blend-alpha) id
       A B C
       wire
       flags
       ) p))))

(define-inline (make-rtriangle-flags cw ccw setw)
  (fxior (if cw rtriangle-flags-cw 0)
         (if ccw rtriangle-flags-ccw 0)
         (if setw rtriangle-flags-setw 0)
         ))

(define-inline (rtriangle-flags-cw? flags)
  (fx> (fxand flags rtriangle-flags-cw) 0))

(define-inline (rtriangle-flags-ccw? flags)
  (fx> (fxand flags rtriangle-flags-ccw) 0))

(define-inline (rtriangle-flags-setw? flags)
  (fx> (fxand flags rtriangle-flags-setw) 0))
