#lang scribble/manual

@require[scribble-math/dollar]

@title[#:style (with-html5 manual-doc-style)]{RRLL: Renderer}
@author[
 @author+email["Dominik Pantůček" "dominik.pantucek@trustica.cz"]]

Racket Rogue-Like Library: 3D renderer.

@section{Curried Projector}

@defmodule[rrll/renderer/projector]

Camera projection in a curried form.

@defproc[((make-projector (near flonum?)
			  (far flonum?)
			  (width fixnum?)
			  (height fixnum?)
			  (pixel-aspect flonum? 1.0))
			  (flv4 flvector?))
			  (values fixnum?
			  	  fixnum?
				  flonum?)]{

Returns simple projector that can be used without camera management.

}

@section{On-Screen Triangle Geometry}

@defmodule[rrll/renderer/geometry]

This module is a helper for recognizing clockwise and
counter-clockwise triangles on screen. It also allows computing the
number of pixels within a given on-screen triangle.

@defproc[(triangle-counter-clockwise? (x1 fixnum?)
				      (y1 fixnum?)
				      (x2 fixnum?)
				      (y2 fixnum?)
				      (x3 fixnum?)
				      (y3 fixnum?))
				      boolean?]{

Returns @racket[#t] if the vertices are ordered clockwise on screen.

}

@defproc[(triangle-make-counter-clockwise (x1 fixnum?)
				      	  (y1 fixnum?)
				      	  (x2 fixnum?)
				      	  (y2 fixnum?)
				      	  (x3 fixnum?)
				      	  (y3 fixnum?))
					  (values fixnum?
					  	  fixnum?
						  fixnum?
						  fixnum?
						  fixnum?
						  fixnum?)]{

Returns the vertices in the counter-clockwise order.

}

@defproc[(triangle-area (x1 fixnum?)
			(y1 fixnum?)
			(x2 fixnum?)
			(y2 fixnum?)
			(x3 fixnum?)
			(y3 fixnum?))
			fixnum?]{

Returns the number of pixels this triangle will cover on the screen.

}

@section{Renderer Camera}

@defmodule[rrll/camera]

This module implements a camera that is used for worldview and
projection transformations upon actual rendering.

@defproc[(make-camera (width fixnum?)
		      (height fixnum?)
		      (#:pos pos flvector? (flvec4))
		      (#:aspect aspect (or/c 'halves 'quads 'slc flonum?) 1.0)
		      (#:azimuth azimuth flonum? 0.0)
		      (#:inclination inclination flonum? 0.0)
		      (#:shear shear flonum? 0.0)
		      (#:far far flonum? 100.0))
		      camera?] {

Creates a new camera struct with given fields.

@itemlist[
	@item{@racket[width] - width of the target canvas in pixels}
	@item{@racket[height] - height of the target canvas in pixels}
	@item{@racket[pos] - position of the camera in world coordinates}
	@item{@racket[aspect] - pixel/canvas aspect ratio or symbol specifying common pixel output methods}
	@item{@racket[azimuth] - rotation around the Y axis (looking around when standing)}
	@item{@racket[inclination] - rotation around the Z axis (looking up and down)}
	@item{@racket[shear] - rotation around the X axis (shearing camera left and right)}
	@item{@racket[far] - far plane for projection matrix computation}
	]

}

@defproc[(camera? (v any/c)) boolean?]{

Returns @racket[#t] if the given values is a camera struct.

}

@defproc[(move-camera* (cam camera?)
		       (x flonum?)
		       (y flonum?)
		       (z flonum?)
		       ) camera?]{

Returns a new camera positioned at given coordinates. Other fields are
kept intact.

}

@defproc[(move-camera (cam camera?)
		      (pos flvector?)
		      ) camera?]{

Sets camera position to the one given.

}

@defproc[(resize-camera (cam camera?)
			(width fixnum?)
			(height fixnum?)
			(#:aspect aspect (or/c #f 'halves 'quads 'slc flonum?) #f)
			(#:far far (or/c #f flonum?) #f)
			) camera?]{

Changes the camera attributes. If @racket[aspect] and/or @racket[far]
are non-@racket[#f], they are changed as well. This allows for
changing rendering targets on the fly.

}

@defproc[(rotate-camera (cam camera?)
			(#:azimuth azimuth (or/c #f flonum?) #f)
			(#:inclination inclination (or/c #f flonum?) #f)
			(#:shear shear (or/c #f flonum?) #f)
			) camera?]{

Sets the camera rotation. All @racket[#f] angles are left intact.

}

@defproc[(rotate-camera-rel (cam camera?)
			    (#:azimuth azimuth flonum? 0.0)
			    (#:inclination inclination flonum? 0.0)
			    (#:shear shear flonum? 0.0)
			    ) camera?]{

Adjusts camera rotation by given angle delta values.

}

@defproc[(camera-worldview (cam camera?)) flvector?]{

Returns the worldview matrix of this camera:

@$${M\times Y\times X\times Z\times T}

See @racketmodname[rrll/math/transform4] for more information.

}

@defproc[(camera-projection (cam camera?)) flvector?]{

Returns the projection matrix of the camera.

}

@defproc[(camera-fov/2 (cam camera?)) flonum?]{

Returns half the size of the field of view in radians.

}

@defproc[(camera-fov (cam camera?)) flonum?]{

Returns the field of view in radians.

}

@section{Positional Lighting}

@defmodule[rrll/renderer/lighting]

This module provides infrastructure for using positional lights with
the renderer.

@defproc[(make-lightsrc (pos flvector?)
			(rgb flvector?)) lightsrc?]{

Creates a new positional light source.

}

@defproc[(lightsrc? (v any/c)) boolean?]{

Returns @racket[#t] if given value is a positional light source.

}

@defproc[(move-lightsrc (ls lightsrc?)
			(pos flvector?)) lightsrc?]{

Sets the light source position to the value provided.

}

@defproc[(colorize-lightsrc (ls lightsrc?)
			    (rgb flvector?)) lightsrc?]{

Sets light source's color.

}

@defproc[(apply-lightsrc (l lightsrc?)
			 (v flvector?)
			 (n flvector?)) flvector?]{


Applies light source @racket[l] at location of vertex @racket[v] with
assumed normal vector of the plane the vertex is part of to be
@racket[n]. Returns @racket[flrgb?]/@racket[fllight?] value.

}

@defproc[(apply-lightsrc* (ls (listof lightsrc?))
			  (v flvector?)
			  (n flvector?)) flvector]{

Applies @racket[apply-lightsrc] for all light sources given, producing
a single resulting light to be applied by rasterizer.

}

@defproc[(triangle-normal (A flvector?)
			  (B flvector?)
			  (C flvector?)) flvector?]{

Returns normalized normal vector of the triangle given.

@$${\vec{n_0}=(B-A)\times(C-A)}
@$${\vec{n}=\frac{\vec{n_0}}{|\vec{n_0}}}

}

@section{Renderer}

@defmodule[rrll/renderer]

This is the module that should be used for rendering 3D triangles onto
a 2D canvas with given camera.

@defproc[(make-renderer (can canvas?) (cam camera?)) renderer?]{

Creates a new renderer targetting given @racket[can] canvas using the
camera @racket[cam] given.

}

@defproc[(renderer? (v any/c)) boolean?]{

Returns @racket[#t] if given value @racket[v] is a renderer.

}

@defproc[(finish-renderer (rdr renderer?)) void?]{

Finishes rendering on given renderer and associated rasterizer. Waits
until all rasterization has been finished.

}

@defproc[(render-triangle (rdr renderer?)
			  (x1 fixnum?) (y1 fixnum?) (z1 flonum?)
			  (x2 fixnum?) (y2 fixnum?) (z2 flonum?)
			  (x3 fixnum?) (y3 fixnum?) (z3 flonum?)
			  (#:color color fixnum? #xff00ff)
			  (#:c1 c1 (or/c fixnum? #f) #f)
			  (#:c2 c2 (or/c fixnum? #f) #f)
			  (#:c3 c3 (or/c fixnum? #f) #f)
			  (#:u1 u1 (or/c flonum? #f) #f)
			  (#:u2 u2 (or/c flonum? #f) #f)
			  (#:u3 u3 (or/c flonum? #f) #f)
			  (#:v1 v1 (or/c flonum? #f) #f)
			  (#:v2 v2 (or/c flonum? #f) #f)
			  (#:v3 v3 (or/c flonum? #f) #f)
			  (#:l1 l1 (or/c flvector? #f) #f)
			  (#:l2 l2 (or/c flvector? #f) #f)
			  (#:l3 l3 (or/c flvector? #f) #f)
			  (#:tex tex (or/c mipmap? #f) #f)
			  (#:blending blending boolean? #f)
			  (#:alpha alpha (or/c (integer-in 0 255) #f) #f)
			  (#:id id fixnum? 0)
			  (#:lights lights (or/c #f (listof lightsrc?)) #f)
			  (#:lights1 lights1 (or/c #f (listof lightsrc?)) #f)
			  (#:lights2 lights2 (or/c #f (listof lightsrc?)) #f)
			  (#:lights3 lights3 (or/c #f (listof lightsrc?)) #f)
		      	  (#:ambient ambient (or/c #f flvector?) #f)
		     	  (#:ambient1 ambient1 (or/c #f flvector?) #f)
		      	  (#:ambient2 ambient2 (or/c #f flvector?) #f)
		      	  (#:ambient3 ambient3 (or/c #f flvector?) #f)
			  (#:wire wire boolean? #f)
			  (#:cw cw boolean? #f)
			  (#:ccw ccw boolean? #t)
			  (#:setw setw boolean #t))
			  void?]{

Renders given triangle in world coordinates using the
renderer. Clipping against near plane is performed and the results are
passed down to renderer's rasterizer.

If @racket[wire] is @racket[#t] then for each triangle sent to
rasterizer its bounding lines are emitted as well. This is used mainly
for debugging purposes.

If @racket[lights] and related is provided, it overrides explicit
@racket[l1] and related values.

The flags @racket[cw] and @racket[ccw] specify whether the clockwise
or counter-clockwise oriented triangles should be rasterized.

If @racket[setw] is set to @racket[#f], the w-buffer is only tested,
not updated.

}

@defproc[(render-quad (rdr renderer?)
		      (x1 fixnum?) (y1 fixnum?) (z1 flonum?)
		      (x2 fixnum?) (y2 fixnum?) (z2 flonum?)
		      (x3 fixnum?) (y3 fixnum?) (z3 flonum?)
		      (x4 fixnum?) (y4 fixnum?) (z4 flonum?)
		      (#:color color fixnum? #xff00ff)
		      (#:c1 c1 (or/c fixnum? #f) #f)
		      (#:c2 c2 (or/c fixnum? #f) #f)
		      (#:c3 c3 (or/c fixnum? #f) #f)
		      (#:c4 c4 (or/c fixnum? #f) #f)
		      (#:u1 u1 (or/c flonum? #f) #f)
		      (#:u2 u2 (or/c flonum? #f) #f)
		      (#:u3 u3 (or/c flonum? #f) #f)
		      (#:u4 u4 (or/c flonum? #f) #f)
		      (#:v1 v1 (or/c flonum? #f) #f)
		      (#:v2 v2 (or/c flonum? #f) #f)
		      (#:v3 v3 (or/c flonum? #f) #f)
		      (#:v4 v4 (or/c flonum? #f) #f)
		      (#:l1 l1 (or/c flvector? #f) #f)
		      (#:l2 l2 (or/c flvector? #f) #f)
		      (#:l3 l3 (or/c flvector? #f) #f)
		      (#:l4 l4 (or/c flvector? #f) #f)
		      (#:tex tex (or/c mipmap? #f) #f)
		      (#:blending blending boolean? #f)
		      (#:alpha alpha (or/c (integer-in 0 255) #f) #f)
		      (#:id id fixnum? 0)
		      (#:lights lights (or/c #f (listof lightsrc?)) #f)
		      (#:lights1 lights1 (or/c #f (listof lightsrc?)) #f)
		      (#:lights2 lights2 (or/c #f (listof lightsrc?)) #f)
		      (#:lights3 lights3 (or/c #f (listof lightsrc?)) #f)
		      (#:lights4 lights4 (or/c #f (listof lightsrc?)) #f)
		      (#:ambient ambient (or/c #f flvector?) #f)
		      (#:ambient1 ambient1 (or/c #f flvector?) #f)
		      (#:ambient2 ambient2 (or/c #f flvector?) #f)
		      (#:ambient3 ambient3 (or/c #f flvector?) #f)
		      (#:ambient4 ambient4 (or/c #f flvector?) #f)
		      (#:wire wire boolean? #f)
		      (#:cw cw boolean? #f)
		      (#:ccw ccw boolean? #t)
		      (#:setw setw boolean #t))
		      void?]{

Like @racket[render-triangle] but emits two triangles comprising the
quad specified.

All four vertices must be coplanar or very close to being so.

}
