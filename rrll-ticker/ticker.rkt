#lang racket/base

(provide run-ticker)

(require "ticker/private/env.rkt"
         "ticker/queue.rkt"
         racket/exn
         racket/string)

;; Wraps given procedure in ticking procedure wrapper that returns
;; whenever the wrapped procedure yields using (current-ticking-yield)
;; continuation. The returned value is the duration to wait or #f when
;; the wrapped procedure terminates.
(define (build-ticking-procedure proc)
  ;(log-debug "Creating ticking procedure.")
  (let* ((break #f)
         (resume #f)
         (finished #f)
         (yield (lambda (duration)
                  (let/cc r
                    (set! resume r)
                    (break duration)))))
    (define (ticking-procedure)
      (let/cc cc
        (set! break cc)
        (if finished
            (break #f)
            (cond (resume
                   (resume (void)))
                  (else
                   (parameterize ((current-ticker-yield yield)
                                  (current-ticker-procedure ticking-procedure))
                     (with-handlers
                       ((exn?
                         (lambda (ex)
                           (define exl (string-split (exn->string ex) "\n"))
                           (log-error "Exception in ~v" proc)
                           (for ((exs (in-list exl)))
                             (log-error exs))
                           (set! finished #t)
                           (break #f))))
                       (proc))
                     (set! finished #t)
                     (break #f)
                     ))))))
    ticking-procedure))

;; Single tick happening at current-ts. Given (listof (pairof
;; activation proc)), processes next slice in each proc and returns
;; the same list with updated activations. If the duration returned by
;; proc slice is #f, it is removed from the list. Returns two values:
;; next-ts to wait for and new list of ticking procedures.
(define (ticker-iteration current-ts procs cleanup)
  (let loop ((next-ts #f)
             (next-procs '())
             (procs procs)
             (evts '()))
    (if (null? procs)
        ;; Nothing more to process, go on
        (values next-ts next-procs evts)
        ;; Something in the list, test
        (let* ((procdef (car procs))
               (activation (car procdef))
               (proc (cdr procdef)))
          (cond ((or (and (number? activation)
                          ;; Activated on tick
                          (>= current-ts activation))
                     (and (list? activation)
                          ;; Activated on one of activation events
                          (apply sync/timeout/enable-break 0 activation)))
                 ;; Activated somehow
                 (let ((res (proc)))
                   (cond ((number? res)
                          ;; Duration
                          (let ((res-next-ts (+ current-ts res)))
                            (loop (if next-ts
                                      (min next-ts res-next-ts)
                                      res-next-ts)
                                  (cons (cons res-next-ts proc)
                                        next-procs)
                                  (cdr procs)
                                  evts)))
                         ((list? res)
                          ;; Event(s)
                          (loop next-ts
                                (cons (cons res proc)
                                      next-procs)
                                (cdr procs)
                                (append evts res)))
                         (else
                          ;; Finished
                          (when cleanup
                            ;; Notificaiton that given proc has terminated
                            (cleanup proc))
                          (loop next-ts
                                next-procs
                                (cdr procs)
                                evts)))))
                (else
                 ;; Not activated, pass on
                 #;(log-debug "not activated next-ts=~a activation=~a"
                            next-ts activation)
                 (define next-next-ts
                   (if (and next-ts
                                (number? activation))
                       (min activation next-ts)
                       (if (number? activation)
                           activation
                           next-ts)))
                 ;(log-debug "  * next-next-ts=~a" next-next-ts)
                 (loop next-next-ts
                       (cons procdef next-procs)
                       (cdr procs)
                       (if (list? activation)
                           (append evts activation)
                           evts))))))))

;; Runs the ticker environment in separate thread and returns control
;; function
(define (run-ticker #:initial-wait (initial-wait #f)
                    #:cleanup (cleanup #f)
                    . init-procs)
  ;; New procedures are (possibly) added at the end of each
  ;; iteration. In the meantime, they are queued here:
  (define control (make-tiqueue))

  ;; Enqueues a single procedure and returns the wrapped procedure
  ;; used for ticking.
  (define (proc-spawner proc)
    (define tickproc (build-ticking-procedure proc))
    (tiqueue-send! control tickproc)
    tickproc)

  ;; Add initial procedures into the queue
  (for ((proc (in-list init-procs)))
    (proc-spawner proc))

  ;; Updated each iteration
  (define clock-timestamp (box #f))

  ;; Used by ticked procedures to get current ts
  (define (get-clock-timestamp)
    (unbox clock-timestamp))

  ;; It is more convenient to have the loop as named procedure here as
  ;; that allows simple passing to separate thread
  ;; constructor. Essentially a tail-recursion loop procedure.
  (define (ticker-loop (current-ts (current-inexact-milliseconds))
                       (iteration 0)
                       (procs '())
                       (wait-for-procs (if initial-wait #f 0)))
    ;; Update clock timestamp
    (set-box! clock-timestamp current-ts)

    ;; Process current procedures
    (define-values (next-ts next-procs evts)
      (ticker-iteration current-ts procs cleanup))
    (define now-ts (current-inexact-milliseconds))

    ;; Compute wait (may be #f)
    (define wait-s (if next-ts
                       (max (/ (- next-ts now-ts) 1000.0) 0.0)
                       (if (null? next-procs)
                           wait-for-procs
                           #f)))

    ;; Check ticking events and procs one as well
    (let ((wait-res (apply sync/timeout/enable-break wait-s (tiqueue-event control) evts)))
      (cond ((tiqueue-ready? control)
             ;; Awaken by command
             (define msg (tiqueue-recv! control))
             (cond ((procedure? msg)
                    ;; Procedure to add
                    (define now2-ts (current-inexact-milliseconds))
                    (ticker-loop now2-ts
                                 (add1 iteration)
                                 (cons (cons now2-ts msg)
                                       next-procs)
                                 wait-for-procs))
                   (else
                    ;; Leave
                    (when cleanup
                      (for ((proc (in-list procs)))
                        (cleanup proc)))
                    (void))))
            ((not wait-res)
             ;; Reached the next-ts - continue only if something there
             (if (not (null? next-procs))
                 (ticker-loop next-ts
                              (add1 iteration)
                              next-procs
                              wait-for-procs)
                 (log-debug "!!! DONE")))
            (else
             ;; Some other event, update timestamp, go on
             (ticker-loop (current-inexact-milliseconds)
                          (add1 iteration)
                          next-procs
                          wait-for-procs)))))

  ;; A new thread for the ticker-loop
  (define ticker-thread
    (parameterize ((current-ticker-spawner proc-spawner)
                   (current-ticker-clock get-clock-timestamp))
      (thread ticker-loop)))

  ;; The control function returned, see individual commands.
  (lambda (cmd . args)
    (case cmd
      ((spawn)
       (apply proc-spawner args))
      ((wait)
       (thread-wait ticker-thread))
      ((break)
       (tiqueue-send! control 'break)
       (thread-wait ticker-thread)))))
