#lang scribble/manual

@(require
  (for-label
    rrll/ticker
    rrll/ticker/env
    rrll/ticker/private/env
    rrll/ticker/queue
    racket/base))

@title{RRLL: Ticker}
@author[@author+email["Dominik Pantůček" "dominik.pantucek@trustica.cz"]]

Racket Rogue-Like Library: Ticker

Continuations-based cooperative scheduler.

@;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
@;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
@;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

@section{Introduction}

The ticker environment provides the facilities required for
cooperative multitasking within single racket @racket[thread]. It
contains the runtime environment @racket[run-ticker], messaging
capability using @racket[tiqueue?] that works both within the
environment and also allows messaging from and to this environment and
environment access module @racketmodname[rrll/ticker/env] that must be
used by the scheduled procedures to coordinate evaluation.

@;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

@section{Ticker Queue}

@defmodule[rrll/ticker/queue]

Special queue that can be used as communication channel between
procedures scheduled by @racket[run-ticker] as well as from outside of
this environment into it and vice-versa.

There should always be only one consumer of the queue that receives
the messages. There may be many producers that send messages to given
queue.

@defproc[(make-tiqueue) tiqueue?]{

Creates a new empty ticker queue.

}

@defproc[(tiqueue? (v any/c)) boolean?]{

Returns @racket[#t] if given argument @racket[v] is an instance of the
ticker queue.

}

@defproc[(tiqueue-send! (q tiqueue?) (v any?)) void?]{

Enqueues given value in the queue and increments its internal
counter. If the queue was empty, its event becomes ready for
synchronization.

}

@defproc[(tiqueue-event (q tiqueue?)) evt?]{

Returns an event that is ready for synchronization when given queue is
not empty.

}

@defproc[(tiqueue-ready? (q tiqueue?)) boolean?]{

Returns @racket[#t] if there is at least one element in the queue.

}

@defproc[(tiqueue-recv! (q tiqueue?)) any?]{

If there is nothing in the queue, does not return until something
becomes available.

When run under @racket[run-ticker] environment, does not actually
block the current thread but rather yields with @racket[(tiqueue-event
q)] which ensures the evaluation is resumed when the queue becomes
ready. Outside of such environment it just blocks the current thread.

Removes the next element from the queue and returns it.

}

@;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

@section{Ticker Environment Core}

@defmodule[rrll/ticker/private/env]

This module contains the @racket[parameter?]s used by the ticker
environment. All of these must not be used directly by other modules
as they are used internally by the ticker environment. All of these
are used by the scheduled procedures through procedures defined in
@racketmodname[rrll/ticker/env].

@defparam[current-ticker-yield yield (or/c #f (-> (or/c real? (listof evt?)) void?)) #:value #f]{

All the procedures scheduled by the ticker runtime environment use
this @racket[parameter?] to suspend its evaluation and yield to other
scheduled procedures.

Outside of the ticker runtime environment, its value is @racket[#f].

Inside the environment this value is @racket[procedure?] accepting
either a number of milliseconds to wait or a list of events. In the
former case the evaluation is suspened for at least given number of
milliseconds - although it may be longer. In the latter case the
evaluation of current procedure is suspended until at least one of the
events becomes ready for synchronization.

}

@defparam[current-ticker-procedure proc (or/c #f (-> void?)) #:value #f]{

Inside any scheduled procedure this @racket[parameter?] holds a unique
value representing this procedure as it is scheduled by the ticker
environment.

This value can be used for identifying the current procedure and the
same value is passed to optional @racket[#:cleanup] argument of the
@racket[ticker-run] procedure.

Outside of the environment its value is @racket[#f].

}

@defparam[current-ticker-spawner proc (or/c #f (-> (-> void?) void?)) #:value #f]{

For all the procedures scheduled within the ticker environment, this
@racket[parameter?] holds a procedure that can add new procedures to
be scheduled. Their evaluation starts immediately when next scheduling
iteration begins.

Outside the environment it is @racket[#f].

}

@defparam[current-ticker-clock proc (or/c #f (-> real?)) #:value #f]{

This @racket[parameter?] holds the procedure that can report the
current internal timestamp derived from
@racket[(current-inexact-milliseconds)] and it can be used for
timestamping.

Outside of the environment it contains @racket[#f].

}

@;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

@section{Ticker Environment}

@defmodule[rrll/ticker/env]

This module provides procedures to use this environment from within
scheduled procedures.

@defproc[(within-ticker?) boolean?]{

Returns @racket[#t] if called from within the
@racket[with-ticker-yield] environment - that is from a procedure that
is currently scheduled under @racket[run-ticker].

}

@defproc[(ticker-yield (duration real?)) void?]{

Yields to other ticked procedures. The duration can either be a number
or a list of events. The evaluation resumes if the duration time has
passed or any of the events become ready for synchronization.

}

@defproc[(ticker-timestamp) inexact-real?]{

  Returns the current internal timestamp in milliseconds of the
  @racket[run-ticker] instance running the caller.

}

@defproc[(ticker-spawn (proc procedure?)) procedure?]{

  Wraps given procedure and immediately spawns it within the current
  @racket[run-ticker] environment. Returns the wrapped procedure.

}

@defproc[(ticker-procedure) procedure?]{

Returns the currently scheduled procedure. It must not be called, but
it may be used as key for registering resources to be cleaned up. See
@racket[run-ticker].

}

@;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

@section{Ticker Scheduler}

@defmodule[rrll/ticker]

The actual scheduler implementation.

@defproc[(run-ticker (#:initial-wait initial-wait boolean? #f)
		     (#:cleanup cleanup boolean? #f)
		     (init-proc procedure?) ...) procedure?]{

Creates and runs a new ticker scheduler in separate
thread. Controlling procedure is returned.

If @racket[initial-wait] is @racket[#t] and no @racket[init-proc]s are
given, the ticker thread waits for the first procedure to be started.

The @racket[cleanup] must be @racket[#f] or a @racket[procedure?]
accepting a single argument that will be called whenever a scheduled
procedure finishes. This can be used for implementing cleanup of
resources created by the scheduled procedure. See
@racket[ticker-this].

}
