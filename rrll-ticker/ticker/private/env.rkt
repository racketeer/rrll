#lang racket/base

(provide current-ticker-yield
         current-ticker-procedure
         current-ticker-spawner
         current-ticker-clock)

;; Parameterized for each ticking procedure, local continuation for
;; suspending this ticking procedure for given amount of ticks.
(define current-ticker-yield (make-parameter #f))

;; Holds the currently ticked procedure for comparison
(define current-ticker-procedure (make-parameter #f))

;; Set by run-ticker to a closure that allows adding procs to the
;; queue from inside of another one.
(define current-ticker-spawner (make-parameter #f))

;; Shared by all ticking procedures, run-ticker uses it to report
;; current timestamp
(define current-ticker-clock (make-parameter #f))
