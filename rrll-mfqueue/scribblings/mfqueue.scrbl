#lang scribble/manual

@title{RRLL: Mutable Future Queue}
@author[@author+email["Dominik Pantůček" "dominik.pantucek@trustica.cz"]]

Racket Rogue-Like Library: Mutable Future Queue

Special type of mutable queue that can be used to send work to running
futures.

@section{Mutable Future Queue}

@defmodule[rrll/mfqueue]

This module contains the whole implementation.

@defproc[(mfqueue? (v any/c)) boolean?]{

A predicate returning @racket[#t] if given value is an instance of
mutable future queue.

}

@defproc[(make-mfqueue) mfqueue?]{

Creates a new, empty @racket[mfqueue?].

}

@defproc[(mfqueue-enqueue! (mfq mfqueue?) (v any/c)) void?]{

Enqueues element in mutable future queue and signals it availability
to the consuming future.

This procedure should be used outside of any future.

}

@defproc[(mfqueue-dequeue! (mfq mfqueue?)) any/c]{

If there are no elements awaiting in the @racket[mfq] mutable future
queue, pauses the consuming future until it is available.

Dequeues the next element from the mutable future queue and returns
it.

This procedure should be used within the consuming future.

}
