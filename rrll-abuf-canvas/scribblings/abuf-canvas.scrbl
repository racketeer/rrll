#lang scribble/manual

@require[scribble-math]

@title[#:style (with-html5 manual-doc-style)]{RRLL: Abuf Canvas}
@author[@author+email["Dominik Pantůček" "dominik.pantucek@trustica.cz"]]

Racket Rogue-Like Library: Converter from Canvas to Abuf.

@section{Finding Pixel Matrix Representation Character}

@defmodule[abuf/partitioning]

@defproc[(quad-find (c0 fixnum?) (c1 fixnum?)
		    (c2 fixnum?) (c3 fixnum?))
	   (values char? fixnum? fixnum?)]{
  Finds the best approximation of given four colors (upper left, upper right, lower left
  and lower right) using a single Unicode quad character and foreground
  and background colors.
}

@defproc[(slc-find (c0 fixnum?) (c1 fixnum?)
	 	   (c2 fixnum?) (c3 fixnum?)
		   (c4 fixnum?) (c5 fixnum?))
	   (values char? fixnum? fixnum?)]{
  Finds the best approximation of given six colors (upper left, upper
  right, middle left, middle right, lower left and lower right) using
  a single Unicode SLC character and foreground and background
  colors.
}

@section{Canvas Display to ANSI Buffer}

@defmodule[abuf/canvas]

@defproc[(abuf-put-canvas/halves! (ab abuf?)
				  (can canvas?)
				  (#:set-dirty set-dirty boolean? #t)) void?]{
    Converts RGB pixels of @racket[canvas] to @racket[dst]
    @racket[ansi-picture?]. Uses Unicode lower half character.

    This procedure assumes the @racket[canvas?] and
    @racket[ansi-picture?] are of appropriate sizes.
}

@defproc[(abuf-put-canvas/quads! (ab abuf?)
			  	 (can canvas?)
			  	 (#:set-dirty set-dirty boolean? #t)) void?]{
    Converts RGB pixels of @racket[canvas] to @racket[dst]
    @racket[ansi-picture?]. Uses Unicode quad symbols.

    This procedure assumes the @racket[canvas?] and
    @racket[ansi-picture?] are of appropriate sizes.
}

@defproc[(abuf-put-canvas/slc! (ab abuf?)
			       (can canvas?)
			       (#:set-dirty set-dirty boolean? #t)) void?]{
    Converts RGB pixels of @racket[canvas] to @racket[dst]
    @racket[ansi-picture?]. Uses Unicode 13.0+ Symbols for Legacy Computing sextant characters.

    This procedure assumes the @racket[canvas?] and
    @racket[ansi-picture?] are of appropriate sizes.
}
