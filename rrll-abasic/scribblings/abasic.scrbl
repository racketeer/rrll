#lang scribble/manual

@require["../abasic/scribble.rkt"
	 scribble/bnf]

@title{RRLL: Actor Basic 2.0}
@author[@author+email["Dominik Pantůček" "dominik.pantucek@trustica.cz"]]

Racket Rogue-Like Library: Actor Basic 2.0

@;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

@section{Introduction}

Actor Basic is a S-Expression language. The basic element of the Actor
Basic language is a S-Expression list with @racket[abasic-lambda] as
its first element and it forms an Actor Basic program. The Racket
representation of such program is then a procedure which accepts a
single argument (the actor), passes it through the steps of the
program which may modify it and eventually returns the final actor as
updated by all the steps performed.

To create modules with Actor Basic programs, use the @racket[s-exp]
meta-language:

@codeblock{
#lang s-exp rrll/abasic/standalone

(define (adder (by 1))
  (method (add n)
    (respond `(+ n by))))
(run
  ($my-adder:= (spawn adder 2))
  (debug "~a + ~a = ~a"
  	 1 2
	 (request $my-adder add 1)))
}

This is a work-in-progress language focused on concepts found both in
actor model, flow-oriented programming and strictly immutable data
structures.

When used ad embedded language the
@racketmodname[rrll/abasic/embedded] module must be used.

@;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

@section{Concepts}

This section clarifies various concepts used in the Actor Basic
documentation. Such clarity is required as certain concepts are
approached differently than the reader might be accustomed to.

@subsubsub*section{Actor}

An Actor is an encapsulation of unit of computation which can be
programmed in imperative-like manner while not requiring shared
mutable state. Actors can communicate only by message passing and can
only update or directly query only their internal state.

@subsubsub*section{Actor State}

The state of each actor consists of two components. Firstly the
current continuation - which can be seen as capturing the current
position in time of this actor's existence - and secondly the data
structure containing such actor's id, auxilliary data, incoming
message queue and registered asynchronous message handlers.

The auxilliary data is just an immutable dictionary with symbols as
keys and any values associated with those keys. These keys are refered
to as actor variables.

@subsubsub*section{Actor Step}

A step is a transition from one actor state to the next one. At the
end of each transaction the actor may suspend its execution or handle
incoming messages.

The whole actor program is a sequence of steps that keep on creating
new actor states as they are performed. Always the latest actor state
is propagated into the actor's containing actor space.

Semantically each step is a function with the following template:

@codeblock{
(define ((step . args) act)
  expr ...)
}

If used without any arguments, no evaluation parentheses are required
in the Actor Basic program:

@codeblock{
  ...
  step
  ...
}

However if arguments are given, they have to be passed as with
ordinary procedure application:

@codeblock{
  ...
  (step 1 2 3)
  ...
}

The result of step evaluation can be anything as it is discarded in
step position. However the same rules apply for expressions where the
result is typically used by further expressions.

@subsubsub*section{Actor Expression}

Any argument to actor steps or special forms is an actor expression.

Such expression can be a literal number, string or symbol.

If it is an identifier, it is assumed to be a procedure with the
following template:

@codeblock{
(define ((proc . args) act)
  expr ...)
}

It is similar to actor step.

An expression can be a actor variable.

In order to evaluate some expression as a Racket expression,
quasiquote must be used. Inside the quasiquote, unquote can be used to
get back to Actor Basic expression evaluation.

@codeblock{
(debug "~v ~v" $pos `(vec2-add ,$pos (vec2 1 0)))
}

This allows for arbitrary nesting of Actor Basic and Racket
expressions.

@subsubsub*section{Actor Variable}

Actor variables are stored in the actor state auxilliary data
dictionary. Each variable is represented by a single key which has to
be a @racket[symbol?]. The associated data can be anything.

In actor steps and expressions the variables can be read by using an
identifier starting with @racket[$] followed by the variable name
(key symbol).

@nested[#:style 'inset]{

If the auxilliary data contains a key @racket['some-var] with value
@racket["Some value"], the following expression returns @racket["Some value"]:

@codeblock{
  $some-var
}

}

The variables can be updated via variable assignment step. The
identifier for such step starts with @racket[$] followed by the
variable name (the key symbol) with @racket[:=] appended to it. It is
used as procedure setting such variable to a new value.

@nested[#:style 'inset]{

The following code updates the @racket[$some-var] variable stored
under the key @racket['some-var] in the actor's auxilliary data.

@codeblock{
  ($some-var:= "Another one")
}

After performing this step, the value stored under given key will be
@racket["Another one"].

}

@subsubsub*section{Actor Message}

Actors can send and receive asynchronous messages (from their
perspective). Each message contains the following fields:

@itemlist[
	@item{code - a @racket[symbol?] specifying the type of the message}
	@item{sender - a reference to originating actor or explicit queue to send reply to}
	@item{kws - keywords for kw-args}
	@item{kw-args - values of kw-args}
	@item{args - a @racket[list?] of arguments}
	@item{return - optional reply message code for request/respond pairs}
	]

@subsubsub*section{Actor Reference}

Any actor can be referenced by its id number and command queue.

@subsubsub*section{Actor Queue}

A thread-safe queue that allows communication both between actors
within the actor space and from/to the actor space.

Actors should wrap the queue usage and if there are no events in the
queue, they should yield with the event that waits for the queue to
become ready.

@subsubsub*section{Actor Method}

A procedure-like construct that is evaluated when given actor receives
a message with given code. This evaluation behaves like
level-triggered interrupt as any other code evaluation of the actor in
question is suspended until the method finishes.

@subsubsub*section{Actor Space}

Contains references to current states of all actors.

The actor's internal state is always updated in current-actorspace
registry upon any update. This update is synchronous and it is safe to
query the actorspace from other threads.

@subsubsub*section{Actor Inheritance}

@codeblock{
#lang s-exp rrll/abasic/standalone

(define (adder)
  (method (get-name)
    "Adder")
  (method (add a b)
    `(+ a b)))

(define (strange-adder (bias 0))
  (adder)
  (method (add a b)
    `(+ a b bias)))
}

@;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

@section{Front-End Modules}

This section describes the modules that should be primarily used for
programs written in Actor Basic.

@subsection{Standalone Actor Basic}

@defmodule[rrll/abasic/standalone]

This module should be used when writing complete modules in Actor
Basic. It should be used with @racketmodname[s-exp] meta-language.

@codeblock{
#lang s-exp rrll/abasic/standalone

...
}

@subsection{Embedded Actor Basic}

@defmodule[rrll/abasic/embedded]

This module should be used when defining Actor Basic programs within
Racket modules.

@codeblock{
#lang racket/base

(require rrll/abasic/embedded)

...
}

@;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
@;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
@;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

@section{Data Structures}

This section describes all the internal data structures used by Actor
Basic.

@subsection{Core Actor State and Reference}

@defmodule[rrll/abasic/actor-struct]

@defstruct*[%actor
	((id fixnum?)
	 (data (and/c immutable/c (hash/c symbol? any/c)))
	 (queue %queue?)
	 (methods (and/c immutable/c (hash/c symbol? procedure?))))]{

Data structure representing a running actor.

}

@defproc[(%actor-put (act %actor?) (key symbol?) (value any?)) %actor?]{

Updates the actor's auxilliary data by setting or changing the value
under given key to the value provided.

}

@defproc*[(((%actor-get (act %actor?) (key symbol?)) any/c)
           ((%actor-get (act %actor?) (key symbol?) (val any/c)) any/c))]{

Gets the value associated with given @racket[key] in the actor's
auxilliary data. The second form provides default value. If the first
form is used and the key is not present, an exception is raised which
effectively terminates the actor.

}

@defproc[(%actor-del (act %actor?) (key symbol?)) %actor?]{

Removes given key from actor's auxilliary data.

}

@defproc[(%actor-has? (act %actor?) (key symbol?)) boolean?]{

Returns @racket[#t] if given actor contains auxilliary data under
given @racket[key].

}

@defstruct*[%ref
	((id fixnum?)
	 (queue %queue?))]{

Represents a reference to actor.

}

@defproc[(%actor->%ref (act %actor?)) %ref?]{

Creates an actor reference from given actor.

}

@;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

@subsection{Actor State Interaction}

@defmodule[rrll/abasic/actor]

@defproc[(make-%actor (id fixnum?)) %actor?]{

Creates a new @racket[%actor?] struct with given @racket[id].

}

@defproc[(%actor-send! (act (or/c %actor? %ref?))
		       (msg %msg?)) void?]{

Sends a message to given actor.

}

@defproc[(%actor-recv! (act %actor?)) any/c]{

Blocking receive of message on given actor's command queue.

}

@defproc[(register-%actor-method (act %actor?)
				 (sym symbol?)
				 (proc (->* () () #:rest list? (-> %actor? %actor?))))
				 void?]{

Registers a new actor method. The method can accept any number of both
keyword and positional arguments and must return a procedure that
performs the transition of the actor to the next state.

Must be used from within the @racket[method] step form.

}

@defproc[(unregister-%actor-method (act %actor?)
				   (sym symbol?))
				   %actor?]{

Removes given symbol from actor's method table.

}

@defproc[(evaluate-%actor-method (act %actor?)
				 (msg %msg?))
				 %actor?]{

Evaluates given actor method within the context of that actor and
returns its new state. The method symbol is contained in message code.

}

@defproc[(maybe-recv+evaluate-%actor-method (act %actor)) %actor?]{

Checks whether there is an incoming message waiting and if so,
receives it and evaluates appropriate method.

}

@;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

@subsection{Actor Queue Implementation}

@defmodule[rrll/abasic/queue]

This module implements the Actor Basic queue concept that can be used
for sending messages to/from actors in their space and also between
actors within that space.

@defstruct*[%queue
	((queue queue?)
	 (lock semaphore?)
	 (sema semaphore?)
	 ((active? #:mutable) boolean?))]{

Internal actor queue struct.

}

@defproc[(make-queue) %queue?]{

Creates new empty Actor Queue struct.

}

@defproc[(%queue-send! (q %queue?) (v any/c)) void?]{

Enqueues given value in the queue and increments its internal
counter. If the queue was empty, its event becomes ready for
synchronization.

}

@defproc[(%queue-evt (q %queue?)) evt?]{

Returns an event that is ready for synchronization when given queue is
not empty.

The event returned should be used inside Actor Space environment if
the queue is not ready.

}

@defproc[(%queue-ready? (q %queue?)) boolean?]{

Returns @racket[#t] if there is at least one element in the queue.

}

@defproc[(%queue-recv! (q %queue?)) any/c]{

If there is nothing in the queue, does not return until something
becomes available.

Removes the next element from the queue and returns it.

}

@defproc[(%queue-close! (q %queue?)) void?]{

Makes given queue inactive.

}

@;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

@subsection{Actor Events}

@defmodule[abasic/events]

This module provides convenient @racket[evt?] wrappers to be used by
actor space for synchonizing actors' computation. Wrapping events as
custom @racket[struct] allows for better displaying in actor monitor.

All the events return the waiting actor's id upon successfull
synchronization.

@defstruct*[%evt
	((evt evt?)
	 (actor-id fixnum?)
	 (kind symbol?)
	 (label (or/c #f string?)))]{

The actual struct used for event handling within actor space.

}

@defproc[(make-%actor-alarm-%evt (act %actor?)
				 (ms positive?))
				 %evt?]{

Returns a synchronizable event that expires @racket[ms] milliseconds
from now.

}

@defproc[(make-%actor-always-%evt (act %actor?)) %evt?]{

Returns a synchronizable event that is always ready.

}

@defproc[(make-id-always-%evt (id fixnum?)) %evt?]{

Returns a synchronizable event that is always ready. Explicit actor id
must be given.

}

@defproc[(make-%actor-%queue-%evt (act %actor?)
				  (queue %queue?))
				  %evt?]{

Returns a synchronizable event that is ready for synchronization when
the given queue is not empty.

}

@;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

@subsection{Actor Message Struct}

@defmodule[abasic/msg]

@defstruct*[%msg
	((code symbol?)
	 (sender (or/c %ref? #f))
	 (kws (listof keyword?))
	 (kw-args list?)
	 (args list?)
	 (return (or/c symbol? #f))
	 )]{

All messages passed between actors are represented by this struct.

@itemlist[
  @item{@racket[code] - represents the message type}
  @item{@racket[sender] - originating actor (if applicable)}
  @item{@racket[kws] - list of keywords}
  @item{@racket[kw-args] - a list of keyword argument values}
  @item{@racket[args] - positional arguments}
  @item{@racket[return] - symbol with return method handler}
  ]

No support for keyword arguments is provided.

}

@defproc[((make-%msg (code symbol?)
		     (#:return return (or/c symbol? #f) #f)
	 	     (#:sender sender (or/c %actor? %ref #f) #f)
	 	     (#:queue queue (or/c %queue? #f) #f))
	 	     (args any/c) ...) %msg?]{

Creates new message suitable for @racket[%actor-send!]. It captures
all positional arguments as well as keyword arguments.

}

@defproc[(%msg-rest (msg %msg?)) %msg?]{

Returns a message created by removing first positional argument of
given message.

}

@defproc[(%msg-first (msg %msg?)) any/c]{

Returns the first positional argument from given message.

}

@defproc[(count-%msg-args (msg %msg?)) fixnum?]{

Returns the number of positional arguments contained in given message.

}

@;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

@subsection{Actor Steps}

@defmodule[rrll/abasic/steps]

This module covers the basic steps provided to Actor Basic
programs. Procedures may return arbirary value - including procedures
that modify the actor state.

@defthing[%step? contract? #:value (-> %actor? %actor?)]{

Not an actual @racket[contract?], but used in this documentation for
reference. It is the step value after applying any arguments.

}

@defstep[(sleep (ms positive-real?))]{

Yields the actor with @racket[alarm-evt?] set to become ready for
synchronization @racket[ms] milliseconds from now.

}

@defstep[(break)]{

Terminates the computation of current actor which also gets removed
from the current actor space.

}

@defstep[(debug (fmt string?)
		(arg any/c) ...)]{

Sends debug message to @racket[current-logger] and returns actor
unchanged.

}

@defproc[(avail? (ref %ref?)) boolean?]{

Returns true if given reference is a reference to a still existing
actor.

}

@;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

@subsection{Actor Space and Time}

@defmodule[rrll/abasic/space]

This module provides the evaluation environment for interacting
actors.

@defstruct*[%space
	((last-id fixnum?)
	 (actors (and/c (hash/c fixnum? %actor?)
	 	 	immutable?))
	 (receiver (or/c log-receiver? #f))
	 (printer (or/c (-> vector? void?) #f)))]{

A structure representing the current state of actor space with all
actors structs in the current state. Technically speaking it is a
snapshot of the actor spacetime at given continuation (time).

}

@defproc[(make-%space) %space?]{

Creates a new, empty actor space.

}

@defproc[(prepare-actor (space-box (box/c %space?))
			(proc (-> any/c ...
			      	  (-> %actor? any)))
			(arg any) ...)
			(values fixnum?
				(-> (or/c evt? #f))
				evt?)]{

Prepares actor for ticking. A new id is allocated in the actor space
that will perform the scheduling. All arguments - including keyword
arguments are passed on to proc upon first invocation.

Returns three values:

@itemlist[
  @item{an actor id}
  @item{the actor procedure that performs its next step and yields event or false}
  @item{the initial event - which is a wrapped @racket[always-evt].}
  ]

}

@defproc[((run-actor (main (-> (arg any/c) ... (-> %actor? any)))
		     (#:space-box space-box (box/c %space?) (box (make-%space)))
		     (#:monitor monitor (or/c (-> (box/c %space?) boolean?) #f) #f))
		     (arg any/c) ...)
		     void?]{

Creates and runs a new actor space with given main actor as initial
actor. Inner evaluation creates procedure that can be evaluated with
any arguments which will be in turn passed to the main actor of the
space created.

It automatically spawns the NSA actor.

}

@;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
@;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
@;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

@section{Actor Basic Expander}

@defmodule[rrll/abasic/expander]

Grammar for Actor Basic programs is as follows:

@(define open @litchar{(})
@(define close @litchar{)})

@BNF[
  (list @nonterm{program}
  	@BNF-seq[open
		 @litchar{actor-lambda}
		 @nonterm{maybe-name}
		 open
		 @nonterm{argdefs}
		 close
		 @nonterm{steps}
		 close])
  (list @nonterm{maybe-name}
	@elem{}
  	@nonterm{identifier})
  (list @nonterm{argdefs}
  	@elem{}
	@BNF-seq[@nonterm{argdef} @nonterm{argdefs}])
  (list @nonterm{argdef}
  	@nonterm{identifier}
	@BNF-seq[open @nonterm{identifier} @nonterm{racket-expr} close]
	@BNF-seq[@nonterm{kw} @nonterm{identifier}]
	@BNF-seq[@nonterm{kw} open @nonterm{identifier} @nonterm{racket-expr} close])
  (list @nonterm{kw}
  	@elem{@racket[keyword?]})
  (list @nonterm{identifier}
  	@elem{valid Racket @racket[identifier?]})
  (list @nonterm{racket-expr}
  	@elem{valid Racket expression})
  (list @nonterm{steps}
  	@elem{}
	@BNF-seq[@nonterm{step} @nonterm{steps}])
  (list @nonterm{step}
  	@elem{Actor Basic step})
]

Actor Basic programs body is just a series of steps with each step
conforming to the following grammar:

@BNF[
  (list @nonterm{step}
  	@nonterm{identifier}
	@BNF-seq[open @litchar{spawn} @nonterm{identifier} @nonterm{args} close]
	@BNF-seq[open @litchar{loop} @nonterm{identifier} @nonterm{steps} close]
	@BNF-seq[open @nonterm{identifier} @nonterm{args} close]
	@BNF-seq[open @nonterm{setter} @nonterm{expr} close]
	@BNF-seq[open @litchar{method} open @nonterm{identifier} @nonterm{argdefs} close @nonterm{steps} close]
	@BNF-seq[open @litchar{send} @nonterm{expr} @nonterm{identifier} @nonterm{args} close]
	@BNF-seq[open @litchar{reply} @nonterm{identifier} @nonterm{args} close]
	@BNF-seq[open @litchar{respond} @nonterm{arg} close]
	@BNF-seq[open @litchar{delete} @nonterm{variable} close]
	@BNF-seq[open @litchar{atomic} @nonterm{steps} close]
	@BNF-seq[open @litchar{when} @nonterm{expr} @nonterm{steps} close]
	@BNF-seq[open @litchar{register} @nonterm{identifier} close]
	@BNF-seq[open @litchar{let} open @nonterm{aletpairs} close @nonterm{steps} close]
	@BNF-seq[open @litchar{case} @nonterm{expr} @nonterm{stepcases} close]
	)
  (list @nonterm{args}
  	@elem{}
	@BNF-seq[@nonterm{arg} @nonterm{args}])
  (list @nonterm{arg}
  	@nonterm{expr}
	@BNF-seq[@nonterm{kw} @nonterm{expr}])
  (list @nonterm{expr}
  	@elem{Actor Basic expression})
  (list @nonterm{setter}
  	@elem{an @racket[identifier?] starting with @racket[$] and ending with @racket[:=] like @racket[$id:=]})
  (list @nonterm{aletpairs}
  	@elem{}
	@BNF-seq[@nonterm{aletpair} @nonterm{aletpairs}])
  (list @nonterm{aletpair}
  	@BNF-seq[open @nonterm{identifier} @nonterm{expr} close])
  (list @nonterm{stepcases}
  	@elem{}
	@BNF-seq[@nonterm{stepcase} @nonterm{stepcases}])
  (list @nonterm{stepcase}
  	@BNF-seq[open @nonterm{primitive} @nonterm{steps} close]
	@BNF-seq[open open @nonterm{primitives} close @nonterm{steps} close])
  (list @nonterm{primitives}
  	@elem{}
	@BNF-seq[@nonterm{primitive} @nonterm{primitives}])
]

Actor Basic expressions use the following grammar:

@BNF[
  (list @nonterm{expr}
  	@nonterm{primitive}
	@nonterm{variable}
	@BNF-seq[open @litchar{spawn} @nonterm{identifier} @nonterm{args} close]
	@nonterm{identifier}
	@BNF-seq[open @litchar{request} @nonterm{expr} @nonterm{identifier} @nonterm{args} close]
	@litchar{sender}
	@litchar{sender/id}
	@nonterm{racket-esc}
	@BNF-seq[open @litchar{lookup} @nonterm{identifier} close]
	@nonterm{named-actor}
	)
  (list @nonterm{primitive}
  	@nonterm{@racket[number?] literal}
	@nonterm{@racket[string?] literal}
	@nonterm{@racket[boolean?] literal}
	@BNF-seq{open @litchar{quote} @nonterm{identifier} close})
  (list @nonterm{variable}
  	@elem{an @racket[identifier?] starting with @racket[$] like @racket[$id]})
  (list @nonterm{racket-esc}
  	@BNF-seq[open @litchar{quasiquote} @nonterm{racket-esc-expr} close]
	@BNF-seq[@litchar{`} @nonterm{racket-esc-expr}])
  (list @nonterm{racket-esc-expr}
  	@BNF-seq[open @litchar{unquote} @nonterm{expr} close]
	@BNF-seq[@litchar{,} @nonterm{expr}]
	@nonterm{racket-expr}
	@BNF-seq[open @nonterm{racket-esc-expr} @nonterm{maybe-racket-esc-expr} close])
  (list @nonterm{maybe-racket-esc-expr}
  	@elem{}
	@nonterm{racket-esc-expr})
  (list @nonterm{named-actor}
  	@elem{@racket[identifier?] starting with @racket[!] like @racket[!actor]})
]

@defform[(define-actor (name arg ...) step ...)]{

Wrapper around @racket[define] and @racket[actor-lambda].

}

@defform[(define/provide-actor (name arg ...) step ...)]{

Like @racket[define-actor] but also @racket[provide]s the new binding
immediately.

}

@defform[(actor-lambda maybe-name (arg ...) step ...)]{

Compiles Actor Basic program. If @racket[maybe-name] is provided, the
expander uses that name for error reporting and monitoring.

}

@;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

@section{Scribbling Steps}

@defmodule[rrll/abasic/scribble]

This module can be used to easily write scribblings of Actor Basic
Steps.

@defform[(defstep prototype pre-flow ...)]{

Wrapper around @racket[defproc] that sets its @racket[#:kind] to
@racket["step"] and ensures the return type is @racket[abasic-step?].

}
