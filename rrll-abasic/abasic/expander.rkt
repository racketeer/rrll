#lang racket/base

(require (for-syntax racket/base
                     syntax/parse
                     syntax/parse/lib/function-header)
         racket/match
         "actor.rkt"
         "steps.rkt"
         "msg-struct.rkt"
         "msg.rkt")

(provide define-actor
         define/provide-actor
         actor-lambda
         named-actor-lambda

         (struct-out %actor-proc)

         loop
         spawn
         method
         method*
         send
         send*
         reply
         request
         respond
         sender
         sender/id
         delete
         atomic
         register
         lookup
         broadcast
         subscribe
         unsubscribe

         let
         cond
         case)

(define-syntax loop #'-loop)
(define-syntax spawn #'-spawn)
(define-syntax method #'-method)
(define-syntax method* #'-method*)
(define-syntax send #'-send)
(define-syntax send* #'-send*)
(define-syntax reply #'-reply)
(define-syntax request #'-request)
(define-syntax respond #'-respond)
(define-syntax sender #'-sender)
(define-syntax sender/id #'-sender/id)
(define-syntax delete #'-delete)
(define-syntax atomic #'-atomic)
(define-syntax register #'-register)
(define-syntax lookup #'-lookup)
(define-syntax nsa #'-nsa)
(define-syntax broadcast #'-broadcast)
(define-syntax subscribe #'-subscribe)
(define-syntax unsubscribe #'-unsubscribe)

(begin-for-syntax

  ;; Represents an auxilliary data setter
  (define-syntax-class setter
    #:attributes (variable)
    (pattern sid:identifier
             #:do ((define sidstr (symbol->string (syntax->datum #'sid)))
                   (define sidmat (regexp-match #rx"^[$](.+):=$" sidstr)))
             #:when sidmat
             #:attr variable (datum->syntax #'sid (string->symbol (cadr sidmat)))
             ))

  ;; Represents a "variable" (auxilliary data getter)
  (define-syntax-class variable
    #:attributes (name)
    (pattern (~and var:identifier
                   (~not sid:setter))
             #:do ((define varstr (symbol->string (syntax->datum #'var)))
                   (define varmat (regexp-match #rx"^[$](.+)$" varstr)))
             #:when varmat
             #:attr name (datum->syntax #'var (string->symbol (cadr varmat)))))

  ;; Convenience syntax to name actors directly using !name naming
  ;; convention
  (define-syntax-class namedactor
    #:attributes (name)
    (pattern na:identifier
             #:do ((define nastr (symbol->string (syntax->datum #'na)))
                   (define namat (regexp-match #rx"^[!](.+)$" nastr)))
             #:when namat
             #:attr name (datum->syntax #'na (string->symbol (cadr namat)))))

  ;; Used for keeping track of current context during expansion
  (define-syntax-class actorctx
    #:attributes (name actbox bindings msg atomic)
    (pattern (name actbox bindings msg atomic)))

  ;; Actor let pair
  (define-syntax-class aletpair
    #:attributes (id ex)
    (pattern (id:identifier ex:expr)))

  )

;; Transforms the racket expression into the same racket expression
;; while keeping track of actor state and bindings for unquoting
;; (unescaping) into actor basic bindings again.
(define-syntax (compile-racket-expr stx)
  (syntax-parse stx
    ;; Unquote to Actor Basic expressions
    ((_ ctx:actorctx ((~literal unquote) ex))
     #'(compile-actor-expr ctx ex))

    ;; Quoted symbol
    ((_ ctx:actorctx ((~literal quote) ex))
     #''ex)

    ;; Explicit literal lambda
    ((_ ctx:actorctx ((~and (~literal lambda) the-lambda) (arg ...) ex ...))
     #'(the-lambda (arg ...) (compile-racket-expr ctx ex) ...))

    ;; Explicit for
    ((_ ctx:actorctx ((~literal for) ((id ex) ...) expr ...))
     #'(for ((id (compile-racket-expr ctx ex)) ...)
         (compile-racket-expr ctx expr) ...))

    ;; Explicit literal let
    ((_ ctx:actorctx ((~literal let) name:identifier ((id:identifier letex:expr) ...) ex ...))
     #'(let name ((id (compile-racket-expr ctx letex)) ...)
         (compile-racket-expr ctx ex) ...))

    ;; Generic macro transformers
    ((_ ctx:actorctx (transformer:identifier ex ...))
     #:when (syntax-local-value #'transformer (lambda () #f))
     #'(transformer (compile-racket-expr ctx ex) ...))

    ;; More racket expressions
    ((_ ctx:actorctx (ex ...))
     #'((compile-racket-expr ctx ex) ...))

    ;; Primitive expression
    ((_ ctx:actorctx ex)
     #'ex)))

;; Compiles conditions within actor basic cond expression
(define-syntax (compile-actor-cond-expr stx)
  (syntax-parse stx
    ;; Last cond
    ((_ ctx:actorctx)
     #'(void))

    ;; Make any conds after else a syntax error
    ((_ ctx:actorctx ((~literal else) ex)
        more ...+)
     (raise-syntax-error
      #f "conds after else"
      #'((else ex) more ...)))

    ;; Default else branch - must be the last one
    ((_ ctx:actorctx ((~literal else) ex))
     #'(compile-actor-expr ctx ex))

    ;; Tree of ifs
    ((_ ctx:actorctx (condition ex ...)
        more ...)
     #'(if (compile-actor-expr ctx condition)
           (compile-actor-exprs ctx ex ...)
           (compile-actor-cond-expr ctx more ...)))
    ))

;; Compiles case expressions
(define-syntax (compile-actor-case-expr stx)
  (syntax-parse stx
    ((_ ctx:actorctx the-val)
     #'(void))
    ((_ ctx:actorctx the-val ((~literal else) ex))
     #'(compile-actor-case-expr ex))
    ((_ ctx:actorctx the-val ((~literal else) ex) more ...+)
     (raise-syntax-error
      #f "cases after else"
      #'((else ex) more ...)))
    ((_ ctx:actorctx the-val ((token ...) ex ...) more ...)
     #'(if (memq the-val '(token ...))
           (compile-actor-exprs ctx ex ...)
           (compile-actor-case-expr ctx the-val more ...)))
    ((_ ctx:actorctx the-val (token ex ...) more ...)
     #'(if (eq? 'token the-val)
           (compile-actor-exprs ctx ex ...)
           (compile-actor-case-expr ctx the-val more ...)))))

;; Introduces local bindings one-by-one and adds them to tracked
;; bindings of the currently expanded expression.
(define-syntax (compile-actor-let-expr stx)
  (syntax-parse stx
    ;; No more bindings are introduced
    ((_ ctx:actorctx () expr ...)
     #'(compile-actor-exprs ctx expr ...))

    ;; A binding gets introduced
    ((_ ctx:actorctx (lp:aletpair more:aletpair ...) expr ...)
     (with-syntax (((binding ...) #'ctx.bindings))
       (syntax/loc stx
         (let ((lp.id (compile-actor-expr ctx lp.ex)))
           (compile-actor-let-expr
            (ctx.name ctx.actbox (lp.id binding ...) ctx.msg ctx.atomic)
            (more ...)
            expr ...)))))
    ))

;; Handles an argument list and evaluates all expressions as actor
;; basic expressions. The result should usually be spliced into the
;; procedure evaluation expression.
(define-for-syntax (compile-actor-args stx)
  (syntax-parse stx
    ((_ ctx:actorctx
        (~or (~seq kw:keyword kwexpr:expr)
             expr:expr) ...)
     (let* ((kws (syntax->list #'(kw ...)))
            (kwexprs (syntax->list #'((compile-actor-expr ctx kwexpr) ...)))
            (kw+exprs (apply append
                             (for/list ((kw kws)
                                        (kwexpr kwexprs))
                               (list kw kwexpr)))))
       (with-syntax (((kwargs ...) kw+exprs))
         #'(kwargs ... (compile-actor-expr ctx expr) ...))))))

;; Compiles actor basic expression (not in a step position)
(define-syntax (compile-actor-expr stx)
  (syntax-parse stx
    ;; Literals
    ((_ ctx:actorctx
        (~and (~or num:number
                   str:string
                   bool:boolean
                   ch:char)
              val))
     #'val)

    ;; Quoted symbol
    ((_ ctx:actorctx
        ((~literal quote) id))
     #''id)

    ;; Variable getter identifier: $id
    ((_ ctx:actorctx var:variable)
     #'(%actor-box-get ctx.actbox 'var.name))

    ;; Variable setter expression: ($id:= val)
    ((_ ctx:actorctx (step:setter arg))
     #'(%actor-box-put! ctx.actbox 'step.variable (compile-actor-expr ctx arg)))

    ;; Variable remover expression: (delete $var)
    ((_ ctx:actorctx ((~literal delete) var:variable))
     #'(%actor-box-del! ctx.actbox 'var.name))

    ;; Named loop expression
    ((_ ctx:actorctx ((~literal loop) label expr ...))
     #'(let label-let ()
         (define ((label) act-box-unused)
           (label-let))
         ;; Disable atomicity at loop start
         (compile-actor-exprs (ctx.name ctx.actbox ctx.bindings ctx.msg #f) expr ...)))

    ;; Atomic sequence of expressions without yielding and receiving
    ((_ ctx:actorctx ((~literal atomic) expr ...))
     #'(compile-actor-exprs
        (ctx.name ctx.actbox ctx.bindings ctx.msg #t)
        expr ...))

    ;; Conditional expressions - when
    ((_ ctx:actorctx ((~literal when) cexpr expr ...))
     #'(when (compile-actor-expr ctx cexpr)
         (compile-actor-exprs ctx expr ...)))

    ;; Spawning as expression
    ((_ ctx:actorctx ((~literal spawn) proc arg ...))
     #`((make-keyword-procedure
         (lambda (kws kw-args . args)
           ((%actor-box-yield ctx.actbox) (list 'spawn proc kws kw-args args))))
        #,@(compile-actor-args #'(_ ctx arg ...))))

    ;; Method definition: (method (name arg ...) expr ...)
    ((_ ctx:actorctx ((~literal method)
                      func:function-header
                      expr ...))
     (with-syntax (((binding ...) #'ctx.bindings)
                   ((mbinding ...) #'func.params))
       (quasisyntax/loc stx
         (register-%actor-box-method!
          ctx.actbox
          'func.name
          (procedure-rename
           (lambda (msg)
             (match-define (%msg code sender kws kw-args args return) msg)
             (keyword-apply
              (lambda #,(cdr (syntax-e #'func))
                (procedure-rename
                 (lambda (act)
                   (compile-actor-exprs
                    (func.name act (binding ... mbinding ...) msg #f)
                    expr ...))
                 'func.name))
              kws kw-args args))
           'func.name)))))

    ;; Method definition: (method* (name msg) expr ...)
    ((_ ctx:actorctx ((~literal method*)
                      (name:identifier msg:identifier)
                      expr ...))
     (with-syntax (((binding ...) #'ctx.bindings))
       (quasisyntax/loc stx
         (register-%actor-box-method!
          ctx.actbox
          'name
          (procedure-rename
           (lambda (msg)
             (match-define (%msg code sender kws kw-args args return) msg)
             (procedure-rename
              (lambda (act)
                (compile-actor-exprs
                 (name act (binding ... msg) msg #f)
                 expr ...))
              'name))
           'name)))))

    ;; Conditional evaluation: if
    ((_ ctx:actorctx ((~literal if) condition truex falsex))
     #'(if (compile-actor-expr ctx condition)
           (compile-actor-expr ctx truex)
           (compile-actor-expr ctx falsex)))

    ;; Conditional evaluation: cond
    ((_ ctx:actorctx ((~literal cond) ex ...))
     #'(compile-actor-cond-expr ctx ex ...))

    ;; Conditional evaluation: case
    ((_ ctx:actorctx ((~literal case) val cases ...))
     #'(let ((the-val (compile-actor-expr ctx val)))
         (compile-actor-case-expr ctx the-val cases ...)))
    
    ;; Local bindings expression
    ((_ ctx:actorctx ((~literal let) (lp:aletpair ...) expr ...))
     (syntax/loc stx
       (compile-actor-let-expr ctx (lp ...) expr ...)))

    ;; Request as expression
    ((_ ctx:actorctx ((~literal request) actor method-id arg ...))
     #`(let/cc cc
         ;; Unique "method" that is continuation of this expression
         (define return (gensym))

         ;; Send the message (add it to the queue of the target actor)
         (%actor-send! (compile-actor-expr ctx actor)
                       ((make-%msg 'method-id
                                   #:sender (%actor-box->%ref ctx.actbox)
                                   #:return return)
                        ;arg ...
                        #,@(compile-actor-args #'(_ ctx arg ...))))
         
         ;; Evaluate do-nothing-loop until we can continue
         (register-%actor-box-method!
          ctx.actbox
          return
          (lambda (msg)
            (match-define (%msg code sender _ _ (list res) _) msg)
            (lambda (actbox)
              ;; actbox can only be the same as ctx.actbox here
              (unregister-%actor-box-method! actbox return)
              (cc res))))
         ((dnl #t) ctx.actbox)))

    ;; Send expression: (send $actor method-id arg ...)
    ((_ ctx:actorctx ((~literal send) actor method-id arg ...))
     #`(%actor-send! (compile-actor-expr ctx actor)
                     ((make-%msg 'method-id
                                 #:sender (%actor-box->%ref ctx.actbox))
                      #,@(compile-actor-args #'(_ ctx arg ...)))))

    ;; Send* expression: (send $actor msg)
    ((_ ctx:actorctx ((~literal send*) actor msg))
     #`(%actor-send! (compile-actor-expr ctx actor)
                     msg))

    ;; Reply expression: (reply method-id arg ...)
    ((_ ctx:actorctx ((~literal reply) method-id arg ...))
     #'(%actor-send! (%msg-sender ctx.msg)
                     ((make-%msg 'method-id
                                 #:sender (%actor-box->%ref ctx.actbox))
                      arg ...)
                     #t))

    ;; Respond expression: (respond expr)
    ((_ ctx:actorctx ((~literal respond) arg))
     #'(%actor-send! (%msg-sender ctx.msg)
                     ((make-%msg (%msg-return ctx.msg)
                                 #:sender (%actor-box->%ref ctx.actbox))
                      (compile-actor-expr ctx arg))
                     #t))

    ;; Special NSA
    ((_ ctx:actorctx (~literal nsa))
     #'((%actor-box-yield ctx.actbox) 'nsa))

    ;; Register with NSA
    ((_ ctx:actorctx ((~literal register) name))
     #'(%actor-send! ((%actor-box-yield ctx.actbox) 'nsa)
                     ((make-%msg 'register #:sender (%actor-box->%ref ctx.actbox)) 'name)))

    ;; Lookup actor expression
    ((_ ctx:actorctx ((~literal lookup) name))
     #'(compile-actor-expr ctx (request nsa lookup 'name)))

    ;; Named actor
    ((_ ctx:actorctx actor:namedactor)
     #'(compile-actor-expr ctx (lookup actor.name)))

    ;; Subscribe to a message bus
    ((_ ctx:actorctx ((~literal subscribe) name))
     #'(compile-actor-expr ctx (send !ambus subscribe 'name)))

    ;; Unsubscribe from a message bus
    ((_ ctx:actorctx ((~literal unsubscribe) name))
     #'(compile-actor-expr ctx (send !ambus unsubscribe 'name)))
    ((_ ctx:actorctx (~or ((~literal unsubscribe))
                          (~literal unsubscribe)))
     #'(compile-actor-expr ctx (send !ambus unsubscribe)))

    ;; Broadcast message to given group on message bus
    ((_ ctx:actorctx ((~literal broadcast) name method arg ...))
     #'(compile-actor-expr ctx (send !ambus broadcast 'name 'method arg ...)))

    ;; Escaping to Racket expressions
    ((_ ctx:actorctx ((~literal quasiquote) ex))
     #'(compile-racket-expr ctx ex))

    ;; actor-lambda bindings
    ((_ ctx:actorctx id:identifier)
     #:when (member #'id (syntax->list #'ctx.bindings) bound-identifier=?)
     #'id)

    ;; Special values
    ((_ ctx:actorctx (~literal sender))
     #'(%msg-sender ctx.msg))
    ((_ ctx:actorctx (~literal sender/id))
     #'(%ref-id (%msg-sender ctx.msg)))

    ;; Identifier (partial) application
    ((_ ctx:actorctx (id:identifier arg ...))
     #`((id #,@(compile-actor-args #'(_ ctx arg ...))) ctx.actbox))

    ;; Non-local identifier
    ((_ ctx:actorctx id:identifier)
     #'((id) ctx.actbox))
    ))

;; Wrapper with inspection information
(struct %actor-proc
  (proc kind name src)
  #:property prop:procedure 0
  #:methods gen:custom-write
  ((define (write-proc v p m)
     (match-define (%actor-proc _ kind name _) v)
     (display
      (format
       "#<~a:~a>"
       kind name) p))))

;; A simple wrapper to compile multiple expressions
(define-syntax (compile-actor-exprs stx)
  (syntax-parse stx
    ((_ ctx:actorctx expr ...)
     #'(begin
         (void)
         (begin
           (compile-actor-expr ctx expr)
           (when (not ctx.atomic)
             (yield)
             (maybe-recv+evaluate-%actor-box-method! ctx.actbox))) ...))))

;; The main entry for compiling named Actor Basic programs
(define-syntax (named-actor-lambda stx)
  (syntax-parse stx
    ((_ name args:formals step ...)
     (quasisyntax/loc stx
       (procedure-rename
        (lambda args
          (lambda (act (dnl? #f))
            (compile-actor-exprs
             (name act (#,@#'args.params dnl?) #f #f)
             step ...
             (dnl dnl?))))
        'name)))))

;; Anonymous Actor Basic program
(define-syntax (actor-lambda stx)
  (syntax-parse stx
    ((_ (arg ...) step ...)
     (syntax/loc stx
       (named-actor-lambda #f (arg ...) step ...)))))

;; Convenience define with name
(define-syntax (define-actor stx)
  (syntax-parse stx
    ((_ func:function-header step ...)
     (quasisyntax/loc stx
       (define func.name
         (%actor-proc
          (named-actor-lambda func.name #,(cdr (syntax-e #'func)) step ...)
          'actor
          'func.name
          #'(#,stx)
          ))))))

;; Convenience define with name and immediate provide
(define-syntax (define/provide-actor stx)
  (syntax-parse stx
    ((_ func:function-header step ...)
     (quasisyntax/loc stx
       (begin
         (define func.name
           (%actor-proc
            (named-actor-lambda func.name #,(cdr (syntax-e #'func)) step ...)
            'actor
            'func.name
            #'(#,stx)
            ))
         (provide func.name))))))
