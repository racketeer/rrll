#lang racket/base

(require (for-syntax racket/base)
         scribble/manual)

(provide defstep)

(define-syntax (defstep stx)
  (syntax-case stx ()
    ((_ (name . args) . doc)
     #'(defproc #:kind "step" (name . args) %step? . doc))))
