#lang racket/base

(require racket/match
         racket/string)

(provide (struct-out %msg))

(struct %msg
  (code sender kws kw-args args return)
  #:methods gen:custom-write
  ((define (write-proc v p m)
     (match-define (%msg code sender kws kw-args args return) v)
     (display
      (format
       "#<msg ~a ~v ~v ~a~a>"
       code sender args
       (string-join
        (for/list ((kw (in-list kws))
                   (kw-arg (in-list kw-args)))
          (format "~v=~v" kw kw-arg))
        " ")
       (if return
           (format " &~a" return)
           ""))
      p))))
