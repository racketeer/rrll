#lang racket/base

(require "core3.rkt"
         "core.rkt")

(provide fl1map3-cut)

(define (fl1map3-dir->dim x y z)
  (cond ((not (= x 0)) fl1map3-width)
        ((not (= y 0)) fl1map3-height)
        ((not (= z 0)) fl1map3-depth)))

(define (fl1map3-cut fm3
                     x0 y0 z0
                     dx:x dx:y dx:z
                     dy:x dy:y dy:z)
  (define width ((fl1map3-dir->dim dx:x dx:y dx:z) fm3))
  (define height ((fl1map3-dir->dim dy:x dy:y dy:z) fm3))
  (define fm2 (make-fl1map width height))
  (for/fold ((y:x x0)
             (y:y y0)
             (y:z z0))
            ((dy (in-range height)))
    (for/fold ((x:x y:x)
               (x:y y:y)
               (x:z y:z))
              ((dx (in-range width)))
      (fl1map-putpixel! fm2 dx dy
                        (fl1map3-getpixel fm3 x:x x:y x:z))
      (values (+ x:x dx:x)
              (+ x:y dx:y)
              (+ x:z dx:z)))
    (values (+ y:x dy:x)
            (+ y:y dy:y)
            (+ y:z dy:z)))
  fm2)
