#lang info

(define license '(Apache-2.0 OR MIT))

(define collection "rrll")

(define version "1.0")

(define deps '("base" "rrll-math" "rrll-unsafe"))

(define build-deps '("scribble-math"))

(define pkg-authors '("Dominik Pantůček"))

(define scribblings '(("scribblings/fl1map.scrbl" ())))

(define compile-omit-paths '("tests"))
