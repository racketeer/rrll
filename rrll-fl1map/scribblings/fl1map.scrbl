#lang scribble/manual

@require[scribble-math/dollar]

@title[#:style (with-html5 manual-doc-style)]{RRLL: Single-Channel Flomaps}
@author[@author+email["Dominik Pantůček" "dominik.pantucek@trustica.cz"]]

Racket Rogue-Like Library: 2D and 3D Single-Channel Flomaps

These modules provide data structure with single-channel canvas-like
behavior used for procedural generation and similar.

@section{Construction and Access}

@defmodule[rrll/fl1map/core]

This module contains the 2D @racket[flonum?] map structure and basic
constructors and accessors.

@defstruct*[fl1map ((width fixnum?)
		    (height fixnum?)
		    (data flvector?))]{
  Stores the single-channel @racket[flonum?] map data.
}

@defproc[(make-fl1map (width fixnum?) (height fixnum?)) fl1map?]{
  Creates @racket[fl1map?] of specified size.
}

@defproc[(fl1map-putpixel! (fm fl1map?)
			   (x fixnum?)
			   (y fixnum?)
			   (v flonum?)) void?]{
  Sets single @racket[fl1map?] pixel to the value specified. Performs
  any clipping necessary.
}

@defproc[(fl1map-getpixel (fm fl1map?)
			  (x fixnum?)
			  (y fixnum?)
			  (default (or/c #f flonum?))) (or/c #f flonum?)]{
  Gets a single @racket[fl1map?] pixel defaulting to @racket[default] value
  if the requested pixel is outside of its bounds.
}

@section{Bilinear Sampling}

@defmodule[rrll/fl1map/sampling]

This module contains the bilinear sampling procedures for
single-channel @racket[flonum?] maps.

@defproc[(fl1map-sample (fm fl1map?)
			(x flonum?)
			(y flonum?)) flonum?]{
  Samples given @racket[fl1map?]. The coordinates are in its dimensions.
}

@defproc[(fl1map-unit-sample (fm fl1map?)
			     (x flonum?)
			     (y flonum?)) flonum?]{
  Samples given @racket[fl1map?]. Both @${x,y\in\langle 0;1.0\rangle}.
}

@section{Querying}

@defmodule[rrll/fl1map/query]

This module contains procedures used for querying the flomaps.

@defproc[(fl1map-min (fm fl1map?)) flonum?]{
  Finds the minimum value in given @racket[fl1map?].
}

@defproc[(fl1map-max (fm fl1map?)) flonum?]{
  Finds the maximum value in given @racket[fl1map?].
}

@section{Operations}

@defmodule[rrll/fl1map/ops]

This module contains mutating operations on 2D @racket[fl1map?]s.

@defproc[(fl1map-addpixel! (fm fl1map?)
			   (x fixnum?)
			   (y fixnum?)
			   (v flonum?)) void?]{
  Adds given value to the pixel value.
}

@defproc[(fl1map-divpixel! (fm fl1map?)
			   (x fixnum?)
			   (y fixnum?)
			   (n flonum?)) void?]{
  Divides given pixel value by the number provided.
}

@defproc[(fl1map-div! (fm fl1map?) (n flonum?)) void?]{
  Divides all pixels by given number.
}

@defproc[(fl1map-normalize! (fm fl1map?)) void?]{
  Normalizes all the values of given @racket[fl1map?].
}

@defproc[(fl1map-transpose (fm fl1map?)) fl1map?]{
  Creates a new @racket[fl1map?] which is a transposition of the given one.
}

@section{Rendering to Canvas}

@defmodule[rrll/fl1map/render]

This module is used for visualizing the flomaps.

@defproc[(fl1map->canvas (fm fl1map?)
			 (conv procedure?)) canvas?]{
  Creates a canvas from given @racket[fl1map?] using @racket[flonum?] to color
  conversion procedure provided.
}

@section{Single-Channel 3D Flomaps}

The following modules are just 3D versions of their 2D counterparts.

@defmodule[rrll/fl1map/core3]

@defstruct*[fl1map3
  ((width fixnum?)
   (height fixnum?)
   (depth fixnum?)
   (data flvector?))]{
  Stores the 3D flomap data.
}

@defproc[(make-fl1map3 (width fixnum?)
		       (height fixnum?)
		       (depth fixnum?)) fl1map3?]{
  Creates an empty 3D flomap filled with @racket[0.0].
}

@defproc[(fl1map3-putpixel! (fm fl1map3)
			    (x fixnum?)
			    (y fixnum?)
			    (z fixnum?)
			    (v flonum?)) void?]{
  Sets the value at given coordinates.
}

@defproc[(fl1map3-getpixel (fm fl1map3?)
			   (x fixnum?)
			   (y fixnum?)
			   (z fixnum?)
			   (default (or/c flonum? #f) #f)) (or/c flonum? #f)]{
  Gets the values stored at given coordinates or @racket[default] value if out of bounds.
}

@defproc[(fl1map3-clear! (fm fl1map3?)
			 (v flonum? 0.0)) void?]{
  Fills the @racket[fl1map3?] with given value @racket[v].
}

@section{Querying the 3D Flonum Map}

@defmodule[rrll/fl1map/query3]

This module contains procedures for querying the 3D flomap.

@defproc[(fl1map3-min (fm fl1map3)) flonum?]{
  Returns the minimum value stored in the 3D flomap.
}

@defproc[(fl1map3-max (fm fl1map3)) flonum?]{
  Returns the maximum value stored in the 3D flomap.
}

@section{Mutating Operations on 3D Flomaps}

@defmodule[rrll/fl1map/ops3]

This module defines the mutating operations on 3D flomaps.

@defproc[(fl1map3-addpixel! (fm fl1map3?)
			    (x fixnum?)
			    (y fixnum?)
			    (z fixnum?)
			    (v flonum?)) void?]{
  Adds given value to the value at given position.
}

@defproc[(fl1map3-divpixel! (fm fl1map3?)
			    (x fixnum?)
			    (y fixnum?)
			    (z fixnum?)
			    (n flonum?)) void?]{
  Divids the value at given position by the number specified.
}

@defproc[(fl1map3-div! (fm fl1map3?)
		       (n flonum?)) void?]{
  Divides all values in the 3D flomap by given number.
}

@defproc[(fl1map3-normalize! (fm fl1map3?)) void?]{
  Normalizes all values in given 3D flomap to the @${\langle 0;1\rangle} range.
}

@section{Fine-grained Sampling of 3D Flomaps}

@defmodule[rrll/fl1map/sampling3]

This module contains procedures for sub-voxel querying 3D flomaps.

@defproc[(fl1map3-sample (fm fl1map3?)
			 (x flonum?)
			 (y flonum?)
			 (z flonum?)) flonum?]{
  Uses bilinear interpolation for non-integer sampling coordinates.
}

@defproc[(fl1map3-unit-sample (fm fl1map3?)
			      (x flomap?)
			      (y flomap?)
			      (z flomap?)) flonum?]{
  Uses bilinear interpolation for sampling the 3D flomap with coordinates
  in the @${\langle 0;1\rangle} range.
}

@section{2D Cuts of 3D Flomaps}

@defmodule[rrll/fl1map/cuts3]

This module allows creating 2D cuts of 3D flomaps.

@defproc[(fl1map3-cut (fm3 fl1map3?)
		      (x0 flonum?)
		      (y0 flonum?)
		      (z0 flonum?)
		      (dx:x flonum?)
		      (dx:y flonum?)
		      (dx:z flonum?)
		      (dy:x flonum?)
		      (dy:y flonum?)
		      (dy:z flonum?)) fl1map?]{
  Creates 2D @racket[fl1map?] from given @racket[fl1map3?] starting
  at the specified @racket[x0], @racket[y0], @racket[z0] position. The
  cutting plane x and y directions are specified in the remaining
  arguments.
}
