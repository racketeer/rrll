#lang racket/base

(module+ test

  (require rackunit
           "../../fl1map/core.rkt")

  (test-equal? "Printer"
               (format "~a" (make-fl1map 10 10))
               "#<fl1map 10x10>")

  (test-case "putpixel and getpixel"
    (define fm1 (make-fl1map 10 10))
    (fl1map-putpixel! fm1 1 1 0.5)
    (check-equal? (fl1map-getpixel fm1 1 1) 0.5)
    (check-equal? (fl1map-getpixel fm1 0 0) 0.0))

  )
