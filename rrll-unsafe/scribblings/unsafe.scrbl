#lang scribble/manual

@require[
  (for-label
    racket/base
    racket/fixnum
    )]

@title{RRLL: Unsafe}
@author[@author+email["Dominik Pantůček" "dominik.pantucek@trustica.cz"]]

Racket Rogue-Like Library: (Un)Safe operations.

These modules contain commonly used safe and unsafe ops for some data
structures.

@;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

@section{Safe/Unsafe Fixnums}

@subsection{Safe Fixnums}

Safe and unsafe variants of fixnum operations.

@defmodule[rrll/safe/fixnum]

Safe operations, provides all bindings from
@racketmodname[racket/fixnum].

@subsection{Unsafe Fixnums}

@defmodule[rrll/unsafe/fixnum]

Unsafe operations, exports @racket[unsafe-fixnum-*] procedures as
@racket[fixnum-*] bindings.

@;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

@section{Safe/Unsafe Flonums}

These modules contain safe and unsafe versions of flonum operations.

@subsection{Safe Flonums}

@defmodule[rrll/safe/flonum]

Provides all bindings from @racketmodname[racket/flonum].

@defproc[(any->fl (v (or/c fixnum? flonum?))) flonum?]{
  Accepts @racket[fixnum?] or @racket[flonum?] and produces equivalent @racket[flonum?].
}

@defproc[(fl+-? (fl1 flonum?) (fl2 flonum?)
		(#:precision precision (or/c #f flonum?) #f)) boolean?]{

Returns @racket[#t] if the two given values are closer than
@racket[precision] or - if the @racket[precision] is @racket[#f] -
when they are exactly the same.

}

@subsection{Unsafe Flonums}

@defmodule[rrll/unsafe/flonum]

Unsafe operations, exports @racket[unsafe-flonum-*] procedures as
@racket[flonum-*] bindings and unsafe versions of other procedures
given by @racketmodname[rrll/safe/flonum].

@;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

@section{Safe/Unsafe Bytes}

These modules contain safe and unsafe versions of bytes operations.

@subsection{Safe Bytes}

@defmodule[rrll/safe/bytes]

Provides nothing, byte operations from @racketmodname[racket/base] are
used.

@subsection{Unsafe Bytes}

@defmodule[rrll/unsafe/bytes]

Unsafe operations, exports @racket[unsafe-bytes-*] procedures
as @racket[bytes-*] bindings.

@;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

@section{Safe/Unsafe Vectors}

These modules provide safe and unsafe versions of vector operations.

@subsection{Safe Vectors}

@defmodule[rrll/safe/vector]

Safe vector operations - @racketmodname[racket/base] bindings are
used.

@subsection{Unsafe Vectors}

@defmodule[rrll/unsafe/vector]

Unsafe operations, exports @racket[unsafe-vector-*] procedures as
@racket[vector-*] bindings.
