#lang racket/base

(require "../safe-unsafe/fixnum.rkt")

(provide-safe-or-unsafe-fixnum #t)
