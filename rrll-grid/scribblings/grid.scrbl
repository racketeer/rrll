#lang scribble/manual

@require[scribble-math pict racket/math racket/class racket/draw]

@title[#:style (with-html5 manual-doc-style)]{RRLL: Functional Grids}
@author[@author+email["Dominik Pantůček" "dominik.pantucek@trustica.cz"]]

Racket Rogue-Like Library: Functional Grids

These modules provide the purely functional immutable grid
implementations. The main differences are memory consumption and the
time and memory complexity of referencing or setting a tile to a new
value.

@section{Generics}
@defmodule[rrll/grid/generics]

The generic interface deals only with accessing and modifying the
grid. On top of this interface more common functionality is added.

@defthing[gen:rlgrid any/c]{

  A generic interface that provides access methods to tile of an
  underlying grid. The following methods must be implemented:

  @itemlist[
	@item{@racket[(rlgrid-ref grid x y)] - retrieves tile at given coordinates}
	@item{@racket[(rlgrid-set grid x y value)] - sets tile at given coordinates to a new value}
	@item{@racket[(rlgrid-width grid)] - returns the width of the grid}
	@item{@racket[(rlgrid-height grid)] - returns the height of the grid}
	]

  The structs implementing this generic interface can - and probably
  will - implement other means of inspection. Construction of the
  underlying data structures is not covered by this generic interface.
  
}

@defproc[(rlgrid-ref (rlg rlgrid?)
		     (x nonnegative-integer?)
		     (y nonnegative-integer?)) any/c]{
    Returns a tile at location given by @racket[x] and @racket[y]
    coordinates.
}

@defproc[(rlgrid-set (rlg rlgrid?)
		     (x nonnegative-integer?)
		     (y nonnegative-integer?)
		     (v any/c)) rlgrid?]{
    Returns the same implementation of @racket[gen:rlgrid] with the
    tile at coordinates @racket[x] and @racket[y] being changed to a new
    @racket[value].
}

@defproc[(rlgrid-multi-set (rlg rlgrid?)
			   (x nonnegative-integer?)
			   (y nonnegative-integer?)
			   (v any/c) ...) rlgrid?]{
    Used to set multiple cells in one tree update. The remaining
    arguments are @racket[(x y value)] triplets.
}

@defproc[(rlgrid-width (rlg rlgrid?)) positive-integer?]{
    Returns the width of given grid.
}

@defproc[(rlgrid-height (rlg rlgrid?)) positive-integer?]{
    Returns the height of given grid.
}

@defproc[(rlgrid? (v any/c)) boolean?]{
    Returns @racket[#t] if given value @racket[v] is an instance of
    any implementation of @racket[gen:rlgrid].
}

@defproc[(rlgrid-inside? (rlg rlgrid?)
			 (x nonnegative-integer?)
			 (y nonnegative-integer?)) boolean?]{
    Returns @racket[#t] if given tile coordinates lie inside the given
    grid.
}

@defform[(in-rlgrid maybe-modifier ... rlg (xstart 0) (ystart 0) (xend #f) (yend #f))
  #:grammar ((maybe-modifier #:with-xy
                             (code:line #:default default)
                             #:only-xy))
  #:contracts ((rlg (generic-instance/c gen:rlgrid))
               (xstart integer?)
               (ystart integer?)
               (xend (or/c #f integer?))
               (yend (or/c #f integer?)))]{
    Creates a sequence (a stream) of tile values for given
    rectangular (sub)area of given grid.

    If no @racket[#:default] is provided, the are is clipped against
    the grid.

    If the clipped or given rectangle has zero area, an empty stream
    is returned.

    If @racket[#:default] is provided, the sequence always iterates
    over the whole rectangle requested, but for tiles outside the grid it
    returns the @racket[default] value provided.

    When @racket[#:with-xy] is given, the sequence will always return
    three values: @racket[x] and @racket[y] coordinates and @racket[value]
    of the tile.

    If @racket[#:only-xy] is requested, only @racket[x] and @racket[y]
    coordinate values are provided by the sequence. This is mainly useful
    without @racket[#:default] to iterate over a clipped rectangle.

    If either @racket[xend] or @racket[yend] is @racket[#f], they are
    substituded for the width and height of given grid respectively.

    To iterate over all tiles in given grid:

    @racketblock[(for ((tile (in-rlgrid grid)))
                   ...)]

    The same iteration with coordinates passed to the for loop body:

    @racketblock[(for (((x y tile) (in-rlgrid #:with-xy grid)))
                   ...)]

    Clipping the iteration rectangle to the grid (will iterate over
    2x2 rectangle in the upper left corner of the grid):

    @racketblock[(for (((x y tile) (in-rlgrid #:with-xy grid -10 -10 2 2)))
                   ...)]

    With @racket[#:default] specified, it is possible to substitute
    meaningful value for tiles outside the grid:

    @racketblock[(for ((tile (in-rlgrid #:default #f -1 -1 2 2)))
                   ...)]

    To only clip given rectangle to the grid and iterate over the
    coordinates:

    @racketblock[(for (((x y) (in-rlgrid #:only-xy grid -1 -1 2 2)))
                   ...)]

}

@section{dtgrid: Deduplicated Tree Based Grid}
@defmodule[rrll/grid/dtgrid]

This module implements @racket[gen:rlgrid] that stores the grid in a
binary tree where any node can be collapsed if its two child nodes
contain values that are @racket[equal?] to each other of if the child
nodes are both collapsed and their collapsed value is the same.

@defproc[(dtgrid? (v any/c)) boolean?]{
    Returns @racket[#t] if given value @racket[v] is a @racket[dtgrid].
}

@defproc[(make-dtgrid (width positive-integer?)
		      (height positive-integer?)
		      (default (or/c (not/c procedure?)
		      	       	     (procedure-arity-includes/c 0)
				     (procedure-arity-includes/c 2)))
		      (#:force force boolean? #f)
		      ) dtgrid?]{
    Constructs a new @racket[dtgrid?] using @racket[default] as the
    initial value for all tiles.

    If the @racket[default] is a procedure, it is evaluated when given
    cell is accessed. If the procedure accepts two arguments, the
    coordinates of the tile being constructed are passed to it.

    If the two tiles in the same cons cell are @racket[equal?], the
    cell is collapsed and separate cells are not allocated. If an internal
    tree node cons cell contains two collapsed values which are
    @racket[equal?] it is collapsed recursively up to root if possible.

    The construction takes constant time as only the collapsed root
    cell is allocated.

  However, if @racket[force] is @racket[#t] and the @racket[default]
  is a @racket[procedure?], all the tiles are created upon creation.

}

@defproc[(dtgrid-stats (dtg dtgrid?)) (values nonnegative-integer?
		       	    	      	      nonnegative-integer?
					      nonnegative-integer?
					      nonnegative-integer?)]{
    Returns the number of actually allocated nodes of given @racket[dtgrid?].

    The values are:

    @itemlist[
              @item{Cons cells,}
              @item{Collapsed subtrees,}
              @item{Default value subtrees,}
              @item{Value cells.}]

}

@defproc[(rlgrid->dtgrid (rlg rlgrid?)) dtgrid?]{
    Converts any grid to @racket[dtgrid]. If it is already a
    @racket[dtgrid?], it is returned as-is.

    Because this procedure uses default value procedure which is
    evaluated lazily, it is actually a constant-time shallow copy of
    original @racket[rlg].
}

@section{vgrid: Vector-Based Grid}
@defmodule[rrll/grid/vgrid]

@itemlist[
	@item{@${n=w\cdot h}}
	]
	
  @tabular[#:style 'boxed
  	   #:column-properties '(left center center)
	   #:row-properties '(bottom-border ())
	   #:sep @hspace[1]
	   (list (list "Method" "Time" "Memory")
	   	 (list "Make" @${\Theta(n)} @${\Theta(n)})
		 (list "Ref" @${\Theta(1)} @${\Theta(1)})
		 (list "Set" @${\Theta(n)} @${\Theta(n)}))
	]

This module implements @racket[gen:rlgrid] that stores the grid in a
one-dimensional @racket[vector]. The tiles in a row are stored from
left to right and the rows are concatenated from top to bottom
linearly in the vector.

@defproc[(vgrid? (v any/c)) boolean?]{
    Returns @racket[#t] if given value @racket[v] is a @racket[vgrid].
}

@defproc[(make-vgrid (width positive-integer?)
		     (height positive-integer?)
		     (default (or/c (not/c procedure?)
		     	      	    (procedure-arity-includes/c 0)
				    (procedure-arity-includes/c 2)))) vgrid?]{
    Constructs a new @racket[vgrid?] using @racket[default] as the
    initial value for all tiles. If the @racket[default] argument is a
    procedure, it is evaluated immediately for all tiles.
}

@defproc[(rlgrid->vgdir (rlg rlgrid?)) vgrid?]{
    Converts any grid to @racket[vgrid]. If it is already a
    @racket[vgrid?], it is returned as-is.
}

@section{Utilities}
@defmodule[rrll/grid/utils]

This module contains simple utilities commonly used by various
algorithms in other modules.

@(define (make-grid-pict w h vals (s 20))
       (apply vc-append (for/list ((j (in-range h)))
                          (apply hc-append (for/list ((i (in-range w)))
                                             (cc-superimpose
                                              (rectangle s s)
                                              (text (format "~a" (vector-ref vals (+ (* j w) i))))
                                              ))))))

@defproc[(rlgrid-values (rlg rlgrid?)
			(xstart fixnum? 0)
			(ystart fixnum? 0)
			(xend (or/c fixnum? #f) #f)
			(yend (or/c fixnum? #f) #f)
			(#:default default any/c #f)
			(#:conv conv (-> any/c any/c) (λ (v) v))) any]{
    Helper procedure to get values of grid area as bindings. If the
    area is (partially) outside the grid, @racket[default] value is
    substituted for all tiles that are not within the grid bounds.

    @racketblock[(define-values (tl t tr l c r bl b cr)
                   (rlgrid-values grid 0 0 3 3))]

    @make-grid-pict[5 4 (vector 'tl 't 'tr "" "" 'l 'c 'r "" "" 'bl 'b 'br "" ""
                                "" "" "" "" "")]

  The @racket[conv] routine is used to optionally convert the tiles as required.

}

@defproc[(rlgrid-ref-vec2 (rlg rlgrid?) (v vec2?)) any/c]{
    A wrapper for @racket[rlgrid-ref]:

    @racketblock[(rlgrid-ref rlg (vec2-x v) (vec2-y v))]
}

@defproc[(rlgrid-set-vec2 (rlg rlgrid?) (p vec2?) (v any/c)) rlgrid?]{
    A wrapper for @racket[rlgrid-set]:

    @racketblock[(rlgrid-set rlg (vec2-x p) (vec2-y p) v)]
}

@defproc[(rlgrid-component (rlg rlgrid?)
			   (x fixnum?)
			   (y fixnum?)
			   (#:directions dirs (listof vec2?) vec2s:grid)
			   (#:passable? passable? (-> any/c boolean) (λ (v) v))
			   ) (set/c vec2?)]{
    Random-first search based algorithm for determining the set of
    @racket[vec2?]s of connected component of given grid.
}

@defproc[(rlgrid-clip-rect (rlg rlgrid?)
			   (x0 fixnum?)
			   (y0 fixnum?)
			   (x1 (or/c #f fixnum?) #f)
			   (y1 (or/c #f fixnum?) #f))
			   (values fixnum? fixnum? fixnum? fixnum?)]{
    Clips given rectangle to be within given grid.
}

@defproc[(rlgrid-set-border (rlg rlgrid?)
			    (component (set/c vec2?))
			    (#:directions dirs (listof vec2?) vec2s:grid)
			    ) (set/c vec2?)]{
    Generates a set of @racket[vec2?]s that is adjacent to given
    component (or any set of tiles) based on @racket[dirs] major directions.
}

@defproc[(rlgrid-set-filter (rlg rlgrid?)
			    (component (set/c vec2?))
			    (pred? (-> any/c boolean?))
			    ) (set/c vec2?)]{
    Filters given set (component) of @racket[vec2?]s in given
    @racket[grid].
}

@defproc[(rlgrid-dead-ends (rlg rlgrid?)) (listof vec2?)] {
    Scans the grid to find dead-ends.
}

@defproc[(rlgrid-pass-neighbors (rlg rlgrid?)
				(pos vec2?)
				(#:directions dirs (listof vec2?) vec2s:grid)
				(#:passable? passable? (-> any/c boolean?) (λ (v) v))
				) (listof vec2?)]{
    Returns passable neighbors from given @racket[pos] in given
    @racket[dirs].
}

@section{Discrete Differential Analysis}
@defmodule[rrll/grid/dda]

@(define (make-dda-grid w h x0 y0 x1 y1 (s 40))
    (dc (λ (dc dx dy)
          (define old-pen (send dc get-pen))
          (define old-brush (send dc get-brush))
          (send dc set-pen (make-color 128 128 128) 1 'solid)
          (for ((j (in-range 0 (add1 h))))
            (send dc draw-line 0 (* j s) (* w s) (* j s)))
          (for ((i (in-range 0 (add1 w))))
            (send dc draw-line (* i s) 0 (* i s) (* h s)))
          (send dc set-brush (make-color 192 192 192) 'crossdiag-hatch)
          (for ((j (in-range 0 h)))
            (send dc draw-rectangle 0 (* j s) s s)
            (send dc draw-rectangle (* (sub1 w) s) (* j s) s s)
            )
          (for ((i (in-range 1 (sub1 w))))
            (send dc draw-rectangle (* i s) 0 s s)
            (send dc draw-rectangle (* i s) (* (sub1 h) s) s s)
            )
          (send dc set-pen (make-color 0 0 0) 1 'solid)
          (send dc draw-line (* x0 s) (* y0 s) (* x1 s) (* y1 s))
          (send dc set-pen (make-color 0 192 0) 1 'solid)
          (define last-x #f)
          (let xloop ((x (ceiling x0))) ; quick hack for ltr direction only
            (when (< x x1)
              (set! last-x x)
              (define y (+ y0
                           (/ (* (- y1 y0)
                                 (- x x0))
                              (- x1 x0))))
              (send dc draw-ellipse (sub1 (* x s)) (sub1 (* y s)) 3 3)
              (xloop (+ x 1.0))))
          (send dc set-pen (make-color 0 0 192) 1 'solid)
          (let yloop ((y (ceiling y0))) ; ...
            (when (< y y1)
              (define x (+ x0
                           (/ (* (- x1 x0)
                                 (- y y0))
                              (- y1 y0))))
              (send dc draw-ellipse (sub1 (* x s)) (sub1 (* y s)) 3 3)
              (yloop (+ y 1.0))))
          (send dc set-pen (make-color 192 0 0) 1 'solid)
          (define last-y (+ y0
                       (/ (* (- y1 y0)
                             (- last-x x0))
                          (- x1 x0))))
          (send dc draw-ellipse (- (* last-x s) 2) (- (* last-y s) 2) 5 5)
          (send dc set-brush old-brush)
          (send dc set-pen old-pen))
        400 320))

@defproc[(dda (rlg rlgrid?)
	      (start-x (and/c (or/c fixnum? flonum?)
	      	       	      (not/c negative?)))
	      (start-y (and/c (or/c fixnum? flonum?)
	      	       	      (not/c negative?)))
	      (ray-dx (or/c fixnum? flonum?))
	      (ray-dy (or/c fixnum? flonum?))
	      (limit-t (or/c fixnum? flonum?) +inf.0)
	      (wall? (-> any/c boolean?) (λ (v) (not v)))
	      (skip-start-tile boolean? #t))
	      (values (or/c #f flonum?)
	      	      any/c
		      (or/c #f vec2?))]{
    Discrete differential analysis line algorithm iterates position on
    the line in discrete steps where each step lies on the underlying
    integer grid. The first impassable tile encountered is taken as a ray
    hit:

    @blank[10 10]

    @make-dda-grid[10 6 1.5 1.5 9.9 3.5]

    If any of the coordinates @racket[start-x], @racket[start-y],
    @racket[ray-dx], or @racket[ray-dy] are @racket[integer?], they are
    adjusted to point to the center of given tile by adding @racket[0.5]
    to their value.

    The @racket[limit-t] argument specifies maximum distance before
    giving up finding a collision and returning @racket[#f] for all return
    values.

    As the implementation expects a grid of @racket[boolean?] values
    where @racket[#f] means wall (impassable, opaque tile) and @racket[#t]
    means free space (passable, transparent tile), the default predicate
    for @racket[wall?] reflects that. This makes the algorithm easy to
    adapt to different representations.

    If @racket[skip-start-tile] is true, a hit within the starting
    tile is ignored.

    The values returned are:

    @itemlist[
              @item{distance from the beginning of the ray to the hit}
              @item{value of the tile that caused the ray hit}
              @item{coordinates of this tile as @racket[vec2?]}
              ]
}

@section{Visibility Arcs}

@defmodule[rrll/grid/varc]

This module implements visibility arcs, simple operations with them
and method for determining tile arc from given center point.

It allows computing possibly visible tiles in a @racket[rlgrid?].

@defstruct*[varc
  ((start flonum?)
   (end flonum?))]{

  This struct represents an arc of visibility by means of using
  starting and ending angle. If end is smalled than start, the arc
  goes over the @${2\pi} boundary.

@centered[
(lt-superimpose
(cc-superimpose
  (hc-append
    (blank 10 10)
    (hline 100 100)
    (arrowhead 10 0))
  (vc-append
    (blank 10 10)
    (vline 100 100)
    (arrowhead 10 (/ (* 3 pi) 2)))
  (dc (lambda (dc dx dy)
    (define old-brush (send dc get-brush))
    (define old-pen (send dc get-pen))
    (send dc set-brush "black" 'transparent)
    (define sangle (- (/ pi 4)))
    (define eangle (/ pi 2))
    (send dc draw-arc (+ dx 1) (+ dy 1) 48 48 sangle eangle)
    (send dc set-brush old-brush)
    (send dc set-pen old-pen)
    ) 50 50)
  (dc (lambda (dc dx dy)
    (define old-brush (send dc get-brush))
    (define old-pen (send dc get-pen))
    (send dc set-brush "black" 'transparent)
    (define sangle (/ (* 1 pi) 4))
    (define eangle (/ pi 2))
    (send dc draw-arc (+ dx 1) (+ dy 1) 68 68 sangle eangle)
    (send dc set-brush old-brush)
    (send dc set-pen old-pen)
    ) 70 70)
    )
  (rb-superimpose
    (blank 96 72)
    (rotate
      (hline 34 34)
      (/ pi 4)))
  (rb-superimpose
    (blank 85 86)
    (rotate
      (hline 24 24)
      (- pi 4)))
  (rb-superimpose
    (blank 95 58)
    (text "β"))
  (rb-superimpose
    (blank 75 25)
    (text "α"))
  (rb-superimpose
      (blank 83.5 79.5)
      (arrowhead 10 (/ (* 5 pi) 4)))
  (rb-superimpose
      (blank 86 39)
      (arrowhead 10 (/ (* 7 pi) 4)))
      )
  ]

  The starting angle @${\alpha} and ending angle @${\beta} are shown in the figure.

}

@defthing[full-varc varc?]{
  A visibility arc representing the full circle.
}

@defproc[(varc-empty? (arc varc?)) boolean?]{
  Returns true if start angle equals the end angle.
}

@defproc[(varcs-intersections-with (arcs (listof varc?)) (arc varc?)) (listof varc?)]{

  Updates the list of given visibility arcs by reducing them to
  intersections with the arc provided as second argument. Some of the
  arcs may be reduced to empty arcs.
}

@defproc[(varcs-subtract (arcs (listof varc?)) (arc varc?)) (listof varc?)]{

  Updates the list of given arcs by subtracting the arc given as
  second argument from them. Some arcs may be reduced to empty arcs.
}

@defproc[(varc-size (arc varc?)) flonum?]{

  Returns the size (in radians) of given visibility arc handling arcs
  that wrap around the @${2\pi} boundary correctly.

}

@defproc[(varcs-size (arcs (listof varc?))) flonum?]{

  Returns the sum of the sizes of all arcs given.

}

@section{Computing Visibility Fans in Grids}

@defmodule[rrll/grid/vfan]

@defproc[(vfan-iter (rlg rlgrid?)
		    (start-x real?)
		    (start-y real?)
		    (proc (-> fixnum? fixnum? void?))
		    (#:wall? wall? (-> any/c boolean?) (λ (v) (not v)))
		    (#:init-varc init-varc varc? full-varc)
		    (#:with-tile with-tile boolean? #f)
		    (#:process? process? (-> any/c boolean?) (λ (v) #f))
		    ) void?]{

  Iterates through given @racket[rlgrid?] in BFS-like manner starting
  at the tile containing the (possibly non-integer) point
  @racket[(start-x start-y)].

  For each tile covered by the visibility fan centered upon the
  starting point it evaluates given @racket[proc] with the X and Y
  coordinate of such tile being the only arguments provided (for now).

  The @racket[#:wall?] keyword argument can be used to specify how the
  algorithm recognizes which walls are opaque for the purpose of
  visibility computation.

  The @racket[init-varc] argument can be used to limit the initial
  search arc to particular direction and field-of-view.

  If @racket[with-tile] is @racket[#t], the @racket[proc] gets always
  evaluated with a third argument - the tile in question - as well.

  When @racket[process?] returns non-@racket[#f] for any tile that is
  considered wall, it is processed with @racket[proc] although it does
  not add any tiles to further iteration.

}

@defform[(in-vfan rlg start-x start-y option ...)
  #:grammar ((option #:with-tile
  	    	     (code:line #:wall? wall? = (λ (v) (not v)))
  	    	     (code:line #:process? process? = (λ (v) #f))
		     (code:line #:dir dir = #f)
		     (code:line #:fov/2 fov/2 = #f)
		     (code:line #:init-varc init-varc = full-varc)))
  #:contracts ((rlg rlgrid?)
   	       (start-x flonum?)
	       (start-y flonum?)
	       (wall? (-> any/c boolean?))
	       (dir (or/c #f flonum?))
	       (fov/2 (or/c #f flonum?))
	       (init-varc varc?))]{

  Uses @racketmodname[racket/generator] wrapper around
  @racket[vfan-iter] to provide more convenient interface to
  visibility fan iteration.

  The @racket[dir] and @racket[fov/2] arguments have precedence over
  @racket[init-varc] if they are both specified.

}
