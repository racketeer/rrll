#lang racket/base

(module+ test

  (require rackunit
           "../../grid/varc.rkt"
           racket/math)

  (test-true "Empty visibility arc" (varc-empty? (varc 0.0 0.0)))
  (test-true "Empty constant visibility arc" (varc-empty? empty-varc))
  (test-false "Non-empty visibility arc" (varc-empty? (varc 0.0 pi)))

  (test-true "Visibility arc contains" (varc-contains-angle? (varc 0 pi) (/ pi 2)))
  (test-false "Visibility arc does not contain" (varc-contains-angle? (varc 0 pi) (/ (* pi 3) 2)))
  (test-true "Wrapped visibility arc contains" (varc-contains-angle? (varc (/ (* pi 3) 2) (/ pi 2)) 0))
  (test-false "Visibility arc does not contain" (varc-contains-angle? (varc (/ (* pi 3) 2) (/ pi 2)) pi))

  (test-true "Visibility arcs overlap"
             (varc-overlap? (varc 0 pi)
                            (varc (/ pi 2) (/ (* pi 3) 2))))
  (test-false "Visibility arcs do not overlap"
              (varc-overlap? (varc 0 (/ pi 2))
                             (varc pi (/ (* pi 3) 2))))
  (test-true "Visibility arcs overlap with the same start"
             (varc-overlap? (varc 0 pi)
                            (varc 0 (* 2 pi))))
  (test-true "Visibility arcs overlap with the same start and end"
             (varc-overlap? (varc 0 pi)
                            (varc 0 pi)))

  (test-true "Visibility arc contains another"
             (varc-contains-varc? (varc 0 pi)
                                  (varc (/ pi 4)
                                        (/ (* pi 3) 4))))
  (test-false "Visibility arc does not contain another"
              (varc-contains-varc? (varc 0 pi)
                                   (varc (/ (* pi 5) 4)
                                         (/ (* pi 7) 4))))

  (test-true "Adjacent visibility arcs"
             (varc-adjacent? (varc 0 pi)
                             (varc pi (* 2 pi))))
  (test-true "Wrapped adjacent visibility arcs"
             (varc-adjacent? (varc 0 pi)
                             (varc (/ (* pi 3) 2) (* 2 pi))))
  (test-false "Non-dadjacent visibility arcs"
              (varc-adjacent? (varc 0 pi)
                              (varc (/ (* pi 3) 2) (/ (* 7 pi) 4))))

  (test-equal? "Empty intersection"
               (varc-intersection (varc 0 pi)
                                  (varc pi (* 2 pi)))
               empty-varc)
  (test-equal? "One quadrant intersection without wraps"
               (varc-intersection (varc 0 pi)
                                  (varc (/ pi 2) (/ (* 3 pi) 2)))
               (varc (/ pi 2) pi))
  (test-equal? "Wrapped intersection"
               (varc-intersection full-varc
                                  (varc (/ (* 3 pi) 2) (/ pi 2)))
               (varc (/ (* 3 pi) 2) (/ pi 2)))

  (test-within "East tile"
               (tile->varc 0.5 0.5 1 0)
               (varc (/ pi 4) (/ (* pi 3) 4))
               0.0001)
  (test-within "North tile"
               (tile->varc 0.5 0.5 0 -1)
               (varc (/ (* pi 7) 4) (/ pi 4))
               0.0001)

  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  (test-equal? "Simple subtraction"
               (varc-subtract (varc 0 pi)
                              (varc (/ pi 2) pi))
               (list (varc 0 (/ pi 2))))

  (test-equal? "Visibility arcs affecting an arc"
               (varcs-filter-within
                (list (varc 0 pi)
                      (varc pi (* 2 pi))
                      (varc (/ pi 2) (/ (* pi 3) 2)))
                (varc 0 (/ pi 2)))
               (list (varc 0 pi)))

  (test-equal? "Intersecting Visibility arcs affecting an arc"
               (varcs-intersections-with
                (list (varc 0 pi)
                      (varc pi (* 2 pi))
                      (varc (/ pi 2) (/ (* pi 3) 2)))
                (varc 0 (/ pi 2)))
               (list (varc 0 (/ pi 2))
                     empty-varc
                     empty-varc
                     ))

  (test-equal? "Overflow intersection"
               (varc-intersection full-varc (varc (/ (* pi 7) 4) (/ pi 4)))
               (varc (/ (* pi 7) 4) (/ pi 4)))

  )
