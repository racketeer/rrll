#lang racket/base

(require "generic.rkt"
         racket/vector
         racket/function
         rrll/unsafe/vector
         rrll/unsafe/fixnum
         )

(provide vgrid?
         make-vgrid
         rlgrid->vgrid)

(struct vgrid
  (width height data)
  #:methods gen:rlgrid
  ((define (rlgrid-ref vg x y (default rlgrid-invalid-tile))
     (define width (vgrid-width vg))
     (define height (vgrid-height vg))
     (cond ((or (fx< x 0)
                (fx< y 0)
                (fx>= x width)
                (fx>= y height))
            (if (eq? default rlgrid-invalid-tile)
                (error 'dtgrid-ref "~v,~v out of bounds ~vx~v"
                       x y width height)
                default))
           (else
            (vector-ref (vgrid-data vg)
                        (fx+ (fx* y (vgrid-width vg)) x)))))
   (define (rlgrid-set vg x y v)
     (define width (vgrid-width vg))
     (define height (vgrid-height vg))
     (when (or (fx< x 0)
               (fx< y 0)
               (fx>= x width)
               (fx>= y height))
       (error 'dtgrid-ref "~v,~v out of bounds ~vx~v"
              x y width height))
     (define data (vector-copy (vgrid-data vg)))
     (vector-set! data (fx+ (fx* y (vgrid-width vg)) x) v)
     (struct-copy vgrid vg
                  (data data)))
   (define (rlgrid-width vg)
     (vgrid-width vg))
   (define (rlgrid-height vg)
     (vgrid-height vg))
   (define (rlgrid-multi-set g . triplets)
     (define data (vector-copy (vgrid-data g)))
     (let loop ((triplets triplets))
       (cond ((null? triplets)
              (struct-copy vgrid g (data data)))
             (else
              (vector-set! data (car triplets) (cadr triplets) (caddr triplets))
              (loop (cdddr triplets))))))
   ))

(define (make-vgrid width height (default #f))
  (when (or (fx<= width 0)
            (fx<= height 0))
    (error 'make-dtgrid "invalid size ~vx~v" width height))
  (vgrid width height
         (for*/vector #:length (fx* width height)
                      ((y (in-range height))
                       (x (in-range width)))
           (if (procedure? default)
               (if (arity=? (procedure-arity default) 2)
                   (default x y)
                   (default))
               default))))

(define (rlgrid->vgrid rlg)
  (if (vgrid? rlg)
      rlg
      (make-vgrid (rlgrid-width rlg)
                  (rlgrid-height rlg)
                  (λ (x y)
                    (rlgrid-ref rlg x y)))))
