#lang scribble/manual

@require[
  scribble-math
  pict
  racket/math
  racket/class
  (for-label rrll/math/util
  	     rrll/math/algebra2
	     rrll/math/algebra3
	     rrll/math/algebra4
	     rrll/math/angles
	     rrll/math/transform4
  	     racket/base)]

@title[#:style (with-html5 manual-doc-style)]{RRLL: Math}
@author[@author+email["Dominik Pantůček" "dominik.pantucek@trustica.cz"]]

Racket Rogue-Like Library: Math Functions

Common mathematical routines used for many RRLL modules.

@section{Basic Math Utilities}

@defmodule[rrll/math/util]

Additional math functions for fixnums and flonums.

@defproc[(flfrac (n flonum?)) flonum?]{
  Returns the difference between given number and its @racket[flfloor].
}

@defproc[(flclamp (v flonum?) (min flonum?) (max flonum?)) flonum?]{
  Returns a @racket[flonum?] @racket[v] clamped between @racket[max] and @racket[min].
  If @racket[v] is less than @racket[min], returns @racket[min], if it is more
  than @racket[max], returns @racket[max].
}

@defproc[(fxclamp (v fixnum?) (min fixnum?) (max fixnum?)) fixnum?]{
  Returns a @racket[fixnum?] @racket[v] clamped between @racket[max] and @racket[min].
  If @racket[v] is less than @racket[min], returns @racket[min], if it is more
  than @racket[max], returns @racket[max].
}

@;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

@section{Coordinate Systems}

The main coordinate systems used in the project are:

@itemlist[
	@item{Screen coordinates @racket[(x y)]}
	@item{Map coordinates @racket[(x y z 1)]}
	@item{Camera coordinates @racket[(x y z w)] and @racket[(x y w)]}
	]

These coordinates and their transformations are used in the rendering
pipeline.

A set of functions for working with vectors in affine and projective
spaces is implemented throghout the following modules.

@subsection{Screen Coordinates}

The screen -- represented by @racket[canvas] -- is a two-dimensional
plane with origin in the upper-left corner and the X coordinate
growing from left to right and the Y coordinate growing from top to
bottom.

@centered[
(lt-superimpose
  (vr-append
    (text "x")
    (hc-append
      (hline 100 10)
      (arrowhead 10 0)))
  (hb-append
    (text "y")
    (vc-append
      (vline 10 100)
      (arrowhead 10 (- (/ pi 2))))))
]

The screen coordinates are represented as @racket[fixnum?] values.



@subsection{Map Coordinates}

The world map is represented from the top view with origin -- as the
screen -- with the origin at the top-left corner and the X coordinate
growing from left to riht and the Y coordinate growing from top to bottom.

@centered[
(rb-superimpose
(lt-superimpose
  (vr-append
    (text "x")
    (hc-append
      (hline 150 10)
      (arrowhead 10 0)))
  (hb-append
    (text "y")
    (vc-append
      (vline 10 150)
      (arrowhead 10 (- (/ pi 2))))))
      (hc-append
 (text "W")
(vc-append
 (text "N")
 (blank 5)
 (arrow 20 (/ pi 2))
(hc-append
 (blank 5)
 (arrow 20 pi)
 (blank 20)
 (arrow 20 0)
 (blank 5)
 )
 (arrow 20 (/ (* pi 3) 2))
 (blank 5)
 (text "S")
 )
 (text "E")))
]

The respective compass directions North, East, South and West are
shown in the picture as well.

For all practical purposes their respective azimut angles are
clockwise from 0 pointing north:

@itemlist[
@item{north @${\varphi=0}}
@item{east @${\varphi=\frac{\pi}{2}}}
@item{south @${\varphi=\pi}}
@item{west @${\varphi=\frac{3\pi}{2}}}
]

The Z coordinate representing the height on the map grows from below
the map up.

@define[(map-axes)
(rb-superimpose
  (vc-append
    (ht-append
      (text "z")
      (rotate
        (hc-append
          (hline 60 10)
          (arrowhead 10 0))
        (/ (* 3 pi) 4))
      (blank (+ (pict-width (text "z")) 80) 1))
    (hc-append
      (blank 1 80)
      ))
  (lt-superimpose
    (vr-append
      (text "x")
      (hc-append
        (hline 100 10)
        (arrowhead 10 0)))
    (hb-append
      (text "y")
      (vc-append
        (vline 10 100)
        (arrowhead 10 (- (/ pi 2)))))))]

@centered[(map-axes)]

The inclination starts at @${\theta=0} for looking north (towards
negative Y). Positive inclination goes up to @${\theta=\frac{\pi}{2}}
looking straight up in positive Z direction or down to
@${\theta=-\frac{\pi}{2}} looking straight down in negative Z direction.



@subsection{Transformations in Map Coordinates}

The simplest transformation is translation by given vector @${\vec{t}}
can be represented by the following matrix:

@$${\vec{t}=(x_y,y_t,z_t)}

@$${T=\begin{bmatrix}
1 & 0 & 0 & x_t \\
0 & 1 & 0 & y_t \\
0 & 0 & 1 & z_t \\
0 & 0 & 0 & 1 \\
\end{bmatrix}}


Rotation around the Z axis counter-clockwise by the angle @${\theta}
is:

@$${Z=\begin{bmatrix}
\cos\theta & \sin\theta & 0 & 0 \\
-\sin\theta & \cos\theta & 0 & 0 \\
0 & 0 & 1 & 0 \\
0 & 0 & 0 & 1 \\
\end{bmatrix}}

@define[(rotator (axis-angle 0) (ccw #f) (color-name "darkred"))
(define offset-angle (/ pi 8))
(colorize
(dc (lambda (dc dx dy)
(define old-brush (send dc get-brush))
(define old-pen (send dc get-pen))
(send dc set-brush "black" 'transparent)
(define sangle (+ axis-angle offset-angle (/ pi 16)))
(define eangle (+ axis-angle offset-angle (/ (* 7 pi) 4)))
(define cangle (if ccw eangle sangle))
(define sx (+ dx 25 (* 24 (cos cangle))))
(define sy (- (+ dy 25) (* 24 (sin cangle))))
(define langle
(if ccw
(- sangle (/ (* pi 9) 16))
(+ sangle (/ (* pi 5) 16))))
(define lx (+ sx (* 6 (cos langle))))
(define ly (- sy (* 6 (sin langle))))
(define rangle
(if ccw
(- langle (/ pi 2))
(+ langle (/ pi 2))))
(define rx (+ sx (* 6 (cos rangle))))
(define ry (- sy (* 6 (sin rangle))))
(send dc draw-arc (+ dx 1) (+ dy 1) 48 48 sangle eangle)
(send dc draw-line sx sy lx ly)
(send dc draw-line sx sy rx ry)
(send dc set-brush old-brush)
(send dc set-pen old-pen))
50 50)
color-name)
]

@centered[
(lt-superimpose
(map-axes)
(hc-append
(blank 20)
(vc-append
(blank 15)
(scale
(rotator (/ (* 3 pi) 4) #t)
0.5)))
)
]

Rotation around the X axis counter-clockwise by the angle @${\theta}
is:

@$${X=\begin{bmatrix}
1 & 0 & 0 & 0 \\
0 & \cos\theta & \sin\theta & 0 \\
0 & -\sin\theta & \cos\theta & 0 \\
0 & 0 & 0 & 1 \\
\end{bmatrix}}

@centered[
(rc-superimpose
(map-axes)
(hc-append
(vc-append
(scale
(rotator 0 #t)
0.5)
(blank 45))
(blank 20))
)
]

Rotation around the Y axis counter-clockwise by the angle @${\theta}
is:

@$${Y=\begin{bmatrix}
\cos\theta & 0 & -\sin\theta & 0 \\
0 & 1 & 0 & 0 \\
\sin\theta & 0 & \cos\theta & 0 \\
0 & 0 & 0 & 1 \\
\end{bmatrix}}

@centered[
(cb-superimpose
(map-axes)
(vc-append
(hc-append
(scale
(rotator (/ (* pi 3) 2) #t)
0.5)
(blank 45))
(blank 20))
)
]



@subsection{Camera Coordinates}

The 3D coordinates used for populating the world with polygons from
the map and the camera coordinates share a similar same coordinate
system.

The X coordinate grows from left to right, the Y coordinate grows from
top to bottom and the Z coordinate grows into the screen.

@centered[
(lb-superimpose
  (vc-append
    (ht-append
      (rotate
        (hc-append
          (hline 60 10)
          (arrowhead 10 0))
        (/ (* 1 pi) 4))
      (text "z")
      )
    (hc-append
      (blank 1 80)
      ))
  (lt-superimpose
    (vr-append
      (text "x")
      (hc-append
        (hline 100 10)
        (arrowhead 10 0)))
    (hb-append
      (text "y")
      (vc-append
        (vline 10 100)
        (arrowhead 10 (- (/ pi 2)))))))
]

After applying camera translation and all rotations, the axis are
flipped to represent the point in camera coordinate axes using the
following matrix:

@$${
M=\begin{bmatrix}
1 & 0 & 0 & 0 \\
0 & 0 & -1 & 0 \\
0 & -1 & 0 & 0 \\
0 & 0 & 0 & 1 \\
\end{bmatrix}}

It switches the axes as follows:

@$${x_w=x_m}
@$${y_w=-z_m}
@$${z_w=-y_m}
@$${w_w=w_m}

With camera axes in place the projective transformation to camera
projective coordinates is performed using the projection matrix. The
following parameters are used:

@itemlist[
@item{@${r} - pixel aspect ratio: pixel width over pixel height}
@item{@${w} - camera canvas width in pixels}
@item{@${h} - camera canvas height in pixels}
@item{@${n} - near plane Z distance}
@item{@${f} - far plane Z distance}]

Because of limitations of the 24-bit fixed-point w-buffer (TODO), the
near-plane distance has to be fixed:

@$${n=0.1}

The far-plane distance can be almost anything but for all practical
purposes the following is used:

@$${n=100}

The projection assumes @${90^\circ} viewing angle in the smaller
canvas dimension and slightly bigger viewing angle in the other
dimension that gets calculated from the camera canvas width and
height.

First, primary lens size is calculated:

@$${w<=h\Rightarrow l=w}
@$${w>h\Rightarrow l=h}

Then width and height aspect ratio is computed:

@$${r_w=\frac{w}{l}}
@$${r_h=\frac{h}{l}}

The projection matrix is then as follows:

@$${P=\begin{bmatrix}
\frac{2\cdot n\cdot r}{r_w} & 0 & 0 & 0 \\
0 & \frac{2\cdot n}{r_h} & 0 & 0 \\
0 & 0 & \frac{f+n}{f-n} & 1 \\
0 & 0 & \frac{2\cdot f\cdot n}{f-n} & 0 \\
\end{bmatrix}}

After applying the projection matrix the vertices are in camera
projective coordinates. Clipping against frustum is performed in these
coordinates.



@subsection{Vector Projection}

Any point in camera projective coordinates can be represented as:

@$${P=(x_p,y_p,z_p,w_p)}

A screen vector then is:

@$${S=(x_s,y_s,w_s)}

The projection is as follows:

@$${x_s=w\cdot\frac{x_p+1}{2\cdot w_p}}
@$${y_s=w\cdot\frac{y_p+1}{2\cdot w_p}}
@$${w_s=w_p}

The screen coordinates must be rounded to the nearest integer.


@section{Generic Linear Algebra}

@subsection{Generic 2D Vectors}

@defmodule[rrll/math/algebra2]

These procedures help with basic map vector handling.

@defproc[(flvec2 (a flonum? 0.0) (b flonum? 0.0)) flvector?]{
  Creates new @racket[flvector] of two elements.
}

@defproc[(flvec2-linear (A flvector?) (B flvector?) (v flonum?)) flvector?]{
  Computes linear interpolation between given vectors:

  @$${C=A+v\cdot(B-A)}
}

@defproc[(flvec2-values (A flvector?)) (values flonum? flonum?)]{
  Returns the two components as separate values.
}

@subsection{Generic 3D Vectors}

@defmodule[rrll/math/algebra3]

@defproc[(flvec3 (a flonum? 0.0) (b flonum? 0.0) (c flonum? 0.0)) flvector?]{
  Returns a three-element @racket[flvector?].
}

@defproc[(flbec3-dot (v1 flvector?) (v2 flvector?)) flonum?]{
  Returns a dot-product of the two 3D vectors given:

  @$${d=\vec{v_1}\cdot\vec{v_2}}
  @$${d=x_{v_1}\cdot x_{v_2}+y_{v_1}\cdot y_{v_2}+z_{v_1}\cdot z_{v_2}}
}

@defproc[(flvec3-length (v flvector?)) flonum?]{
  Returns the length of given three-element @racket[flvector?]:

  @$${l=\sqrt{\vec{v}\cdot \vec{v}}}
  @$${l=\sqrt{x_v^2+y_v^2+z_v^2}}
}

@defproc[(flvec3-length^2 (v flvector?)) flonum?]{
  Returns the square of given vector length.

  @$${l^2=x_v^2+y_v^2+z_v^2}
}

@defproc[(flvec3-dist (A flvector?) (B flvector?)) flonum?]{
  Returns the distance between the two vectors.

  @$${d=\sqrt{(x_A-x_B)^2+(y_A-y_B)^2+(z_A-z_B)^2}}
}

@defproc[(flvec3-dist^2 (A flvector?) (B flvector?)) flonum?]{
  Returns the square of the distance between the two vectors.

  @$${d^2=(x_A-x_B)^2+(y_A-y_B)^2+(z_A-z_B)^2}
}

@defproc[(flvec3-mul (v flvector?) (n flonum?)) flvector?]{
  Multiplies given vector by scalar:

  @$${\vec{v'}=n\cdot \vec{v}}
  @$${\vec{v}=(x_v,y_v,z_v)}
  @$${\vec{v'}=(x_v',y_v',z_v')}
  @$${\vec{v'}=(n\cdot x_v,n\cdot y_v,n\cdot z_v)}
}

@defproc[(flvec3-div (v flvector?) (n flonum?)) flvector?]{
  Divides given flvector by scalar.

  @$${\vec{v'}=\frac{v}{n}}
  @$${\vec{v'}=\frac{1}{n}\cdot \vec{v}}
}

@defproc[(flvec3-normalize (v flvector?)) flvector?]{
  Returns the given vector divided by its size resulting in a vector
  of the same direction but unit size.
}

@defproc[(flvec3-values (v flvector?)) (values flonum? flonum? flonum?)]{
  Returns the three components as separate values.
}

@defproc[(flvec3-linear (A flvector?) (B flvector?) (v flonum?)) flvector?]{
  Computes linear interpolation between given vectors:

  @$${C=A+v\cdot(B-A)}
}

@defproc[(flvec3-add (A flvector?) (B flvector?)) flvector?]{
  Computes the sum of two three-dimensional vector by summing its components.
}

@defproc[(flvec3-neg (A flvector?)) flvector?]{
  Computes the negation of given three-dimensional vector by negating
  its components. The resulting vector has opposite direction and the same
  size as the original vector.
}

@defproc[(flvec3-sub (A flvector?) (B flvector?)) flvector?]{
  Computes the difference of the two three-dimensional vectors
  given by subtracting the components of the latter from the components
  of the former.
}

@defproc[(flvec3-cross (A flvector?) (B flvector?)) flvector?]{
  Computes the cross product of the two three-dimensional vectors
  given. The resulting vector is perpendicular to both original vectors
  and its length is equal to the area of parallelogram delimited
  by them:

  @$${C=A\times B}
  @$${A=(x_A,y_A,z_A)}
  @$${B=(x_B,y_B,z_B)}
  @$${C=(x_C,y_C,z_C)}
  @$${x_C=y_A\cdot z_B-z_A\cdot y_B}
  @$${y_C=z_A\cdot x_B-x_A\cdot z_B}
  @$${z_C=x_A\cdot y_B-y_A\cdot x_B}
}


@section{Normalized Projective 3D Linear Algebra}

@defmodule[rrll/math/algebra4]

This module handles the basic vector transformation in 4D affine
vector space representing 3D projective vector space.

@subsection{Normalized Projective 3D Vectors}

The first part of this module implements the basic affine 4D vector
operations.

@defproc[(flvec4 (a flonum? 0.0)
		 (b flonum? 0.0)
		 (c flonum? 0.0)
		 (d flonum? 1.0)) flvector?]{

  Creates a new 4D vector with the @racket[w] coordinate defaulting
  @racket[1.0] to represent projective 3D vectors easily.
}

@defproc[(flvec4-x (v flvector?)) flonum?]{
  Returns the 0th (x) coordinate of given @racket[flvec4].
}
@defproc[(flvec4-y (v flvector?)) flonum?]{
  Returns the 1st (y) coordinate of given @racket[flvec4].
}
@defproc[(flvec4-z (v flvector?)) flonum?]{
  Returns the 2nd (z) coordinate of given @racket[flvec4].
}
@defproc[(flvec4-w (v flvector?)) flonum?]{
  Returns the 3rd (w) coordinate of given @racket[flvec4].
}

@defproc[(flvec4-values (v flvector?)) (values flonum? flonum? flonum? flonum?)]{
  Returns the four separated components of the 4D affine vector
  representing a vector in the 3D projective vector space.
}

@defproc[(flvec4=? (v1 flvector?) (v2 flvector?) (#:precision precision flonum? 0.0001)) boolean?]{
  Compares all elements of two @racket[flvec4]s with given arbitrary precision.
}

@defproc[(flvec4-add (v1 flvector?) (v2 flvector?)) flvector?]{
  Adds respective components of two @racket[flvec4]s and returns the result as new one.
}
@defproc[(flvec4-neg (v flvector?)) flvector?]{
  Negates all components of given @racket[flvec4] and returns the result as new one.
}
@defproc[(flvec4-sub (v1 flvector?) (v2 flvector?)) flvector?]{
  Subtracts respective components of two @racket[flvec4]s and returns the result as new one.
}

@defproc[(flvec4-mul (v flvector?) (n flonum?)) flvector?]{
  Multiplies all components of given @racket[flvec4] by specified number.
}

@defproc[(flvec4-linear (A flvector?) (B flvector?) (v flonum?)) flvector?]{
  Performs linear interpolation of the two vectors given.
}

@defproc[(flvec4-project (v flvector?)) flvector?]{
  Performs the projection of given vector @${\vec{v}} and returns the result @${\vec{v}'}.

  @$${\vec{v}=(x_v,y_v,z_v,w_v)}
  @$${\vec{v}'=(x_v',y_v',z_v',w_v')}
  @$${x_v'=\frac{x_v}{w_v}}
  @$${y_v'=\frac{y_v}{w_v}}
  @$${z_v'=\frac{z_v}{w_v}}
  @$${w_v'=w_v}
}

@defproc[(flvec4-unproject (v flvector?)) flvector?]{
  Performs the reverse of projection of given vector @${\vec{v}} and returns the result @${\vec{v}'}.

  @$${\vec{v}=(x_v,y_v,z_v,w_v)}
  @$${\vec{v}'=(x_v',y_v',z_v',w_v')}
  @$${x_v'=x_v\cdot w_v}
  @$${y_v'=y_v\cdot w_v}
  @$${z_v'=z_v\cdot w_v}
  @$${w_v'=w_v}
}

@subsection{Normalized 3D Transformation Matrices}

These functions implement construction and application of @${4\times 4} matrices.

@defproc[(flmat4 (r0c0 flonum? 0.0) (r0c1 flonum? 0.0) (r0c2 flonum? 0.0) (r0c3 flonum? 0.0)
                (r1c0 flonum? 0.0) (r1c1 flonum? 0.0) (r1c2 flonum? 0.0) (r1c3 flonum? 0.0)
                (r2c0 flonum? 0.0) (r2c1 flonum? 0.0) (r2c2 flonum? 0.0) (r2c3 flonum? 0.0)
                (r3c0 flonum? 0.0) (r3c1 flonum? 0.0) (r3c2 flonum? 0.0) (r3c3 flonum? 0.0)) flvector?]{
  Creates a @${4\times 4} matrix:

@$${M=\begin{bmatrix}
r0c0 & r0c1 & r0c2 & r0c3 \\
r1c0 & r1c1 & r1c2 & r1c3 \\
r2c0 & r2c1 & r2c2 & r2c3 \\
r3c0 & r3c1 & r3c2 & r3c3 \\
\end{bmatrix}}
}

@defproc[(flmat4-unit) flvector?]{
  Creates a @${4\times 4} unit matrix:

@$${M=\begin{bmatrix}
1 & 0 & 0 & 0 \\
0 & 1 & 0 & 0 \\
0 & 0 & 1 & 0 \\
0 & 0 & 0 & 1 \\
\end{bmatrix}}

}

@defproc[(flmat4*flvec4 (M flvector?) (v flvector?)) flvector?]{
Multiplies given vector @${\vec{v}} by matrix @${M}, returning the result @${\vec{v}'}

@$${M=\begin{bmatrix}
M_{0,0} & M_{0,1} & M_{0,2} & M_{0,3} \\
M_{1,0} & M_{1,1} & M_{1,2} & M_{1,3} \\
M_{2,0} & M_{2,1} & M_{2,2} & M_{2,3} \\
M_{3,0} & M_{3,1} & M_{3,2} & M_{3,3} \\
\end{bmatrix}}

@$${\vec{v}=(x_v,y_v,z_v,w_v)}
@$${\vec{v}'=(x_v',y_v',z_v',w_v')}
@$${x_v'=M_{0,0}\cdot x_v+M_{0,1}\cdot y_v+M_{0,2}\cdot z_v+M_{0,3}\cdot w_v}
@$${y_v'=M_{1,0}\cdot x_v+M_{1,1}\cdot y_v+M_{1,2}\cdot z_v+M_{1,3}\cdot w_v}
@$${z_v'=M_{2,0}\cdot x_v+M_{2,1}\cdot y_v+M_{2,2}\cdot z_v+M_{2,3}\cdot w_v}
@$${w_v'=M_{3,0}\cdot x_v+M_{3,1}\cdot y_v+M_{3,2}\cdot z_v+M_{3,3}\cdot w_v}

@$${\begin{bmatrix}x_v'\\y_v'\\z_v'\\w_v'\end{bmatrix}=
\begin{bmatrix}
M_{0,0} & M_{0,1} & M_{0,2} & M_{0,3} \\
M_{1,0} & M_{1,1} & M_{1,2} & M_{1,3} \\
M_{2,0} & M_{2,1} & M_{2,2} & M_{2,3} \\
M_{3,0} & M_{3,1} & M_{3,2} & M_{3,3} \\
\end{bmatrix}\cdot
\begin{bmatrix}x_v\\y_v\\z_v\\w_v\end{bmatrix}}
}

@defproc[(flmat4-ref (m flvector?) (r fixnum?) (c fixnum?)) flonum?]{
  Returns a value found in column @racket[c], row @racket[r] of given matrix @racket[m].
}

@defproc[(flmat4*flmat4 (m1 flvector?) (m2 flvector?)) flvector?]{
  Performs matrix multiplication.
}

@defproc[(flmat4* (mat flvector?) ...) flvector?]{
  Performs chained matrix multiplication from right to left.
}


@section{Transformations in World/Camera Coordinates}

@defmodule[rrll/math/transform4]

This module contains useful matrices for working in world/camera coordinates.

@defproc[(flmat4-rotZ (theta flonum?)) flvector?]{
  Returns a rotation matrix that rotates counter-clockwise around the Z
  axis by given angle.
}

@defproc[(flmat4-rotX (theta flonum?)) flvector?]{
  Returns a rotation matrix that rotates counter-clockwise around the X
  axis by given angle.
}

@defproc[(flmat4-rotY (theta flonum?)) flvector?]{
  Returns a rotation matrix that rotates counter-clockwise around the Y
  axis by given angle.
}

@defproc[(flmat4-translate (x flonum?) (y flonum?) (z flonum?)) flvector?]{
  Returns a translation matrix that translates by given amount along all axes.
}

@defproc[(flmat4-scale (x flonum? 1.0) (y flonum? 1.0) (z flonum? 1.0)) flvector?]{
  Returns a scaling matrix.
}

@defproc[(flmat4-map->camera) flvector?]{
  Creates a transformation matrix that maps from map to world/camera coordinates.
}

@defproc[(flmat4-project-unit (near flonum?) (far flonum?)
			 (width (or/c fixnum? flonum?)) (height (or/c fixnum? flonum?))
			 (pixel-aspect (or/c #f flonum?) #f)) flvector?]{
			 
  Constructs a standard projection matrix for the world/camera
  coordinate system. The screen size is measured from @racket[-1] to
  @racket[1] in both directions.

}

@defproc[(flmat4-project (near flonum?) (far flonum?)
			 (width (or/c fixnum? flonum?)) (height (or/c fixnum? flonum?))
			 (pixel-aspect (or/c #f flonum?) #f)) flvector?]{
Constructs a standard projection matrix for the world/camera coordinate system.
}

@section{Angle Manipulation Helpers}

@defmodule[rrll/math/angles]

This module contains simple functions for working with angles in the
range @${\langle 0;2\pi\rangle}.

Angles in this module and other related modules represented in 2D map
coordinates point in the north direction for zero angle rotating
clockwise as the angle increases.

@centered[
(lt-superimpose
(cc-superimpose
  (hc-append
    (blank 10 10)
    (hline 100 100)
    (arrowhead 10 0))
  (vc-append
    (blank 10 10)
    (vline 100 100)
    (arrowhead 10 (/ (* 3 pi) 2)))
  (dc (lambda (dc dx dy)
(define old-brush (send dc get-brush))
(define old-pen (send dc get-pen))
(send dc set-brush "black" 'transparent)
    (define sangle (- (/ pi 4)))
    (define eangle (/ pi 2))
    (send dc draw-arc (+ dx 1) (+ dy 1) 48 48 sangle eangle)
(send dc set-brush old-brush)
(send dc set-pen old-pen)
    ) 50 50))
  (rb-superimpose
    (blank 75 55)
    (text "θ"))
  (rb-superimpose
      (blank 81.5 81.5)
      (arrowhead 10 (/ (* 5 pi) 4))))
  ]

@defproc[(angle-add (a1 flonum?) (a2 flonum?)) flonum?]{

  Adds two angles and clips the result within the @${2\pi}.

}

@defproc[(angle-subtract (a1 flonum?) (a2 flonum?)) flonum?]{

  Subtracts the second angle from the first returning a nonnegative
  result smaller than @${2\pi}.

}

@defproc[(angle-wrap (theta flonum?)) flonum?]{

  Makes sure given angle is within the @${\langle 0;2\pi\rangle}
  range.

}

@defproc[(angle-flvec3 (theta flonum?)) flvector?]{

  Returns a unit vector pointing in the direction of given
  @racket[theta] azimuth within the plane @${z=0}.

}

@defproc[(azimuth-name (theta flonum?)) string?]{

  Returns the nearest name for given azimuth angle. All eight major
  and semi-major directions are handled.

}

@section{2D Integer Vectors}
@defmodule[rlgrid/vec2]

A convenience structure for representing 2D coordinates in the
grid. The @racket[x] and @racket[y] values can only be
@racket[fixnum?]s.

@defstruct*[vec2
	((x fixnum?)
	 (y fixnum?))]{
    Represents a single cell in a 2D grid - usually implemented as
    @racket[gen:rlgrid]: @racket[tgrid], @racket[stgrid], and
    @racket[vgrid].
}

@defproc[(vec2-add (v1 vec2?) (v2 vec2?)) vec2?]{
    Returns the sum of two @racket[vec2]s.
}

@defproc[(vec2-neg (v vec2?)) vec2?]{
    Returns a @racket[vec2] with both coordinates negated.
}

@defproc[(vec2-sub (v1 vec2?) (v2 vec2?)) vec2?]{
    Subtracts @racket[v2] from @racket[v1] in both coordinates and
    returns the result.
}

@defthing[vec2s:grid (listof vec2?)]{
    List of orthogonal grid directions: north, east, south, west.

    @(vc-append
      (arrow 30 (/ pi 2))
      (hc-append
       (arrow 30 pi)
       (blank 30 30)
       (arrow 30 0))
      (arrow 30 (/ (* 3 pi) 2)))
}

@defthing[vec2s:grid+diagonal (lisof vec2?)]{
    List of orthogonal and diagonal grid directions like
    @racket[vec2s:grid] but also including: northeast, southeast,
    southwest, northwest.

    @(vc-append
      (hc-append
       (arrow 30 (/ (* 3 pi) 4))
       (arrow 30 (/ pi 2))
       (arrow 30 (/ pi 4)))
      (hc-append
       (arrow 30 pi)
       (blank 30 30)
       (arrow 30 0))
      (hc-append
       (arrow 30 (/ (* 5 pi) 4))
       (arrow 30 (/ (* 3 pi) 2))
       (arrow 30 (/ (* 7 pi) 4))))
}

@defproc[(vec2-values (v vec2?)) (values fixnum? fixnum?)]{
    Deconstructs vector value.
}

@defproc[(get-vec2-displacement (d (or/c 'left 'right 'up 'down))) vec2?]{
    Returns @racket[vec2?] displacement for @racket['left], @racket['right],
    @racket['up] and @racket['down].
}

