#lang scribble/manual

@require[scribble-math/dollar]

@title[#:style (with-html5 manual-doc-style)]{RRLL: Color Support}
@author[@author+email["Dominik Pantůček" "dominik.pantucek@trustica.cz"]]

Racket Rogue-Like Library: Color Support

Various utilitis for color manipulation, blending and lighting.

@section{Core Color Functions}

@defmodule[rrll/color/core]

@require["../color/core.rkt"]

Requiring this module raises error if the system word size is less
than 64 bits.

@defthing[alpha-max fixnum? #:value #,(racketvalfont (format "#x~x" alpha-max))]{
  Defines the maximum alpha channel value for 32-bit ARGB color representation.
}

@defthing[alpha-mask fixnum? #:value #,(racketvalfont (format "#x~x" alpha-mask))]{
  Bit mask of the alpha channel part or 32-bit ARGB color representation.
}

@defproc[(split-rgb (color fixnum?)) (values fixnum? fixnum? fixnum?)]{
  Splits given 24-bit RGB value into its 8-bit components.
}

@defproc[(split-argb (color fixnum?)) (values fixnum? fixnum? fixnum? fixnum?)]{
  Splits given 32-bit ARGB value into its 8-bit components.
}

@defproc[(make-rgb (r fixnum?) (g fixnum?) (b fixnum?)) fixnum?]{
  Creates a 24-bit RGB color from 8-bit components.
}

@defproc[(make-argb (a fixnum?) (r fixnum?) (g fixnum?) (b fixnum?)) fixnum?]{
  Creates a 32-bit ARGB color from 8-bit components.
}

@defproc[(extract-argb-alpha (argb fixnum?)) fixnum?]{
  Extracts only alpha-channel value from given 32-bit ARGB color.
}

@defproc[(set-argb-alpha (argb fixnum?) (alpha fixnum?)) fixnum?]{

  Modifies given ARGB value and returns a new one with the alpha
  channel replaced by the explicit value given.

}

@defproc[(rgb-distance^2 (c1 fixnum?) (c2 fixnum?)) fixnum?]{
  Computes the square of euclidean distance in RGB space of the two colors given.

  @$${d=(r_1-r_2)^2+(g_1-g_2)^2+(b_1-b_2)^2}
}

@defproc[(is-argb? (-rgb fixnum?)) boolean?]{

  Returns @racket[#t] if the given @racket[-rgb] value has non-zero
  alpha channel.

}


@section{Color Blending}

@defmodule[rrll/color/blending]

@defproc[(rgb-average (color fixnum?) ...) fixnum?]{
  Averages given RGB values.
}

@defproc[(rgb-average2 (c0 fixnum?) (c1 fixnum?)) fixnum?]{
  Optimized implementation that averages two RGB values.
}

@defproc[(rgb-average4 (c0 fixnum?) (c1 fixnum?) (c2 fixnum?) (c3 fixnum?)) fixnum?]{
  Optimized implementation that averages four RGB values.
}

@defproc[(argb-average (color fixnum?) ...) fixnum?]{
  Averages given ARGB values.
}

@defproc[(argb-average2 (c0 fixnum?) (c1 fixnum?)) fixnum?]{
  Optimized implementation that averages two ARGB values.
}

@defproc[(argb-average4 (c0 fixnum?) (c1 fixnum?) (c2 fixnum?) (c3 fixnum?)) fixnum?]{
  Optimized implementation that averages four ARGB values.
}

@defproc[(alpha-blend-alpha (a1 fixnum?) (a2 fixnum?)) fixnum?]{
  Blends @racket[alpha2] alpha value over @racket[alpha1] value using:

  @$${\alpha=\alpha_1\cdot\alpha_2}

  @$${\alpha_1,\alpha_2\in\langle 0;255\rangle \equiv \langle 0.0;1.0\rangle}
}

@defproc[(rgb-blend-argb (rgb fixnum?) (argb fixnum?)) fixnum?]{
  Blends @racket[argb] over @racket[rgb] based on the alpha channel value.
}

@defproc[(rgb-blend-rgb+alpha (rgb0 fixnum?) (rgb1 fixnum?) (alpha fixnum?)) fixnum?]{
  Blends @racket[rgb1] over @racket[rgb0] based on the @racket[alpha] value.

  @$${\alpha\in\langle 0;255\rangle\equiv\langle 0.0;1.0\rangle}
}

@defproc[(rgb-blend-argb+alpha (rgb fixnum?) (argb fixnum?) (alpha fixnum?)) fixnum?]{
  Blends @racket[argb] over @racket[rgb] based on the combined alpha channel and given @racket[alpha] value.

  @$${\alpha\in\langle 0;255\rangle\equiv\langle 0.0;1.0\rangle}

  Uses @racket[alpha-blend-alpha] to merge @racket[argb]'s alpha channel with the @racket[alpha] value.
}

@defproc[(rgb-linear (c0 fixnum?) (c1 fixnum?) (f fixnum?)) fixnum?]{
  Linearly interpolates two RGB colors using given @racket[f] factor.

  @$${f\in\langle 0;256\rangle\equiv\langle 0.0;1.0\rangle}
}

@defproc[(argb-linear (c0 fixnum?) (c1 fixnum?) (f fixnum?)) fixnum?]{
  Linearly interpolates two ARGB colors using given @racket[f] factor.

  @$${f\in\langle 0;256\rangle\equiv\langle 0.0;1.0\rangle}
}

@defproc[(rgb-bilinear (r0c0 fixnum?) (r0c1 fixnum?) (r1c0 fixnum?) (r1c1 fixnum?) (u fixnum?) (v fixnum?)) fixnum?]{
  Performs bilinear interpolation between four RGB colors.

  @$${u,v\in\langle 0;256\rangle\equiv\langle 0.0;1.0\rangle}
}

@defproc[(argb-bilinear (r0c0 fixnum?) (r0c1 fixnum?) (r1c0 fixnum?) (r1c1 fixnum?) (u fixnum?) (v fixnum?)) fixnum?]{
  Performs bilinear interpolation between four ARGB colors.

  @$${u,v\in\langle 0;256\rangle\equiv\langle 0.0;1.0\rangle}
}



@section{Color Names}

@defmodule[rrll/color/names]

@[define cga-colors '(
(define BLACK #x0)
(define BLUE #x0000AA)
(define GREEN #x00AA00)
(define CYAN #x00AAAA)
(define RED #xAA0000)
(define MAGENTA #xAA00AA)
(define BROWN #xAA5500)
(define LIGHTGRAY #xAAAAAA)
(define DARKGRAY #x555555)
(define LIGHTBLUE #x5555FF)
(define LIGHTGREEN #x55FF55)
(define LIGHTCYAN #x55FFFF)
(define LIGHTRED #xFF5555)
(define LIGHTMAGENTA #xFF55FF)
(define YELLOW #xFFFF55)
(define WHITE #xFFFFFF))]

The following 16 @racket[fixnum?] constants are defined:

@for/list[((cdef (in-list cga-colors)))]{
  @(racketvalfont (format "~a " (cadr cdef)))
}

@defthing[CGA-COLORS (vectorof fixnum?)]{
  Contains the ordered vector of 16 aforementioned CGA colors.
}


@section{Flonum Colors}

@defmodule[rrll/color/flcolor]

This module provides @racket[flonum?]-based RGB and ARGB processing
used when color interpolation is required.

@defproc[(make-flrgb (r flonum?)
		     (g flonum?)
		     (b flonum?)) flvector?]{
  Creates new RGB triplet. The result is @racket[(flvector b g r)] to
  keep the in-memory ordering the same as with byte-channel RGB.
}

@defproc[(make-flargb (a flonum?)
		      (r flonum?)
		      (g flonum?)
		      (b flonum?)) flvector?]{
  Creates new RGB triplet. The result is @racket[(flvector b g r a)] to
  keep the in-memory ordering the same as with byte-channel ARGB.
}

@defproc[(split-flrgb (fc flvector?))
	(values flonum? flonum? flonum?)]{
  Returns the red, green and blue channels as separate values. See
  @racket[make-flrgb] for the in-memory data ordering.
}

@defproc[(split-flargb (fc flvector?))
	(values flonum? flonum? flonum? flonum?)]{
  Returns the alpha, red, green and blue channels as separate
  values. See @racket[make-flargb] for the in-memory data ordering.
}

@defproc[(rgb->flrgb (c fixnum?)) flvector?]{
  Convers given RGB color (see @racket[make-rgb]) to flvector? RGB
  represenation (see @racket[make-flrgb]).
}

@defproc[(flrgb->rgb (fc flvector?)) fixnum?]{
  Converts given @racket[flonum?] RGB color to single-@racket[fixnum?]
  color. See @racket[make-flrgb] for the former and @racket[make-rgb]
  for the latter.
}

@defproc[(argb->flargb (c fixnum?)) flvector?]{
  Convers given ARGB color (see @racket[make-argb]) to flvector? ARGB
  represenation (see @racket[make-flargb]).
}

@defproc[(flargb->argb (fc flvector?)) fixnum?]{
  Converts given @racket[flonum?] ARGB color to single-@racket[fixnum?]
  color. See @racket[make-flargb] for the former and @racket[make-argb]
  for the latter.
}

@defproc[(flrgb-add (fc1 flvector?) (fc2 flvector?)) flvector?]{
  Adds two @racket[flonum?] RGB triplets together. No clamping is performed.
}

@defproc[(flargb-add (fc1 flvector?) (fc2 flvector?)) flvector?]{
  Adds two @racket[flonum?] ARGB quadruplets together. No clamping is performed.
}

@defproc[(flrgb-div (fc flvector?) (n flonum?)) flvector?]{
  Divides all components of a @racket[flonum?] RGB color by given number.
}

@defproc[(flargb-div (fc flvector?) (n flonum?)) flvector?]{
  Divides all components of a @racket[flonum?] ARGB color by given number.
}

@defproc[(flrgb-mul (fc flvector?) (n flonum?)) flvector?]{
  Multiplies all components of a @racket[flonum?] RGB color by given number.
}

@defproc[(flargb-mul (fc flvector?) (n flonum?)) flvector?]{
  Multiplies all components of a @racket[flonum?] ARGB color by given number.
}



@section{Color Lighting}

@defmodule[rrll/color/lighting]

This module provides reference implementaiton of colored lighting
routines to be used by the renderer mostly for Goraud shading.

@defproc[(make-light (r flonum?)
		     (g flonum?)
		     (b flonum?)) flvector?]{
  Creates a colored light data structure of three fload numbers in single @racket[flvector?].
}

@defproc[(light-rgb (rgb fixnum?)
		    (l flvector?)) fixnum?]{
  Applies colored light to given color value and returns the lit color.

  Each component is adjusted according to:

  @$${c=c_0\cdot l}

  The result is @racket[fxclamp]ed in @${\langle 0;255\rangle}.
}

@defproc[(light-argb (argb fixnum?)
		     (l flvector?)) fixnum?]{
  Applies colored light to given color value and returns the lit color
  like @racket[light-rgb]. The alpha component of the color is kept
  unchanged.
}

@defproc[(light-add (l1 flvector?) (l2 flvector?)) flvector?]{
  An alias for @racket[flrgb-add].
}


@section{Random Colors}

@defmodule[rrll/color/random]

Helper module for random RGB and ARGB colors where needed.

@defproc[(random-rgb) fixnum?]{
  Generates valid random RGB color.
}

@defproc[(random-argb) fixnum?]{
  Generates valid random ARGB quadruplet.
}

@section{Color Interpolation}

@defmodule[rrll/color/interpolate]

This module implements a special format with more bits per channel to
allow simple iteration over (possibly multi-dimensional) field of
color values.

@defproc[(rgb+add (rgb+1 fixnum?) (rgb+2 fixnum?)) fixnum?]{

Adds two RGB+ values maintaining the bit format.

}

@defproc[(make-rgb+ (r flonum?) (g flonum?) (b flonum?)) fixnum?]{

Returns RGB+ representation of given RGB triplet.

}

@defproc[(rgb+>rgb (rgb+ fixnum?)) fixnum?]{

Converts to standard RGB representation.

}

@defproc[(argb+add (argb+1 fixnum?) (argb+2 fixnum?)) fixnum?]{

Adds two ARGB+ values maintaining the bit format.

}

@defproc[(make-argb+ (a flonum?) (r flonum?) (g flonum?) (b flonum?)) fixnum?]{

Returns ARGB+ representation of given ARGB quadruplet.

}

@defproc[(argb+>argb (argb+ fixnum?)) fixnum?]{

Converts to standard ARGB representation.

}

@section{Fixed-Point Color Light}

@defmodule[rrll/color/fxlight]

@defproc[(fxlight-rgb (rgbc fixnum?) (rgbl fixnum?)) fixnum?]{

Lights given @racket[rgbc] color using the @racket[rgbl] light. The
light components are interpreted as follows:

@itemlist[
	@item{@racket[0] is no light at all}
	@item{@racket[127] is 100% of the original color component intensity}
	@item{@racket[255] is almost 200% (256 would be the actual 200% intensity)}
	]

These values were chosen purely for performance reasons.

}

@defproc[(fxlight-argb (argbc fixnum?) (rgbl fixnum?)) fixnum?]{

Lights the @racket[argbc] color using @racket[rgbl] light, preserving
the alpha channel of the source.

}
