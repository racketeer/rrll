#lang scribble/manual

@title{RRLL: ANSI Buffers}
@author[@author+email["Dominik Pantůček" "dominik.pantucek@trustica.cz"]]

Racket Rogue-Like Library: ANSI Buffers

Manipulating rectangular character buffers with foreground and background color.

@defmodule[rrll/abuf]

This module provides constructor bindings from
@racketmodname[rrll/abuf/private/core] and all bindings from
@racketmodname[rrll/abuf/output] and
@racketmodname[rrll/abuf/drawing].

@section{Core ANSI Buffers Functions}

@defmodule[rrll/abuf/private/core]

@defstruct*[abuf
  ((width fixnum?)
   (height fixnum?)
   (cells (vectorof acell?))
   (offset fixnum?)
   (stride fixnum?)
   (buffer bytes?))]{
    Rectangular area of terminal filled with @racket[acell?]s.
}

@defproc[(make-abuf (width fixnum?) (height fixnum?)) abuf?]{
    Creates a new, empty @racket[abuf?] of given
@racket[width] and @racket[height].
}

@defproc[(make-sub-abuf (ab abuf?) (x fixnum?) (y fixnum?) (width fixnum?) (height fixnum?)) abuf?]{
    Creates a new @racket[abuf?] which is a rectangular part of already existing @racket[abuf?].
}

@section{ANSI Cells}

@defmodule[rrll/abuf/private/acell]

@defstruct*[acell
  ((char char?)
   (fg fixnum?)
   (bg fixnum?)
   (dirty boolean?))
   #:mutable
   #:transparent ]{
    A structure representing single ANSI cell on screen.
}

@defproc[(acell=? (a acell?) (b acell?)) boolean?]{
    Compares two @racket[ansi-cell?] values and returns true if one
    would look like the other when output. It compares the character and
    both colors of the cell.
}

@defproc[(update-acell! (cell acell?) (char char?)
			(#:fg fg (or/c fixnum? #f) #f)
			(#:bg bg (or/c fixnum? #f) #f)
			(#:set-dirty set-dirty boolean? #t)) void?]{
    Updates single cell character and foreground and background color and optionally
    sets the dirty bit if anything changed and @racket[set-dirty] is @racket[#t].
}

@defproc[(copy-acell! (dst acell?) (src acell?)
		      (#:only-dirty only-dirty boolean? #f)
		      (#:clear-dirty clear-dirty boolean? #f)
		      (#:set-dirty set-dirty boolean? #t)) void?]{
    Copies single cell contents into another and optionally
    sets the dirty bit if anything changed and @racket[set-dirty] is @racket[#t].

    If @racket[only-dirty] is @racket[#t], the copy is performed only if the @racket[src]
    cell is dirty.

    If anything changed, @racket[clear-dirty] is @racket[#t] and the source cell was dirty
    its dirty flag is set back to @racket[#f].

    If anything changed and @racket[set-dirty] is @racket[#t], the destination cell
    dirty flag is set to @racket[#t].
}

@section{Output to Terminal}

@defmodule[rrll/abuf/output]

@defproc[(display-abuf (ab abuf?)
		       (x0 fixnum? 0)
		       (y0 fixnum? 0)
		       (#:only-dirty only-dirty boolean? #t)
		       (#:clear-dirty clear-dirty boolean? #t)
		       (#:linear linear boolean #f)) void?]{
    Displays given @racket[abuf?] on terminal.

    The picture is positioned at coordinates
    [@racket[x0],@racket[y0]] with [0,0] being the upper left
    corner of the terminal and X coordinate growing to the right and Y
    coordinate growing down.

    If @racket[only-dirty] is @racket[#t] then only cells marked dirty
    as identified by @racket[acell-dirty?] are displayed.

    For each cell output with @racket[clear-dirty] being @racket[#t]
    and the source cell was dirty its dirty flag is set back to
    @racket[#f].

    If @racket[linear] is @racket[#t], cursor movement is restricted
    to newlines and forward movement on the current line.
}

@section{ANSI Pictures Drawing}

@defmodule[rrll/abuf/drawing]

@defproc[(abuf-putchar! (ab abuf?)
			(x fixnum?)
			(y fixnum?)
			(ch char?)
			(#:fg fg fixnum? #,(racketvalfont (format "#x~x" #x7f7f7f)))
			(#:bg bg fixnum? #,(racketvalfont (format "#x~x" 0)))
			(#:set-dirty set-dirty boolean? #t)) void?]{
    Puts a character into a @racket[abuf?] at specified position
    using given foreground and background colors. By default it sets
    modified cell's dirty bit to @racket[#t].
}

@defproc[(abuf-putstring! (ab abuf?)
			  (x fixnum?)
			  (y fixnum?)
			  (str string?)
			  (#:fg fg fixnum? #,(racketvalfont (format "#x~x" #x7f7f7f)))
			  (#:bg bg fixnum? #,(racketvalfont (format "#x~x" 0)))
			  (#:set-dirty set-dirty boolean? #t)) void?]{
    Draws given @racket[str] to given position in the picture in given color.

    If @racket[set-dirty] is @racket[#t], sets the
    @racket[acell-dirty?] flag on the destination cells if they have
    been changed.
}

@defproc[(abuf-clear! (ab abuf?)
		      (ch char?)
		      (#:fg fg fixnum? #,(racketvalfont (format "#x~x" #x7f7f7f)))
		      (#:bg bg fixnum? #,(racketvalfont (format "#x~x" 0)))
		      (#:set-dirty set-dirty boolean? #t)) void?]{
    Clears the @racket[abuf?] with specified character, color
    and background attributes.

    If @racket[set-dirty] is @racket[#t], sets the
    @racket[acell-dirty?] flag on the destination cells if they have
    been changed.
}

@defproc[(abuf-bar! (dst abuf?)
		    (x fixnum?)
		    (y fixnum?)
		    (w fixnum?)
		    (h fixnum?)
		    (#:fg fg fixnum? #,(racketvalfont (format "#x~x" #x7f7f7f)))
		    (#:bg bg fixnum? #,(racketvalfont (format "#x~x" 0)))
		    (#:set-dirty set-dirty boolean? #t)) void?]{
    Draws a filled rectangle (a bar) of given width and height with
    character, color and background attributes.

    If @racket[set-dirty] is @racket[#t], sets the
    @racket[acell-dirty?] flag on the destination cells if they have
    been changed.
}

@defproc[(abuf-box! (dst abuf?)
		    (x fixnum?)
		    (y fixnum?)
		    (w fixnum?)
		    (h fixnum?)
		    (type (or/c 'ascii 'single 'dashed 'thick 'double) 'single)
		    (#:fg fg fixnum? #,(racketvalfont (format "#x~x" #x7f7f7f)))
		    (#:bg bg fixnum? #,(racketvalfont (format "#x~x" 0)))
		    (#:set-dirty set-dirty boolean? #t)) void?]{
    Draws a rectangle outline (a box) of given width and height with
    character based on @racket[type], color and background attributes.

    If @racket[set-dirty] is @racket[#t], sets the
    @racket[acell-dirty?] flag on the destination cells if they have
    been changed.
}

@defproc[(abuf-overlay! (dst abuf?)
			(src abuf?)
			(dx fixnum? 0)
			(dy fixnum? 0)
			(#:w sw (or/c fixnum? #f) #f)
			(#:h sh (or/c fixnum? #f) #f)
			(#:sx sx fixnum? 0)
			(#:sy sy fixnum? 0)
		      	(#:only-dirty only-dirty boolean? #f)
		      	(#:clear-dirty clear-dirty boolean? #f)
		      	(#:set-dirty set-dirty boolean? #t)) void?]{
    Overlays @racket[src] over @racket[dst] at coordinates
    @racket[dst-x],@racket[dst-y]. The dimensions of the @racket[src]
    region are taken from @racket[src]'s dimensions if the arguments are
    @racket[#f]. By default it sets the dirty bit on all modified cells.
}

