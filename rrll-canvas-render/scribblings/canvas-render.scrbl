#lang scribble/manual

@title{RRLL: Rendering Canvas}
@author[@author+email["Dominik Pantůček" "dominik.pantucek@trustica.cz"]]

Racket Rogue-Like Library: Rendering Canvas

Extension for @racketmodname[rrll/canvas] that uses unused bits for
storing the depth and id buffers in the canvas data.

@section{Rendering Canvas Procedures}

@defmodule[rrll/canvas/render]

@defproc[(wfl->fx (wfl flonum?)) fixnum?]{

Converts floating point representation of w-value to fixed-point
representation for storing in canvas data.

}

@defproc[(wfxmerge (rgb fixnum?) (wfx fixnum?)) fixnum?]{

Merges 24-bit RGB color with fixed-point representation of w-value.

}

@defproc[(wfxextract (wrgb fixnum?)) fixnum?]{

Extracts the w-buffer @racket[fixnum?] value from given combined data.

}

@defproc[(wfxtest (wrgb fixnum?) (wfx fixnum?)) boolean?]{

Tests whether given @racket[wfx] value is greater than the extracted
value from given @racket[wrgb].

}

@defproc[(wfxtest>= (wrgb fixnum?) (wfx fixnum?)) boolean?]{

Tests whether given @racket[wfx] value is greater than or equal to the
extracted value from given @racket[wrgb].

}

@defproc[(wfxidmerge (rgb fixnum?) (wfx fixnum?) (id fixnum?)) fixnum?]{

Merges the color value with fixed-point w-buffer value and numerical
surface identifier to one value.

}

@defproc[(widextract (iwrgb fixnum?)) fixnum?]{

Extracts id value from given combined color, w-buffer and id composite
value.

}

@defproc[(widmerge (wrgb fixnum?) (id fixnum?)) fixnum?]{

Merges id value into color (possibly with wfx information).

}

@defproc[(canvas-getwid (can canvas?) (x fixnum?) (y fixnum?)) fixnum?]{

Extracts id value from composite pixel data at given location of the
provided @racket[canvas?].

}

@defproc[(wfx->rgb (wfx fixnum?)) fixnum?]{

Converts the fixed-point @racket[wfx] value into a single color of
given color scale from black over blue to white. Used by
@racket[wbuffer-w->canvas].

}

@defproc[(wbuffer->canvas (wcan canvas?)) canvas?]{

Creates a @racket[canvas?] with color representation of the
fixed-point w-buffer values from given @racket[canvas?].

}

@defproc[(idbuffer->canvas (wcan canvas?)) canvas?]{

Creates a @racket[canvas?] with color representation of the
fixed-point w-buffer values from given @racket[canvas?].

}

@defproc[(widwrap (id fixnum?)) fixnum?]{

Makes sure the id is not bigger than the internal maximum value.

}
