#lang racket/base

(require rrll/unsafe/fixnum
         rrll/unsafe/flonum)

(provide color-bits
         color-mask

         w-mult:fl
         w-max
         w-shift
         w-bits
         w-near:fl

         id-shift
         id-max

         w&id-mask
         w&color-mask
         color&id-mask)

(define color-bits 24)
(define color-mask (fx- (fxlshift 1 color-bits) 1))

(define w-bits 24)
(define w-shift color-bits)
(define w-max (fx- (fxlshift 1 w-bits) 1))
(define w-mask (fxlshift w-max w-shift))
(define w-max:fl (fx->fl w-max))

(define w-near:fl 0.1)
(define w-mult:fl (fl/ w-max:fl 50.0)) ; Fixed for n=0.1, projection max w = 2n^2 (n=near)
(define w-mult (fl->fx w-mult:fl))

(define id-bits 12)
(define id-shift (fx+ w-shift w-bits))
(define id-max (fx- (fxlshift 1 id-bits) 1))
(define id-mask (fxlshift id-max id-shift))

(define w&id-mask (fxior w-mask id-mask))
(define w&color-mask (fxior w-mask color-mask))
(define color&id-mask (fxior color-mask id-mask))
