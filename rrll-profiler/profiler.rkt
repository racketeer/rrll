#lang racket/base

(require (for-syntax racket/base
                     syntax/parse)
         racket/match
         racket/format)

(provide with-profiler)

(struct profres
  (name iterations runtime pixels)
  #:methods gen:custom-write
  ((define (write-proc v port mode)
     (match-define (profres name iterations runtime pixels) v)
     (define per-second (/ (* iterations 1000) runtime))
     (define pixels-per-second (/ (* pixels 1000) runtime))
     (display
      (format "#<profiler-result ~a~a op/s ~a, i=~a, px=~a, t=~ams>"
              (if (> pixels 0)
                  (format "~a px/s | "
                          (~a #:width 15 #:align 'right
                              (real->decimal-string pixels-per-second 3)))
                  "")
              (~a #:width 15 #:align 'right
                  (real->decimal-string per-second 3))
              name
              iterations
              pixels
              runtime
              ) port))))

(define-syntax (with-profiler stx)
  (syntax-parse stx
    ((_ name
        (~optional (~seq (~and #:iterations iterations-kw) iterations)
                   #:defaults ((iterations #f)))
        (~optional (~seq (~and #:time runtime-kw) max-runtime)
                   #:defaults ((max-runtime #f)))
        #:setup setup ... #:loop (iter) body ...
        #:finish finish ...)
     (unless (or (attribute iterations-kw)
                 (attribute runtime-kw))
       (error 'with-profiler "You must specify #:iterations or #:time!"))
     #`(let ()
         setup ...
         (define start-time (current-inexact-milliseconds))
         #,@(if (attribute runtime-kw)
                #'((define max-run-time-ms (* max-runtime 1000)))
                '())
         (let loop ((iter 0)
                    (pixels 0))
           #,@(if (attribute runtime-kw)
                  #'((define end-time (current-inexact-milliseconds))
                     (define run-time (- end-time start-time)))
                  '())
           (cond (#,(if (attribute runtime-kw)
                        #'(< run-time max-run-time-ms)
                        #'(< iter iterations))
                  (define iter-pixels
                    (let ()
                      body ...))
                  (loop (add1 iter)
                        (if (number? iter-pixels)
                            (+ pixels iter-pixels)
                            pixels)
                        ))
                 (else
                  finish ...
                  (define end-time (current-inexact-milliseconds))
                  (define run-time (- end-time start-time))
                  (profres (quote name) iter run-time pixels))))))))
