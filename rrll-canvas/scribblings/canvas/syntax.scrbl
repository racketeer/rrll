#lang scribble/manual

@require[(for-syntax racket/base) rrll/helper/syntax]

@provide[defdraw defoverlay]

@define-syntax[(defdraw stx)
  (syntax-case stx ()
    ((_ (name dst args ...)  doc ...)
     (with-syntax ((name/a (make-variant-id stx "~a/a" #'name)))
     #'(begin
         (defproc (name (dst canvas?)
     		       args ...) void? doc ...)
         (defproc (name/a (dst canvas?)
     		       args ...) void?
		       "The same as "
		       (racketidfont (symbol->string 'name))
		       " but blending the ARGB color over current destination."
		       )
	))))]

@define-syntax[(defoverlay stx)
  (syntax-case stx ()
    ((_ (name dst args ...)  doc ...)
     (with-syntax ((name/a (make-variant-id stx "~a/a" #'name))
     		   (name/b (make-variant-id stx "~a/b" #'name))
		   (name/a/b (make-variant-id stx "~a/a/b" #'name)))
     #'(begin
         (defproc (name (dst canvas?)
     		       args ...) void? doc ...)
         (defproc (name/a (dst canvas?)
     		       args ...) void?
		       "The same as "
		       (racketidfont (symbol->string 'name))
		       " but blending the ARGB color over current destination."
		       )
	(defproc (name/b (alpha fixnum?)
		 	 (dst canvas?)
     		       args ...) void?
		       "The same as "
		       (racketidfont (symbol->string 'name))
		       " but blending with "
		       (racketidfont "alpha")
		       " over current destination."
		       )
	(defproc (name/a/b (alpha fixnum?)
		      	   (dst canvas?)
     		       args ...) void?
		       "The same as "
		       (racketidfont (symbol->string 'name))
		       " but blending with "
		       (racketidfont "alpha")
		       " combined with alpha channel from source ARGB over current destination."
		       )
		       ))))]

