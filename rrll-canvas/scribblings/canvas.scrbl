#lang scribble/manual

@require[scribble-math/dollar]

@title[#:style (with-html5 manual-doc-style)]{RRLL: Canvas}
@author[@author+email["Dominik Pantůček" "dominik.pantucek@trustica.cz"]]

Racket Rogue-Like Library: Drawing (A)RGB Canvas

@defmodule[rrll/canvas]

This module re-provides all bindings from @racketmodname[rrll/canvas/core],
@racketmodname[rrll/canvas/pixels], @racketmodname[rrll/canvas/drawing],
@racketmodname[rrll/canvas/transform], @racketmodname[rrll/canvas/bitmap] and
@racketmodname[rrll/canvas/overlay].

@section{Core Canvas}

@defmodule[rrll/canvas/core]

Provides the raw @racket[canvas] struct and its constructors.

@defstruct*[canvas ((width fixnum?)
		    (height fixnum?)
		    (data fxvector?)
		    (offset fixnum?)
		    (stride fixnum?))]{
  Storage for canvas pixels.
}

@defproc[(make-canvas (w fixnum?) (h fixnum?)) canvas?]{
  Creates new canvas of given size.
}

@defproc[(make-sub-canvas (c canvas?)
			  (x fixnum?)
			  (y fixnum?)
			  (w fixnum?)
			  (h fixnum?)) canvas?]{
  Creates new canvas which is part of already existing canvas and
  shares the backing store with the original one.
}

@section{Direct Pixel Access}

@defmodule[rrll/canvas/pixels]

This module contains procedures for direct pixel access of the canvas.
It is not intended for general use.

@defproc[(canvas-putpixel! (can canvas?)
			   (x fixnum?)
			   (y fixnum?)
			   (color fixnum?)) void?]{
  Changes given pixel to specified color. Handles clipping
  correctly.
}

@defproc[(unsafe-canvas-putpixel! (can canvas?)
				  (x fixnum?)
				  (y fixnum?)
				  (color fixnum?)) void?]{
  Changes given pixel to specified color without any clipping.
}

@defproc[(canvas-getpixel (can canvas?)
			  (x fixnum?)
			  (y fixnum?)) fixnum?]{
  Gets pixel from given canvas. If a pixel outside the canvas is chosen,
  @racket[0] is returned.
}

@defproc[(unsafe-canvas-getpixel (can canvas?)
			  	 (x fixnum?)
			  	 (y fixnum?)) fixnum?]{
  Gets pixel from given canvas. No bounds checks are performed.
}

@section{Canvas Drawing}

@defmodule[rrll/canvas/drawing]

@require["canvas/syntax.scrbl"]

@defdraw[(draw-pixel dst
		     (x fixnum?)
		     (y fixnum?)
		     (color fixnum? #,(racketvalfont (format "#x~x" #xff000000))))]{
  Draws a single pixel to canvas using given @racket[rgb] color.
}

@defdraw[(draw-clear dst
		     (color fixnum? #,(racketvalfont (format "#x~x" #xff000000))))]{
  Clears canvas using specified color.
}

@defdraw[(draw-bar dst
		   (x fixnum?)
		   (y fixnum?)
		   (w fixnum?)
		   (h fixnum?)
		   (color fixnum? #,(racketvalfont (format "#x~x" #xff000000))))]{
  Fills rectangular bar with given color. Performs necessary clipping.
}

@defdraw[(draw-hline dst
		     (x fixnum?)
		     (y fixnum?)
		     (w fixnum?)
		     (color fixnum? #,(racketvalfont (format "#x~x" #xff000000))))]{
  Draws a horizontal line using given color. Performs necessary clipping.
}

@defdraw[(draw-vline dst
		     (x fixnum?)
		     (y fixnum?)
		     (h fixnum?)
		     (color fixnum? #,(racketvalfont (format "#x~x" #xff000000))))]{
  Draws a vertical line using given color. Performs any clipping necessary.
}

@defdraw[(draw-line dst
		    (x0 fixnum?)
		    (y0 fixnum?)
		    (x1 fixnum?)
		    (y1 fixnum?)
		    (color fixnum? #,(racketvalfont (format "#x~x" #xff000000))))]{
  Draws a line using Bresenham's line-drawing algorithm using the color specified.
  Performs necessary clipping.
}

@defdraw[(draw-circle dst
		      (xc fixnum?)
		      (yc fixnum?)
		      (radius fixnum?)
		      (color fixnum? #,(racketvalfont (format "#x~x" #xff000000))))]{
  Draws a circle using Bresenham's circle-drawing algorithm centered at
  the point @racket[xc], @racket[yc] with given @racket[radius]. Performs
  any clipping necessary.
}

@defdraw[(fill-circle dst
		      (xc fixnum?)
		      (yc fixnum?)
		      (radius fixnum?)
		      (color fixnum? #,(racketvalfont (format "#x~x" #xff000000))))]{
  Fills a circle using Bresenham's circle-drawing algorithm centered at
  the point @racket[xc], @racket[yc] with given @racket[radius]. Performs
  any clipping necessary.
}


@defdraw[(fill-triangle dst
			(x1 fixnum?)
			(y1 fixnum?)
			(x2 fixnum?)
			(y2 fixnum?)
			(x3 fixnum?)
			(y3 fixnum?)
			(color fixnum? #,(racketvalfont (format "#x~x" #xff000000))))]{
  Fills a triangle specified by the three points' coordinates. Performs
  any clipping necessary.
}

@defdraw[(fill-flood dst
		     (x fixnum?)
		     (y fixnum?)
		     (color fixnum? #xff000000)
		     (#:wrap? wrap? boolean? #f))]{

  Flood-fill with given @racket[color] starting at given coordinates
  wrapping around if @racket[wrap?] is @racket[#t].

}

@section{Transformations}

@defmodule[rrll/canvas/transform]

Full-canvas transformations.

@defproc[(canvas-transpose! (dst canvas?) (src canvas?)) void?]{
  Transpose given @racket[src] into @racket[dst]. The source width must
  be the same as destination height and vice-versa.
}

@defproc[(canvas-downscale! (dst canvas?)
			    (src canvas?)
			    (sw boolean? #t)
			    (sh boolean? #t)) void?]{
  Downcales given source canvas to the destination one by width
  if @racket[sw] is @racket[#t] and/or by height if @racket[sh]
  is @racket[#t].
}

@section{Bitmap Saving and Loading}

@defmodule[rrll/canvas/bitmap]

Procedures for canvas saving into JPEG or PNG images and loading from
any file type supported by @racketmodname[racket/draw].

@defproc[(load-bitmap (filename-or-data (or/c string? bytes?))) canvas?]{
  Loads given file or image data into a @racket[canvas?].
}

@defproc[(canvas->bitmap% (can canvas?)) (is-a?/c bitmap%)]{
  Converts given canvas to @racketmodname[racket/draw] @racket[bitmap%].
}

@defproc[(save-bitmap (canvas canvas?) (filename string?)) void?]{
  Saves given @racket[canvas?] into specified @racket[filename]. The file
  extension must be either @racket[".png"], @racket[".jpg"] or @racket[".jpeg"].
}

@section{Mipmapping}

@defmodule[rrll/mipmap/core]

Special @racket[canvas] extension to allow anisotropic sampling
for high-quality downscaling and texturing.

@defstruct*[(mipmap canvas)
	    ((log2width fixnum?)
	     (log2height fixnum?)
	     (w-offsets fxvector?)
	     (h-offsets fxvector?)
	     (mmwidth:fl flonum?)
	     (mmheight:fl flonum?)
	     (row-shift fixnum?))]{
  Holds the extended canvas containing all mipmaps required. Both its
  width and height is twice the original with the last pixel in both
  dimensions unused.
}

@subsection{Mipmap Construction}

@defmodule[rrll/mipmap/build]

@defproc[(canvas->mipmap (can canvas?)) mipmap?]{
  Creates a @racket[mipmap] from @racket[canvas]. The source
  dimensions must both be a power of 2.
}

@section{Advanced Sampling}

@defmodule[rrll/canvas/sampling]

This module contains methods for direct bilinear sampling of @racket[canvas?] structs
- useful mainly for upscaling.

@require[rrll/helper/syntax]

@define-syntax[(defsampler stx)
  (syntax-case stx ()
    ((_ (name dst args ...) ret doc ...)
     (with-syntax ((name/a (make-variant-id stx "~a/a" #'name)))
     #'(begin
         (defproc (name args ...) ret doc ...)
         (defproc (name/a args ...) ret
		       "The same as "
		       (racketidfont (symbol->string 'name))
		       " but interpolating the ARGB color instead of RGB."
		       )
	))))]

@defsampler[(canvas-sample (can canvas?)
			   (u flonum?)
			   (v flonum?)) fixnum?]{
 Performs bilinear sampling on given canvas. The @racket[u]
  and @racket[v] coordinates are in canvas space ranging from
  @racket[0] to the width and height of the canvas.

  If the coordinates are outide of canvas range, they are interpreted
  as if the canvas was infinitely repeated in both vertical and horizontal
  directions.
}

@section{Advanced Mipmap Sampling}

@defmodule[rrll/mipmap/sampling]

This moduel contains methods for trilinear/quadrilinear anisotropic sampling
of @racket[mipmap?] canvas extensions.

@defsampler[(mipmap-sample (mm mipmap?)
			   (tw flonum?)
			   (th flonum?)
			   (u flonum?)
			   (v flonum?)) fixnum?]{
  Performs bilinear/trilinear/quadrilinear sampling of given mipmap based
  on requested @racket[tw] and @racket[th] dimensions and the @racket[u]
  and @racket[v] coordinates.

  If the coordinates are outside the mipmap, the sampled coordinates are
  wrapped around in both directions.
}

@defsampler[(mipmap-sample-quad (mm mipmap?)
				(tw flonum?)
				(th flonum?)
				(u00 flonum?)
				(v00 flonum?)
				(u01 flonum?)
				(v01 flonum?)
				(u10 flonum?)
				(v10 flonum?)
				(u11 flonum?)
				(v11 flonum?))
				(values fixnum? fixnum? fixnum? fixnum?)]{
  Samples four texels at once.
}

@section{Canvas Overlays}

@defmodule[rrll/canvas/overlay]

@defoverlay[(draw-canvas dst
			 (src canvas?)
			 (dx fixnum? 0)
			 (dy fixnum? 0)
			 (sx fixnum? 0)
			 (sy fixnum? 0)
			 (sw (or/c fixnum? #f) #f)
			 (sh (or/c fixnum? #f) #f))]{
      Draws @racket[src] into the backing @racket[canvas?] at
      position @racket[(dx dy)] performing any clipping required.
}

@defoverlay[(scale-canvas dst
			 (src canvas?)
			 (x fixnum?)
			 (y fixnum?)
			 (w fixnum?)
			 (h fixnum?)
			 (sx fixnum? 0)
			 (sy fixnum? 0)
			 (sw (or/c fixnum? #f) #f)
			 (sh (or/c fixnum? #f) #f))]{
    Draws @racket[src] into the backing @racket[canvas?] at position
    @racket[(x y)] scaled to @racket[w]@${\times}@racket[h]
    performing any clipping required.
}

@section{Mipmap Overlays}

@defmodule[rrll/mipmap/overlay]

@defoverlay[(scale-mipmap dst
			 (src mipmap?)
			 (dx fixnum?)
			 (dy fixnum?)
			 (dw fixnum?)
			 (dh fixnum?))]{
    Draws @racket[src] into the backing @racket[canvas?] at position
    @racket[(dx dy)] scaled to @racket[dw]@${\times}@racket[dh]
    performing any clipping required.
}

@section{Canvas Syntax Definers}

@defmodule[rrll/canvas/syntax]

This module provides utility syntaxes for defining multiple variants of
procedures that work with @racket[canvas?].

@defform[(define/provide-draw [#:b] (name dst args ...) body ...)]{
  Defines a procedure that draws something onto @racket[dst]. The plain
  RGB variant @racket[name] is defined and ARGB blending variant @racket[name/a]
  is created as well.

  The procedure body must do the actual drawing using the following
  procedure provided as local binding:

  @defproc[(draw (doff fixnum?) (sc fixnum?)) void?]{
    Draws a single pixel to destination canvas, possibly performing
    any source to destination blending required.
  }

  The source of the pixel color to be painted may be a constant color
  or color sampled from some resource like @racket[canvas?] or @racket[mipmap?].

  With the @racket[#:b] keyword argument more functions are created:
  Plain RGB variant with explicit alpha value @racket[name/b] and ARGB
  blending variant with explicit alpha value on top @racket[name/a/b].

  Also, within the body the @racket[linear] and @racket[bilinear] identifiers are
  bound to either @racket[rgb-linear] and @racket[rgb-bilinear] or @racket[argb-linear]
  and @racket[argb-bilinear] as needed by particular drawing variant.
}

@defform[(define/provide-sampler (name can ...) body ...)]{
  Defines two sampler variants one @racket[name] for plain RGB sampling
  and another @racket[name/a] for ARGB sampling.

  Within the body the @racket[linear] and @racket[bilinear] identifiers are
  bound to either @racket[rgb-linear] and @racket[rgb-bilinear] or @racket[argb-linear]
  and @racket[argb-bilinear] as needed by particular sampling variant.

  Note: This is an internal syntax form.
}

@section{Querying Canvas Properties}

@defmodule[rrll/canvas/query]

This module can infer canvas properties based on its contents.

@defproc[(canvas-alpha? (can canvas?)) boolean?]{
  Returns true if given @racket[canvas?] contains one or more pixels with
  alpha value less than @racket[255].
}
