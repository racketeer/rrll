#lang racket/base

(require rrll/helper/syntax
         "core.rkt"
         rrll/color/blending
         rrll/unsafe/fixnum
         (for-syntax syntax/parse)
         racket/performance-hint)

(provide define/provide-draw
         define/provide-sampler)

(define-syntax (define/provide-draw stx)
  (syntax-parse stx
    ((_ (~optional (~and #:b b-variants))
        (name dst args ...) body ...)
     (with-syntax ((draw (datum->syntax stx 'draw))
                   (linear (datum->syntax stx 'linear))
                   (bilinear (datum->syntax stx 'bilinear))
                   (name/a (make-variant-id stx "~a/a" #'name))
                   (name/b (make-variant-id stx "~a/b" #'name))
                   (name/a/b (make-variant-id stx "~a/a/b" #'name)))
       (define add-b-variants (attribute b-variants))
       #`(begin
           (define (name dst args ...)
             (define ddata (canvas-data dst))
             (define linear rgb-linear)
             (define bilinear rgb-bilinear)
             (define (draw doff sc)
               (fxvector-set! ddata doff sc))
             body ...
             (void))
           (define (name/a dst args ...)
             (define ddata (canvas-data dst))
             (define linear argb-linear)
             (define bilinear argb-bilinear)
             (define (draw doff sc)
               (fxvector-set! ddata doff
                              (rgb-blend-argb (fxvector-ref ddata doff) sc)))
             body ...
             (void))
           (provide name name/a)
           #,@(if add-b-variants
                  #'((begin  
                       (define (name/b alpha dst args ...)
                         (define ddata (canvas-data dst))
                         (define linear rgb-linear)
                         (define bilinear rgb-bilinear)
                         (define (draw doff sc)
                           (fxvector-set! ddata doff
                                          (rgb-blend-rgb+alpha
                                           (fxvector-ref ddata doff)
                                           sc
                                           alpha)))
                         body ...
                         (void))
                       (define (name/a/b alpha dst args ...)
                         (define ddata (canvas-data dst))
                         (define linear argb-linear)
                         (define bilinear argb-bilinear)
                         (define (draw doff sc)
                           (fxvector-set! ddata doff
                                          (rgb-blend-argb+alpha
                                           (fxvector-ref ddata doff)
                                           sc
                                           alpha)))
                         body ...
                         (void))
                       (provide name/b name/a/b)))
                  '()))))))

(define-syntax (define/provide-sampler stx)
  (syntax-case stx ()
    ((_ (name can ...)
        body ...)
     (with-syntax ((linear (datum->syntax stx 'linear))
                   (bilinear (datum->syntax stx 'bilinear))
                   (name/a (make-variant-id stx "~a/a" #'name)))
       #'(begin
           (define-inline (name can ...)
             (define linear rgb-linear)
             (define bilinear rgb-bilinear)
             body ...)
           (define-inline (name/a can ...)
             (define linear argb-linear)
             (define bilinear argb-bilinear)
             body ...)
           (provide name name/a))))))
