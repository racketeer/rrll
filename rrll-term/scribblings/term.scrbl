#lang scribble/manual

@require[
  scribble-math
  (for-label
    rrll/term
    racket/base
    )]

@title[#:style (with-html5 manual-doc-style)]{RRLL: Terminal I/O}
@author[@author+email["Dominik Pantůček" "dominik.pantucek@trustica.cz"]]

Racket Rogue-Like Library: Local and Network Virtual Terminal

Unified interface for local and network virtual terminals.

@defmodule[rrll/term]

This module provides all bindings from
@racketmodname[rrll/term/private/ansi-output].

It provides @racket[make-tty] from
@racketmodname[rrll/term/private/tty], @racket[make-nvt] and
@racket[nvt-tcp-accet] from @racketmodname[rrll/term/private/nvt] and
@racket[term-resize?] and @racket[struct] @racket[term-size] bindings
from @racketmodname[rrll/term/private/size].


@defform[(with-term term body ...)]{
  Uses @racket[parameterize] on @racket[current-input-port], @racket[current-output-port] and
  @racket[current-term] and sets them to @racket[term]. The @racket[body ...] expressions are
  evaluated within the new context.

  The @racket[current-term] parameter is internal to this module only.
}

@defproc[(get-term-size (term term-base? (current-term)))
		(values (or/c fixnum? #f) (or/c fixnum? #f))]{
  Returns the size in character cells of the currently used terminal.
}

@defproc[(enable-term-read-resize! (term term-base? (current-term))) void?]{
  Enables returning terminal resize events with @racket[read].
}

@defproc[(disable-term-read-resize! (term term-base? (current-term))) void?]{
  Disables returning terminal resize events with @racket[read].
}

@defproc[(get-mouse-pos (term term-base? (current-term)))
		  (values (or/c fixnum? #f) (or/c fixnum? #f))]{
  Returns current mouse position if known - @racket[#f]s otherwise.
}

@defproc[(get-term-width (term term-base? (current-term))) fixnum?]{
  Returns the number of columns of given terminal.
}

@defproc[(get-term-height (term term-base? (current-term))) fixnum?]{
  Returns the number of lines of given terminal.
}


@section{ANSI Terminal Output}

@defmodule[rrll/term/private/ansi-output]

This module contains various utility functions for directly altering
the state of connected ANSI terminal.

@defproc[(hide-cursor (out (current-output-port))) void?]{
  Hides the terminal text cursor.
}

@defproc[(show-cursor (out (current-output-port))) void?]{
  Shows the terminal text cursor.
}

@defproc[(clrscr (out (current-output-port))) void?]{
  Clears the terminal screen.
}

@defproc[(gotoxy (x fixnum?) (y fixnum?) (out (current-output-port))) void?]{
  Moves the cursor at specified location. The upper left corner has
  both coordinates @racket[0].
}

@defproc[(enable-alternate-screen (out (current-output-port))) void?]{
  Switches the terminal to alternate screen.
}

@defproc[(disable-alternate-screen (out (current-output-port))) void?]{
  Switches the terminal back to normal screen.
}

@defproc[(enable-mouse (out (current-output-port))) void?]{
  Turns on mouse events reporting.
}

@defproc[(disable-mouse (out (current-output-port))) void?]{
  Turns off mouse events reporting.
}

@defproc[(set-window-title (title string?) (out (current-output-port))) void?]{
  Sets terminal window title to given string.
}

@section{TTY}

@defmodule[rrll/term/private/tty]

Local terminal representation.

@defproc[(make-tty (in input-port? (current-input-port))
		   (out output-port? (current-output-port))
		   (#:size-poll-interval size-poll-interval fixnum? 1)) tty?]{
  Creates new TTY terminal from given input and output ports and sets it to raw input mode. Currently can work
  only with the terminal the program was started with - as the underlying @racket[term/termios]
  operations are performed on fixed port descriptor numbers.

  Because lack of support for signals (especially SIGWINCH), the terminal
  is periodically queried for any size changes and the interval between these
  polls can be set using @racket[size-poll-interval].
}

@section{NVT - TELNET}

@defmodule[rrll/term/private/nvt]

Network virtual terminal representation.

@defproc[(make-nvt (in input-port?) (out output-port?)) nvt?]{
  Creates a new NVT from provided ports and negotiates raw input and output mode.
}

@defproc[(nvt-tcp-accept (tcp-listen-socket tcp-listener?)) nvt?]{
  Performs @racket[tcp-accept] and creates new NVT from the ports returned.
}

@section{NVT Options Storage}

@defmodule[rrll/term/private/nvt-options]

Handling of NVT options on both sides of the connection.

@require["../term/private/nvt-options.rkt"]

@defthing[nvt-code-names hash?]{

The following NVT codes are provided:

@tabular[
  #:column-properties '(left right)
  #:sep " "	
  (for/list ((code (sort (hash-keys nvt-code-names) >)))
    (define name (hash-ref nvt-code-names code))
    (list (racketidfont (format "~a" name)) (racketvalfont (format "#x~x" code)))
)]

}

@defproc[(set-nvt-option (options hash?) (action byte?) (option byte?)) hash?]{
  Maps given @racket[option] to symbolic name and in @racket[options] it sets
  given key to @racket[#t] or @racket[#f]
  based on @racket[action] interpretation.
}

@defproc[(set-nvt-option! (options hash?) (action byte?) (option byte?)) void?]{
  Like @racket[set-nvt-option] but modifies @racket[options] in place.
}

@defproc[(format-nvt-options (options hash?)) string?]{
  Returns human-readable version of NVT options stored in @racket[options]
  hash. It performs the binary to text mapping using @racket[nvt-code-names]
  lookup table.
}

@defproc[(nvt-option-bytes (action byte?) (option byte?)) bytes?]{
  Returns a 3-byte sequence that can be sent directly to the remote NVT.
}

@section{Terminal Size and Resize Reporting}

@defmodule[rrll/term/private/size]

This module unifies size and resize reporting for any terminal implementation.

@defstruct*[term-size ((columns fixnum?) (lines fixnum?)) ]{
  Stores the rectangular dimensions of terminal screen.
}

@defstruct*[(term-resize term-size) ()]{
  The same as @racket[term-size] but used in input streams.
}

@section{Terminal Base}

@defmodule[rrll/term/private/base]

Provides basic facilities to implement a terminal backend.

@defstruct*[term-base ((in input-port?)
		       (out output-port?)
		       ((size #:mutable) (or/c #f term-size?))
		       ((read-size? #:mutable) boolean?)
		       (mouse mouse-status?))]{
  All terminals must be derived from this structs. Provides unified resizing
  and mouse reporting.
}

@defproc[(do-term-resize! (tb term-base?) (new-columns fixnum?) (new-lines fixnum?)) void?]{
  Performs terminal resize - if anything changed. If the resize took place and
  the terminal's @racket[read-size?] is @racket[#t], queues resize event in
  its input port.
}

@section{Terminal Resources Garbage Collection}

@defmodule[rrll/term/private/cleanup]

This module runs a thread that performs cleanup after garbage collected terminals.

@defthing[term-will-executor will-executor?]{
  All terminals must register with this executor to ensure
  release of their resources upon garbage collection.
}

@section{Foreign Function Interface Struct Size Detection}

@defmodule[rrll/term/private/ffi-struct-size]

This module is used by @racketmodname[rrll/term/private/termios] and
@racketmodname[rrll/term/private/tiocgwinsz] to allocate memory properly.

@defproc[(ffi-detect-struct-size (procs (listof (-> cpointer? any/c)))
				 (size fixnum? 1024)) fixnum?]{
    Detects size of buffer by filling it with 0's and 255's and for
both cases checking difference after running each of procs. The
maximum offset detected + 1 is returned.
}

@defproc[(cached-ffi-detect-struct-size (procs (listof (-> cpointer? any/c)))
					(size fixnum? 1024)) fixnum?]{
  Cached (lazy-evaluated) version of @racket[ffi-detect-struct-size].
}

@section{Low-Level Termios Interface}

@defmodule[rrll/term/private/termios]

This module allows direct manipulation of program TTY descriptors. It
is used by @racketmodname[rrll/term/private/tty] to put the terminal
into a raw mode.

@defproc[(termios? (termios any/c)) boolean?]{
    Returns true if given termios is termios
    wrapped structure.
}

@defproc[(termios-get) termios?]{
    Shorthand for getting current struct termios with memory allocation.
}

@defproc[(termios-load (termios termios?)) void?]{
    Loads struct termios on stdin from given buffer.
}

@defproc[(termios-copy (termios termios?)) termios?]{
    Creates copy of given struct termios.
}

@defproc[(termios-make-raw! (termios termios?)) void?]{
    Sets given struct termios up to make it a raw terminal device.
}

@section{TTY Window Size I/O Control}

@defmodule[rrll/term/private/tiocgwinsz]

@defproc[(tiocgwinsz) (values fixnum? fixnum?)]{
Uses ioctl on system terminal to get window structure and extracts its
size in character cells.

Falls back to 80x24.
}

@section{Queue Input Port}

@defmodule[rrll/term/private/queue-port]

An implementation of input port with queue-like semantics of adding
more data to it.

@defproc[(make-queue-port (name symbol? 'queue-port)) input-port?]{
  Creates new empty queue port.
}

@defproc[(queue-port-post! (qp queue-port?) (v any/c)) void?]{
  Adds a value to given queue port queue.
}

@section{NVT Input Port}

@defmodule[rrll/term/private/iac-input-port]

Handles incoming NVT IAC codes.

@defproc[(make-iac-input-port (in input-port?) (iac-handler procedure?)) input-port?]{
  Creates an IAC input port from given port. All IAC codes are captured and
  passed on to given @racket[iac-handler]. Other data is passed on unchanged.
}

@defproc[(nvt-iac-handler (in input-port?) (options hash?) (sb-handler procedure? void)) void?]{
  Reasonable implementation of NVT IAC handler that allows passing
  IAC subnegotiations to @racket[sb-handler] for further processing.
}

@section{ANSI Input Port}

@defmodule[rrll/term/private/ansi-input-port]

Implementation of ANSI input decoding on top of another input port.

@defproc[(make-ansi-input-port (in input-port?)
			       (queue-port (or/c queue-port? #f) #f)
			       (#:mouse-status mouse-status (or/c mouse-status? #f) #f))
		input-port?]{
  Creates ANSI input decoding port. If no @racket[queue-port] is given, it
  creates a new one. Existing is needed by one terminal implementation.

  Both the original port and @racket[mouse-status] struct for mouse reporting
  are held in weak boxes.
}

@section{Mouse Input}

@defmodule[rrll/term/private/mouse]

This module is used by
@racketmodname[rrll/term/private/ansi-input-port] to store the results
of mouse tracking.

@defstruct*[mouse-status ((x fixnum?)
			  (y fixnum?)
			  (b1 boolean?)
			  (b2 boolean?)
			  (b3 boolean?))]{
  Storage for mouse position and status of its three buttons.
}

@defproc[(make-mouse-status) mouse-status?]{
  Creates empty mouse status - a helper procedure so that the creator doesn't need to know
  the exact contents of the struct.
}
