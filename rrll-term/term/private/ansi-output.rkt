#lang racket/base

(provide hide-cursor
         show-cursor
         clrscr
         gotoxy
         enable-alternate-screen
         disable-alternate-screen
         enable-mouse
         disable-mouse
         set-window-title)

(define (hide-cursor (out (current-output-port)))
  (write-bytes #"\x1B[?25l" out))

(define (show-cursor (out (current-output-port)))
  (write-bytes #"\x1B[?25h" out))

(define (clrscr (out (current-output-port)))
  (write-bytes #"\x1B[0m\x1B[2J" out))

(define (gotoxy x y (out (current-output-port)))
  (display (format "\x1B[~a;~aH" (add1 y) (add1 x)) out))

(define (enable-alternate-screen (out (current-output-port)))
  (write-bytes #"\x1B[?1049h" out))

(define (disable-alternate-screen (out (current-output-port)))
  (write-bytes #"\x1B[?1049l" out))

(define (enable-mouse (out (current-output-port)))
  ; #define SET_ANY_EVENT_MOUSE         1003
  ; #define SET_SGR_EXT_MODE_MOUSE      1006
  (write-bytes #"\x1B[?;1003h\x1B[?;1006h" out))

(define (disable-mouse (out (current-output-port)))
  (write-bytes #"\x1B[?;1003l\x1B[?;1006l" out))

(define (set-window-title title (out (current-output-port)))
  (display (format "\x1B]2;~a\x1B\\" title)))
