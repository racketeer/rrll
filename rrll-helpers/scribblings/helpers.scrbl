#lang scribble/manual

@title{RRLL: Helpers}
@author[@author+email["Dominik Pantůček" "dominik.pantucek@trustica.cz"]]

Racket Rogue-Like Library: Miscellaneous Helpers

These are miscellaneous modules containing commonly used algorithms and/or
(optionally enabled) unsafe ops for some data structures.

@section{Clipping Helpers}

@defmodule[rrll/helper/clipping]

This module contains clipping helpers used by other RRLL packages.

@defproc[(clip-range (x fixnum?) (w fixnum?) (bx fixnum?) (bw fixnum?)) (values fixnum? fixnum?)]{
  Makes sure the range from @racket[x] (inclusive) to @racket[(+ x w)]
  (exclusive) fits within the range @racket[bx] (inclusive) to
  @racket[(+ bx bw)] (exclusive).

  Returns clipped @racket[x] and @racket[w]. If returned @racket[w] is not @racket[positive?],
  it should be treated as empty range.
}

@defproc[(clip-rect (x fixnum?) (y fixnum?) (w fixnum?) (h fixnum?)
		    (bx fixnum?) (by fixnum?) (bw fixnum?) (bh fixnum?))
	   (values fixnum? fixnum? fixnum? fixnum?)]{

  Uses @racket[clip-range] to clip both horizontal and vertical range
  of a rectangle against another rectangle.
}

@defproc[(double-clip-range (sx fixnum?) (sw fixnum?) (dx fixnum?) (dw fixnum?))
	   (values fixnum? fixnum? fixnum? fixnum?)]{
  Like @racket[clip-range] but if @racket[dx] is negative, makes sure
  it is @racket[0] and adjusts @racket[sx] accordingly before clipping it further.

  Returns all four values clipped - although @racket[dw] is never changed.
}

@defproc[(double-clip-rect (sx fixnum?) (sy fixnum?) (sw fixnum?) (sh fixnum?)
			   (dx fixnum?) (dy fixnum?) (dw fixnum?) (dh fixnum?))
	   (values fixnum? fixnum? fixnum? fixnum? fixnum? fixnum? fixnum? fixnum?)]{
  Uses @racket[double-clip-range] to clip a rectangle against another.

  The @racket[dw] and @racket[dh] arguments are returned unchanged.
}

@defproc[(clip-range/scale (x fixnum?) (w fixnum?)
			   (bx fixnum?) (bw fixnum?)
			   (us flonum?) (ue flonum?))
	    (values fixnum? fixnum? flonum? flonum?)]{
  Like @racket[clip-range] but also clips associated @racket[flonum?]
  coordinates proportionally.
}

@section{Sorting Helpers}

@defmodule[rrll/helper/ordering]

This module contains helpers for sorting vertices of triangles on screen.

@defproc[(order-pairs (x1 fixnum?) (y1 fixnum?)
		      (x2 fixnum?) (y2 fixnum?)
		      (x3 fixnum?) (y3 fixnum?))
	  (values fixnum? fixnum? fixnum? fixnum? fixnum? fixnum?)]{
  Returns the three pairs sorted by the Y-coordinate.
}

@section{Generic Syntax Variant Helpers}

@defmodule[rrll/helper/syntax]

This module reprovides all of @racketmodname[racket/base] for syntax
stage and the @racket[make-variant-id] for syntax stage as well.

@defproc[(make-variant-id (stx syntax?)
			  (fmt string?)
			  (sids any/c) ...) syntax?]{

  Returns a new syntax identifier in the same scope as @racket[stx]
  using @racket[fmt] format string and @racket[format] argument list
  @racket[sids].

}
